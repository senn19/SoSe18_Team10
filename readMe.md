## Synopsis

Dieses Git umfasst alle Projektfortschritte des Integrationsprojekts für **Team10**.

Die Vision der Sharing-Plattform ist es auch weniger solventen Menschen den Zugang zu hochwertigen Zeichentabletts und
Pen-Displays zu ermöglichen. Das Unternehmen vermittelt diese Geräte dabei von Zeichentablett und Pen-Display Anbietern
an Unternehmens- und Privatverbrauchergruppen und erhält für diese Vermittlung eine Provision vom Mietpreis des Angebots.
Sollte der Erwerb des Gerätes während einer Vertragslaufzeit gewünscht sein, kann der Mieter das Gerät, abzüglich der
bereits gezahlten Miete, in einem Zug abbezahlen. Außerdem besteht auch von Beginn an die Möglichkeit die Zeichentabletts
und Pen-Displays über die Sharing-Plattform zu erwerben. Hierbei wird eine Provision für die Vermittlung vom Neupreis erhoben.
Jedes Angebot kann von den Unternehmens- und Privatverbrauchergruppen bewertet und kommentiert werden.


## Code Example

#### MySQL Handler

Um Probleme mit asynchronen Funktionen zu vermeiden, bietet die Klasse MySQL_Handler die Möglichkeit die SQL-Verbindungen
nacheinander aufzubauen, die Datensätze in Variablen zu speichern und über weitere Promises zu verarbeiten.

Folgend ein Beispiel aus der server.ts Datei:
```javascript
    mySQL.query(sql).then(rows => {
        let result : any = rows;
            ...
    }).catch(err => {
        ...
    }).finally(mySQL.close);
```

Hier wird eine neue Verbindung aufgebaut und der Parameter "sql" wird an den MySQL Handler übergeben.
Optional können auch zusätzliche Argumente übergeben werden, sollte man andere Funktionen von MySQL2 nutzen wollen.
Ist die Verbindung erfolgreich gewesen, erhält man die Datensätze, mit welchen man nun arbeiten könnte, zurück.
Sollte eine Verbindung fehlschlagen wird mit catch die Rejection abgefangen.

## Motivation

## Installation

Um das Projekt erfolgreich auf lokalen Systemen testen und ausführen zu können sind einige Installationen erforderlich.
Vorerst festgehalten: Man spricht hier von einem Single Pager auf HTML5/CSS3 Basis, laufend auf einem lokalen/externen
Server mit NodeJS.

express & express-session (Fast, unopinionated, minimalist web framework for node + session management):
```bash
    >> npm i express
    >> npm i express-session
```

client-side-sheets/scripts (bootstrap, jquery, font-awesome, popper.js)
```bash
    >> npm i bootstrap
    >> npm i jquery
    >> npm i --save-dev @fortawesome/fontawesome-free
    >> npm i popper.js
```

mysql2 (MySQL client for Node.js with focus on performance):
```bash
    >> npm i mysql2
```

ejs (Embedded JavaScript templates):
```bash
    >> npm i ejs
```

apidoc (Generates a RESTful web API Documentation):
```bash
    >> npm i apidoc
```

@types/xyz (Typen-Definitionen für Node):
```bash
    >> npm i @types/jquery
    >> npm i @types/bootstrap
    >> npm i @types/node
    >> npm i @types/express
    >> npm i @types/express-session
    >> npm i --save-dev @types/es6-promise
    >> npm i @types/cryptojs
```

Zusätzliche Module für Smooth Scrolling & CryptoJS
```bash
    >> npm i wowjs
    >> npm i crypto-js
    >> npm i sprintf-js
```

## API Reference

```bash
    >> apidoc -i server/ -o server/apiDoc/
```

## Tests

## License