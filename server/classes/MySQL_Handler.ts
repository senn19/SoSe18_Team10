import * as mysql from "mysql2";
import {Connection, MysqlError} from "mysql2";
import {Promise} from 'es6-promise'

export class MySQL_Handler {
    connection;

    // build connection to database
    constructor() {
         this.connection = mysql.createConnection({
             host: '127.0.0.1',
             user: 'root',
             password: '',
             database: 'creativeminds'
         });
    }
    // query sql-statement with optional args, on success resolve rows, on failure reject with an error
    public query(sql : string, args? : any[]) {
        return new Promise( (resolve, reject) => {
            this.connection.query(sql, args, (err: MysqlError|null, rows: any) => {
                if (err) return reject(err);
                resolve(rows);
            });
        });
    }
    // close opened mysql connection
    public close() {
        return new Promise((resolve, reject) => {
            this.connection.end(err => {
                if (err) return reject(err);
                resolve();
            });
        });
    }
}