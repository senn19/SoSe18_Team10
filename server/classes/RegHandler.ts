import CryptoJS = require("crypto-js");

export class RegHandler {

    email : string;
    password : string;

    nickname : string;

    constructor(email : string, password : string) {
        this.email = email;
        this.password = password;
    }

    // validate entered email, encodeURI(email), decrypt password to validate and encodeURI(password), encrypt it again
    // create nickname by splitting email. If nothing went wrong, return true
    public prepareAccount() : boolean {
        if(this.validateEmail()) {
            this.escapeEmail();
        } else {
            return false;
        }
        this.decryptPassword();
        if(this.validatePassword()) {
            this.escapePassword();
        } else {
            return false;
        }
        this.createNickname();
       // console.log("Debug: RegHandler");
        return true;
    }

    // email is valid if there is an @-Symbol and a dot after the @.
    private validateEmail() : boolean {
        let posAt: number = this.email.indexOf("@");
        let posDot: number = this.email.lastIndexOf(".");
        return (!(posAt < 1) || !(posDot < posAt + 2) || !(posDot + 2 > this.email.length));
    }

    // password is valid if password contains upper/lowercase, min 7 - max 16 chars, numbers
    private validatePassword() : boolean {
        return (this.password.match(/[0-9]/g) && this.password.match(/[A-Z]/g) &&
            this.password.match(/[a-z]/g) && this.password.length > 7 && this.password.length < 16)
    }

    // escape email
    private escapeEmail() : void {
        this.email = encodeURI(this.email);
    }

    // escape passworod
    private escapePassword() : void {
        this.password = encodeURI(this.password);
    }

    // decrypt password - cryptoJS
    private decryptPassword() : void {
        this.password = CryptoJS.AES.decrypt(this.password, "crossword").toString(CryptoJS.enc.Utf8);
    }

    // explode e-mail, create nickname
    private createNickname() : void {
        let nickname : string[] = this.email.split("@");
        this.nickname = nickname[0];
    }
}