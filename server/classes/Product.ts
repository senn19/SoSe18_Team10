export class Product {
    ID: number;
    id_provider: number;
    id_category: number;
    price: number;
    name: string;
    description: string;
    length: string;
    width: string;
    height: string;
    display_size: string;
    resolution: string;
    advanced_controls: string;
    productivity_supplies: string;
    processor: string;
    graphics_card: string;
    memory: string;
    product_type: string;
    micro_sd_slot: string;
    ram: string;
    extensible_memory: string;
    picture: string; //url


    constructor(id: number,
                id_provider: number,
                id_category: number,
                price: number,
                name: string,
                description: string,
                length: string,
                width: string,
                height: string,
                display_size: string,
                resolution: string,
                advanced_controls: string,
                productivity_supplies: string,
                processor: string,
                graphics_card: string,
                memory: string,
                product_type: string,
                micro_sd_slot: string,
                ram: string,
                extensible_memory: string,
                picture: string) {
        this.ID = id;
        this.id_provider = id_provider;
        this.id_category = id_category;
        this.price = price;
        this.name = name;
        this.description = description;
        this.length = length;
        this.width = width;
        this.height = height;
        this.display_size = display_size;
        this.resolution = resolution;
        this.advanced_controls = advanced_controls;
        this.productivity_supplies = productivity_supplies;
        this.processor = processor;
        this. graphics_card = graphics_card;
        this.memory = memory;
        this.product_type = product_type;
        this.micro_sd_slot = micro_sd_slot;
        this.ram = ram;
        this.extensible_memory = extensible_memory;
        this.picture = picture;
    }
}