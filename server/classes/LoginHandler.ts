import {MySQL_Handler} from "./MySQL_Handler";
import CryptoJS = require("crypto-js");

let mySQL = new MySQL_Handler();

export class LoginHandler {

    email : string;
    password : string;

    constructor(email : string, password : string) {
        this.email = email;
        this.password = password;
    }

    /* authenticate user via condition, encrypt clientside password, get serverside password from database,
    *  encrypt serverside password, send promise back to route */
    public authenticateLogin() : Promise<any> {
        this.password = this.encryptClientPassword();
        let sql : string = "SELECT password FROM user " +
            "WHERE email='"+this.email+"';";
        return mySQL.query(sql).then(row => {
            let result : any = row;
            if(!result[0]) return false;
            return (this.password == this.encryptDatabasePassword(result[0].password));
        });
    }

    private encryptClientPassword() : string {
        return CryptoJS.AES.decrypt(this.password, "crossword").toString(CryptoJS.enc.Utf8);
    }

    private encryptDatabasePassword(password : string) : string {
        return CryptoJS.AES.decrypt(password, "drowssap").toString(CryptoJS.enc.Utf8);
    }
}