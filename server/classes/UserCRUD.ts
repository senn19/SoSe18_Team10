import {User} from "./User";
import {MySQL_Handler} from "./MySQL_Handler";
import CryptoJS = require("crypto-js");
import {UserData} from "./UserData";

const LANG_USER = require("../const/const_UserCRUD");
let mySQL = new MySQL_Handler(),
    sprintf = require("sprintf-js").sprintf;


export class UserCRUD {

    public createUser(user: User): Promise<any> {
        user.password = CryptoJS.AES.encrypt(user.password, "drowssap").toString();
        // check if email already exists in database
        return mySQL.query(sprintf(LANG_USER.SQL_EMAIL_SELECT, user.email)).then(rows => {
            if (rows[0]) return {message: LANG_USER.ERR_EMAIL_EXIST, user: false}; // email already exists
            // check if groupLabel exists in database
            return this.getGroupID(user.usergroup_label).then(groupID => {
                if (!groupID[0]) return {message: LANG_USER.ERR_GROUP_LABEL, user: false}; // group doesn't exist
                // insert user in database
                return mySQL.query(sprintf(LANG_USER.SQL_USER_INSERT, user.forename, user.surname,
                    user.nickname, user.email, user.password, groupID[0].ID)).then(result => {
                    let anyRows: any = result;
                    if (!anyRows) return {message: LANG_USER.ERR_USER_CREATE, user: false}; // mySQL insert failure
                    // insert user.ID in address.ID_user
                    return mySQL.query(sprintf(LANG_USER.SQL_ADDRESS_INSERT, anyRows.insertId)).then(result => {
                        let anyRows: any = result;
                        if (!anyRows.insertId) return {message: LANG_USER.ERR_USER_CREATE, user: false};
                        return {message: sprintf(LANG_USER.SUCC_USER_CREATE, user.email), user: user}
                    });
                });
            });
        });
    }


    private getGroupID(groupLabel: string): Promise<any> {
        return mySQL.query(sprintf(LANG_USER.SQL_GROUPLABEL_SELECT, groupLabel));
    }


    public updateUser(user: any, id: number): Promise<any> {
        let userObject: UserData = new UserData(
            user.email, (user.password? CryptoJS.AES.encrypt(user.password, "drowssap").toString() : ""),
            user.forename, user.surname, user.nickname, user.usergroup_label,
            id, user.street, user.housenumber, user.postcode,
            user.town, user.telefone, user.mobile, user.fax,
            user.date_of_birth, user.IBAN, user.BIC);

        // check if email already exists in database
        return mySQL.query(sprintf(LANG_USER.SQL_UPDATE_EMAIL_SELECT, user.email, id)).then(rows => {
            if (rows[0] && rows[0].email == user.email && rows[0].ID != id)
                return {message: LANG_USER.ERR_EMAIL_EXIST, failure: true}; // email already exists

            // build sql statement
            let sqlStatement: string = "UPDATE user, address, usergroup SET ";
            for (let key in userObject) {
                if (userObject.hasOwnProperty(key)) {
                    if (!userObject[key]) continue;
                    if (key == "ID") {
                        sqlStatement += "user." + key + "=" + "'" + userObject[key] + "',";
                    } else if (key == "usergroup_label") { // TODO USERGROUP LABEL
                        sqlStatement += "";
                    } else {
                        sqlStatement += key + "=" + "'" + (userObject[key])/*.replace("<(?:[^>=]|='[^']*'|=\"[^\"]*\"|=[^'\"][^\\s>]*)*>", "") */+ "',"
                    }
                }
            }
            sqlStatement = sqlStatement.slice(0, -1) + " WHERE user.ID=" + id + " AND user.ID=address.ID_user " +
                "AND usergroup.ID = user.ID_usergroup;";

            return mySQL.query(sqlStatement)
        }).then(update => {
            let anyRows: any = update;
            if (!anyRows.affectedRows || anyRows.failure) return {
                message: LANG_USER.ERR_DATA_NOTUPDATED,
                previousMessage: anyRows.message,
                user: false
            };
            return {message: LANG_USER.SUCC_USER_UPDATED, user: userObject}
        });
    };


    public deleteUser(id: number): Promise<any> {
        return mySQL.query(sprintf(LANG_USER.SQL_USER_DELETE_ADDRESS, id)).then(rows => {
            return mySQL.query(sprintf(LANG_USER.SQL_USER_DELETE_USER, id))
        }).then(rows => {
            let result: any = rows;
            if (!result.affectedRows) return {message: LANG_USER.ERR_USER_DELETE, user: false};
            return {message: LANG_USER.SUCC_USER_DELETE};
        })
    }


    public getUser(id: number): Promise<any> {
        return mySQL.query(sprintf(LANG_USER.SQL_USER_ADDRESS_SELECT, id)).then(user => {
            if (!user[0]) return {message: LANG_USER.ERR_NO_USER_WITH_ID, user: false};
            let userObject: UserData = new UserData(
                user[0].email, null, user[0].forename,
                user[0].surname, user[0].nickname, user[0].usergroup_label,
                user[0].ID, user[0].street, user[0].housenumber,
                user[0].postcode, user[0].town, user[0].telefone,
                user[0].mobile, user[0].fax, user[0].date_of_birth,
                user[0].IBAN, user[0].BIC);
            return {message: sprintf(LANG_USER.SUCC_USER_FOUND, id), user: userObject};
        });
    }


    public getUserList(): Promise<any> {
        return mySQL.query(LANG_USER.SQL_USERLIST_SELECT).then(rows => {
            let userList: User[] = [];
            let user: any = rows;
            if (!user[0]) return {message: LANG_USER.ERR_USER_NOT_FOUND, userList: false};
            for (let i: number = 0; i < user.length; i++) {
                if (user[i]) userList.push(new User(user[i].email, user[i].password, user[i].forename, user[i].surname,
                    user[i].nickname, user[i].usergroup_label, user[i].ID));
            }
            return {message: LANG_USER.SUCC_USER_LOAD, userList: userList};
        });
    }
}