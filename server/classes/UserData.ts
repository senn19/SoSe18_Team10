import {User} from "./User";

export class UserData extends User {
    street: string;
    housenumber: number;
    postcode: string;
    town: string;
    telefone: string;
    mobile: string;
    fax: string;
    date_of_birth: string;
    IBAN: string;
    BIC: string;

    constructor(email: string, password: string, forename: string = "", surname: string = "", nickname: string = "",
                usergroup_label: string = "Nutzer", id: number = null, street: string = "",
                housenumber: number = null, postal: string = "", town: string = "", cellphone: string = "", mobile: string = "",
                fax: string = "", date_of_birth: string = "", IBAN: string = "", BIC: string = "") {

        super(email, password, forename, surname, nickname, usergroup_label, id);
        this.street = street;
        this.housenumber = housenumber;
        this.postcode = postal;
        this.town = town;
        this.telefone = cellphone;
        this.mobile = mobile;
        this.fax = fax;
        this.date_of_birth = date_of_birth;
        this.IBAN = IBAN;
        this.BIC = BIC;
    }
}