import {Product} from "./Product";
import {MySQL_Handler} from "./MySQL_Handler";

let mySQL = new MySQL_Handler();

export class ProductList {
    // optionalID: -1 if not ID submitted
    public getProductList(sqlArgs?): Promise<Product[]> {
       // console.log("getprodlist");
        let query = 'SELECT product.ID, product.ID_producer, ID_categories, price, product.name, description, length, width,' +
            'height, display_size, resolution, advanced_controls, productivity_supplies, processor, graphics_card, memory,' +
            'product_type, micro_sd_slot, ram, extensible_memory, picture FROM product ';
        if(sqlArgs && sqlArgs != "") {
            query += sqlArgs;
        }
        return mySQL.query(query).then(rows => {
            let productList: Product[] = [];
            let result: any = rows;
            if (result.affectedRows != 0) {
                // console.log("affected");
                for (let i = 0; i < result.length; i++) {
                    productList.push(new Product(
                        result[i].ID,
                        result[i].id_provider,
                        result[i].id_category,
                        result[i].price,
                        result[i].name,
                        result[i].description,
                        result[i].length,
                        result[i].width,
                        result[i].height,
                        result[i].display_size,
                        result[i].resolution,
                        result[i].advanced_controls,
                        result[i].productivity_supplies,
                        result[i].processor,
                        result[i].graphics_card,
                        result[i].memory,
                        result[i].product_type,
                        result[i].micro_sd_slot,
                        result[i].ram,
                        result[i].extensible_memory,
                        result[i].picture));
                    //console.log("ID: "+result[i].ID);
                }
            }
            return productList;
        });
    }

    public getProductListByID(id: number) : Promise<Product[]> {
        let query = 'SELECT * FROM product WHERE ID = ' + '"' + id + '";';

       return mySQL.query(query).then(rows => {
            let productList: Product[] = [];
            let result: any = rows;

            if (result.affectedRows != 0) {
               // console.log("affected");
                for (let i = 0; i < result.length; i++) {
                 //   console.log("result[i]: "+result[i].name);
                    productList.push(new Product(
                        result[i].ID,
                        result[i].id_provider,
                        result[i].id_category,
                        result[i].price,
                        result[i].name,
                        result[i].description,
                        result[i].length,
                        result[i].width,
                        result[i].height,
                        result[i].display_size,
                        result[i].resolution,
                        result[i].advanced_controls,
                        result[i].productivity_supplies,
                        result[i].processor,
                        result[i].graphics_card,
                        result[i].memory,
                        result[i].product_type,
                        result[i].micro_sd_slot,
                        result[i].ram,
                        result[i].extensible_memory,
                        result[i].picture));
                }
            }
            return productList;
        });
    }
}