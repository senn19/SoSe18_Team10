export class FAQ{
    questi : string;
    answer : string;

    constructor(questi: string, answer: string){
        this.questi = questi;
        this.answer = answer;
    }
}