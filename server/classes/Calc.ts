const PRICE = require("../const/const_Pricing");

export class Calc {

    time : number; // time in days
    price : number;
    cost : number;

    constructor(time : number, price : number) {
        this.time = time;
        this.price = price;
    }

    public calcPrice() : number {
        if(this.time % PRICE.ONE_WEEK === 0) { // 14 / 7 = 0    ||     30 / 7 != 0
            this.cost = this.calcPriceForWeeks();
        } else {
            this.cost = this.calcPriceForMonths();
        }
        this.calcProvision();
        return this.cost;
    }

    private calcPriceForWeeks() : number {
        this.time = PRICE.ONE_MONTH;
        let pricePerMonth : number = this.calcPriceForMonths();
        return pricePerMonth * PRICE.PERCENTAGE_FOR_WEEKS;
    }

    private calcPriceForMonths() : number {
        let amount : number = this.time / PRICE.ONE_MONTH;
        switch(amount) {
            case 1:
                return this.price * PRICE.PERCENTAGE_FOR_ONE_MONTH;
            case 3:
                return this.price * PRICE.PERCENTAGE_FOR_THREE_MONTHS;
            case 6:
                return this.price * PRICE.PERCENTAGE_FOR_SIX_MONTHS;
            case 12:
                return this.price * PRICE.PERCENTAGE_FOR_TWELVE_MONTHS;
        }
    }

    private calcProvision() : void {
        if(this.price < PRICE.PRICE_LIMIT_ONE) {
            this.cost = this.cost * ((PRICE.PRICE_PROVISION_ONE + 1) * (PRICE.MWST + 1))
        } else if (this.price < PRICE.PRICE_LIMIT_TWO) {
            this.cost = this.cost * ((PRICE.PRICE_PROVISION_TWO + 1) * (PRICE.MWST + 1))
        } else {
            this.cost = this.cost + ((this.cost * PRICE.PRICE_PROVISION_THREE) * (PRICE.MWST + 1));
        }
    }
}