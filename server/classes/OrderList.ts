import {MySQL_Handler} from "./MySQL_Handler";
import {Order} from "./Order";
const COLS_ORDER_WISHLIST = require("../const/const_table_column_names");
const COLS_PRODUCT = require("../const/const_table_column_names");
const PRODUCT = require("../const/const_tablenames");
const ORDER_WISHLIST = require("../const/const_tablenames");

let mySQL = new MySQL_Handler();

export class OrderList {
    public getOrderList(id): Promise<Order[]> {

        let query = 'SELECT '
            + COLS_PRODUCT.PROD_NAME + ', '
            + COLS_PRODUCT.PROD_PICTURE + ', '
            + COLS_ORDER_WISHLIST.ORDER_WISHLIST_CREATION_DATE + ', '
            + COLS_ORDER_WISHLIST.ORDER_WISHLIST_LOAN_START_DATE + ', '
            + COLS_ORDER_WISHLIST.ORDER_WISHLIST_LOAN_END_DATE + ', '
            + COLS_ORDER_WISHLIST.ORDER_WISHLIST_FINAL_PRICE
            +' FROM ' + PRODUCT.TABLE_PRODUCT + ", " + ORDER_WISHLIST.TABLE_ORDER_WISHLIST + ' WHERE '
            + COLS_ORDER_WISHLIST.ORDER_WISHLIST_ID_USER + " = " + id + ' AND '
            + COLS_ORDER_WISHLIST.ORDER_WISHLIST_ID_PRODUCT + " = " + COLS_PRODUCT.ID + ";";

        return mySQL.query(query).then(rows => {
            let orderList: Order[] = [];
            let result: any = rows;

            if (result.affectedRows != 0) {
                console.log("affected");
                for (let i = 0; i < result.length; i++) {
                    orderList.push(new Order(
                        result[i].product_name,
                        result[i].product_image,
                        result[i].creation_date,
                        result[i].loan_start_date,
                        result[i].loan_end_date,
                        result[i].final_price));
                }
            }
            return orderList;
        });
    }
}