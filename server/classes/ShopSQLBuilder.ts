export class ShopSQLBuilder {

    productTable : string = "product";
    //userTable : string = "user";
    producerTable : string = "producer";
    categoriesTable : string = "categories";

    SQL_SELECT : string = "SELECT ";
    SQL_FROM : string = " FROM ";
    SQL_WHERE : string = " WHERE ";
    SQL_STATEMENT : string = "";

    // build sql-statement through conditions.
    public getShopListSQL(query : any) : string {
            this.SQL_STATEMENT += this.SQL_SELECT +
                                this.productTable+".ID,"+
                                this.productTable+".name,"+
                                this.productTable+".picture";
            this.SQL_FROM += this.productTable;

            if(Object.keys(query).length) {
                let tempSQL : string = "";

                // check if category exists and iterate through category list to create the 'args' statement of WHERE
                if(query.category) {
                    let categorySQL : string = "";
                    this.sqlFROM("," + this.categoriesTable);
                    this.sqlWHERE(this.productTable+".ID_categories="+this.categoriesTable+".ID");
                    let categories : any[] = [query.category];
                    categories = categories[0].toString().split(",");
                    for(let key in categories) {
                        categorySQL += this.categoriesTable+".name='"+categories[key]+"'";
                        if(parseInt(key)+1 < categories.length) categorySQL += " or ";
                    }
                    tempSQL += "("+categorySQL+")";
                }

                // to combine category and producer args
                if(query.category && query.producer) {
                    tempSQL += " AND ";
                }

                // check if producer exists and iterate through producer list to create the 'args' statement of WHERE
                if(query.producer) {
                    let producerSQL : string = "";
                    this.sqlFROM("," + this.producerTable);
                    this.sqlWHERE(this.productTable+".ID_producer="+this.producerTable+".ID");
                    let producer : any[] = [query.producer];
                    producer = producer[0].toString().split(",");
                    for(let key in producer) {
                        producerSQL += this.producerTable+".name='"+producer[key]+"'";
                        if(parseInt(key)+1 < producer.length) producerSQL += " or ";
                    }
                    tempSQL += "("+producerSQL+")";
                }
                this.sqlSTATEMENT(tempSQL);
            } else {
                this.sqlSTATEMENT();
            }

            return this.SQL_STATEMENT;
        }

        private sqlFROM(args : string) : void {
            this.SQL_FROM += args;
        }

        private sqlWHERE(args : string) : void {
            this.SQL_WHERE += args + " AND ";
        }

        private sqlSTATEMENT(args : string = "") : string {
            if(!args) return this.SQL_STATEMENT += this.SQL_FROM;
            this.SQL_STATEMENT += this.SQL_FROM + this.SQL_WHERE + "(" + args + ");";
        }
    }