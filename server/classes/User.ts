export class User {
    ID: number;
    forename: string;
    surname : string;
    nickname : string;
    password : string;
    email : string;
    usergroup_label : string;

    constructor(email: string, password : string, forename: string = "", surname: string = "", nickname: string = "",
                usergroup_label : string = "Nutzer", id: number = null) {

        this.ID = id;
        this.forename = forename;
        this.surname = surname;
        this.nickname = nickname;
        this.email = email;
        this.password = password;
        this.usergroup_label = usergroup_label;
    }
}