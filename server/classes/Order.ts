export class Order {
    ID: number;
    product_name: string;
    product_image: string;
    creation_date: string;
    loan_start_date: string;
    loan_end_date: string;
    final_price: string;

    constructor(creation_date: string, product_name: string, product_image: string,
                loan_start_date: string, loan_end_date: string, final_price: string) {
        this.creation_date = creation_date;
        this.product_name = product_name;
        this.product_image = product_image;
        this.loan_start_date = loan_start_date;
        this.loan_end_date = loan_end_date;
        this.final_price = final_price;
    }
}