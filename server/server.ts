// import of different modules
import express    = require ("express");
import { Request, Response } from "express";
import {Connection, MysqlError} from "mysql2";
import session = require("express-session");
// import CryptoJS = require("crypto-js");

// import different Routings
let shopRoute = require("./routes/shopRoute");
let loginRoute = require("./routes/loginRoute");
let userCRUD_Route = require("./routes/userCRUD_Route");
let productCrudRoute = require ("./routes/productCRUD_route");
let faqRoute = require ("./routes/faq_routes");
let calcRoute = require ("./routes/calcRoute");

// initialising and declaring global variables
let router = express();

// testing crypto-js. :)))))))))
// console.log(CryptoJS.AES.encrypt("root45Ab", "crossword").toString());

// set view engine to ejs
router.set('view engine', 'ejs');

// router/express() listens on port 8080
router.listen(8080, "localhost", function () {
    console.log(`
    -------------------------------------------------------------
    Server wurde erfolgreich gestartet und läuft unter:
    Single Pager:
    http://localhost:8080/
    
    phpMyAdmin:
    http://localhost/phpmyadmin/
    
    apiDoc:
    http://localhost/apiDoc/
    -------------------------------------------------------------
  `);
});

// middleware ~ cuz of whyever
router.use((req: Request, res, next) => {
    res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
   // console.log(req.method + " " + req.url); nah - consoleflood
    next();
});



// routing different folders and main path. Folders contain stylesheets & scripts.
router.use("/",   express.static(__dirname + "/../client/"));
router.use("/apiDoc", express.static(__dirname + "/apiDoc/"));
router.use("/jquery",  express.static(__dirname + "/../node_modules/jquery/dist/"));
router.use("/popper.js",    express.static(__dirname + "/../node_modules/popper.js/dist/"));
router.use("/bootstrap",    express.static(__dirname + "/../node_modules/bootstrap/dist/"));
router.use("/font-awesome", express.static(__dirname + "/../node_modules/@fortawesome/fontawesome-free/"));
router.use("/crypto-js", express.static(__dirname + "/../node_modules/crypto-js/"));
router.use("/animate", express.static(__dirname + "/../node_modules/wowjs/css/"));
router.use("/wowjs", express.static(__dirname + "/../node_modules/wowjs/dist/"));

router.use(express.json());
router.use(session({
    resave: true,
    saveUninitialized: true,
    name : 'Creative Cookie',
    secret: '2C44-4D44-WppQ38S',
    cookie : { maxAge: 20 * 60 * 1000 },
}));


router.get("/", (req: Request, res: Response) => {
    res.render(__dirname+'/../client/index', {
        loggedIn : !!req.session.email, // true, if user is loggedIn via mail
        admin : req.session.admin, // true, if user has group admin
        provider : req.session.provider, // true, if user has group provider
        nickname : req.session.nickname,
        full_name : req.session.forename +" "+ req.session.surname,
   });
});



/* * * * * * * * * S H O P S Y S * * * * * * * * * * */
/**
 * @api {get} /shop ShopList
 * @apiName ShopList
 * @apiGroup ShopSys
 *
 * @apiDescription Endpunkt gibt eine Liste an Shop Produkten als nested Array in JSON zurück.
 * Anhand von Parametern die an den Endpunkt übergeben werden, können bereits Werte zurückgegeben werden.
 *
 * @apiParam {String} [category] Parameter zum Filtern nach Kategorien
 * @apiParam {String} [producer] Parameter zum Filtern nach Anbietern
 *
 * @apiSampleRequest /shop
 *
 * @apiError Internal Server Error
 *
 * @apiErrorExample {json} Internal Server Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Select all error: " + err.code" }
 *
 *
 * @apiSuccess {Any[]} productList Nested Array-JSON: [{product : {ID, name, picture}}]
 *
 * @apiSuccessExample {json} Products found:
 *     HTTP/1.1 200 OK
 *     {"productList" : "productList"}
 *
 * @apiSuccessExample {json} No Products found:
 *     HTTP/1.1 200 OK
 *     {message: "Es wurden keine Produkte im Shop gefunden."}
 *
 * @apiVersion 1.2.0
 */
router.route("/shop").get(shopRoute.getShopList);





/* * * * * * * * * L O G I N S Y S * * * * * * * * * * */
/**
 * @api {post} /login Login
 * @apiName Login
 * @apiGroup LoginSys
 *
 * @apiDescription Endpunkt führt einen Login aus, sofern die übergebenen Parameter korrekt verschlüsselt, übermittelt
 * und mit den Datensätzen in der Datenbank übereinstimmen.
 *
 * @apiSampleRequest /login
 *
 * @apiParam {String} email Benutzerkonten-Email - Parameter explizit notwendig.
 * @apiParam {String} password AES-verschlüsseltes Passwort eines Nutzerkontos.
 *
 * @apiError Invalid_Data "Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist nicht korrekt."
 * @apiError Internal_Server_Error "An Error has occurred: code"
 *
 * @apiErrorExample {json} Internal Server Error:
 *     HTTP/1.1 500 Internal Server Error
 *     {"message": "An Error has occurred: error"}
 *
 * @apiErrorExample {json} Invalid Data:
 *      HTTP/1.1 400 Bad Request
 *      {"message": "Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist nicht korrekt."}
 *
 *
 * @apiSuccess {Boolean} login Boolean Wert zum Bestätigen eines erfolgreichen/fehlgeschlagenen Logins
 * @apiSuccessExample {json} Logged In:
 *     HTTP/1.1 200 OK
 *     { message: "Sie haben sich erfolgreich eingeloggt.", login: true }
 *
 * @apiVersion 1.2.0
 */
router.route("/login").post(loginRoute.userLogin);

/**
 * @api {post} /logout Logout
 * @apiName Logout
 * @apiGroup LoginSys
 *
 * @apiDescription Endpunkt zerstört eine vorhandene Session eines Benutzers, sofern vorhanden.
 *
 * @apiSampleRequest /logout
 *
 * @apiError Internal_Server_Error "Es ist ein Fehler aufgetreten.. Versuchen Sie es nocheinmal. Sollten weiterhin
 * Schwierigkeiten auftreten, wenden Sie sich an die Administration. #Fehler : Fehlercode"
 * @apiError Session_not_found "Es konnte keine Session auf Ihren Namen gefunden werden."
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Bad Internal Server Error
 *     { message : "Es ist ein Fehler aufgetreten.. Versuchen Sie es nocheinmal. Sollten weiterhin" +
 *                   "Schwierigkeiten auftreten, wenden Sie sich an die Administration. #Fehler : " + err,
 *           logout : false }
 *
 * @apiErrorExample {json} Session_not_found:
 *     HTTP/1.1 400 Bad Request
 *     { message : "Es konnte keine Session auf Ihren Namen gefunden werden." + err,
 *           logout : false }
 *
 * @apiSuccess {Boolean} logout Boolean Wert zum Bestätigen eines erfolgreichen Logouts
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     { message : "Sie wurden erfolgreich ausgeloggt.", logout: true }
 *
 * @apiVersion 1.2.0
 */
router.route("/logout").post(loginRoute.userLogOut);

/**
 * @api {post} /register Register
 * @apiName Register
 * @apiGroup LoginSys
 *
 * @apiSampleRequest /register
 *
 * @apiDescription Öffentlicher Endpunkt zum Anlegen von Benutzerkonten. Parameter werden verschlüsselt und entkodiert entgegengenommen.
 * Endpunkt verarbeitet einkommende Daten und sendet ein Bad Request aus, sollten ankommende Daten nicht den Vorgaben entsprechen.
 *
 * @apiParam {String} email Benutzerkonten E-Mail Adresse
 * @apiParam {String} password AES verschlüsseltes Benutzerkonten Passwort
 * @apiParam {String} forename Vorname des Benutzerkontos
 * @apiParam {String} surname Nachname des Benutzerkontos
 *
 * @apiError Invalid_Data "Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist ungültig.
 * Bitte korrigieren Sie Ihre Eingaben und versuchen Sie es erneut."
 * @apiError Email_already_exists "Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!"
 * @apiError UserGroup_not_found "Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden."
 * @apiError User_Create_failed "Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten."
 * @apiError Internal_Server_Error "An Error has occurred: %s"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "An Error has occurred: error" }
 *
 * @apiErrorExample {json} Email_already_exists:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!" }
 *
 * @apiErrorExample {json} UserGroup_not_found:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden." }
 *
 * @apiErrorExample {json} Invalid_Data:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten." }
 *
 *
 * @apiSuccess {Boolean} user Boolean Wert zum Bestätigen einer erfolgreichen/gescheiterten Registrierung.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     { message : "Ihre Registrierung war erfolgreich %s! Sie können sich nun in Ihr Benutzerkonto einloggen.", user : true }
 *
 * @apiVersion 1.2.0
 */
router.route("/register").post(loginRoute.userRegister);





/* * * * * * * * * U S E R   C R U D * * * * * * * * * * */
/**
 * @api {post} /user Create
 * @apiName Create
 * @apiGroup CRUD_USER
 *
 * @apiDescription Endpunkt zum Erstellen von Benutzerkonten. Daten werden sowohl im Client als auch im Endpunkt verschlüsselt
 * und verarbeitet.
 *
 * @apiParam {String} email Benutzerkonten E-Mail Adresse
 * @apiParam {String} password AES verschlüsseltes Benutzerkonten Passwort
 * @apiParam {String} forename Vorname des Benutzerkontos
 * @apiParam {String} surname Nachname des Benutzerkontos
 *
 * @apiError Invalid_Data "Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist ungültig.
 * Bitte korrigieren Sie Ihre Eingaben und versuchen Sie es erneut."
 * @apiError Email_already_exists "Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!"
 * @apiError UserGroup_not_found "Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden."
 * @apiError User_Create_failed "Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten."
 * @apiError Internal_Server_Error "An Error has occurred: %s"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "An Error has occurred: error" }
 *
 * @apiErrorExample {json} Email_already_exists:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!" }
 *
 * @apiErrorExample {json} UserGroup_not_found:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden." }
 *
 * @apiErrorExample {json} Invalid_Data:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten." }
 *
 * @apiSuccess {User[]} user Nested Array-JSON [{user{ID, nickname, forename, surname, email}]
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 CREATED
 *     { message: "Der Nutzer user.nickname konnte erfolgreich hinzugefügt werden!",
 *     userList:
 *     [
 *          {
 *              "forename":"Sebastian",
 *              "surname":"Enns",
 *              "nickname":"datsepp",
 *              "email":"datsepp@gmail.com",
 *           }
 *      ]}
 *
 * @apiVersion 1.2.0
 */
router.route("/createUser").post(userCRUD_Route.userCreate);

/**
 * @api {get} /userList Read All
 * @apiName Read All
 * @apiGroup CRUD_USER
 * @apiPermission admin
 *
 * @apiDescription Endpunkt gibt alle bereits vorhandenen Benutzerkonten samt wichtigen Nutzeroberflächendaten aus.
 *
 * @apiError User_Not_Found "Es konnten keine Benutzerkonten in der Nutzerliste gefunden werden."
 * @apiError Internal_Server_Error "An Error has occurred: error"
 * @apiError Permission_denied "401 - Unauthorized"
 *
 * @apiErrorExample {json} Permission_denied:
 *     HTTP/1.1 401 Unauthorized
 *     { "message": "401 - Unauthorized" }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "An Error has occurred: error" }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Es konnten keine Benutzerkonten in der Nutzerliste gefunden werden." }
 *
 * @apiSuccess {UserList[]} userList Nested Array-JSON [{user{ID, forename, surname, email}}]
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {message: "Die Benutzerliste konnte erfolgreich geladen werden.",
 *     userList:
 *     [
 *          {
 *              "ID":1,
 *              "forename":"Sebastian",
 *              "surname":"Enns",
 *              "nickname":"datsepp",
 *              "email":"datsepp@gmail.com",
 *              "usergroup_label":"Admin"
 *           }
 *      ]}
 *
 * @apiVersion 1.2.0
 */
router.route("/userList").get(userCRUD_Route.userList);

/**
 * @api {get} /user/:id Read
 * @apiName Read
 * @apiGroup CRUD_USER
 * @apiPermission admin
 * @apiPermission user.ID
 *
 * @apiDescription Endpunkt zum Auslesen aller Nutzerdaten eines Benutzerkontos. Personengeschützte Daten werden nicht
 * übermittelt. Diese Datensätze bleiben 'null'.
 *
 * @apiError No_ID_delivered "Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben."
 * @apiError Internal_Server_Error "An Error has occurred: error"
 * @apiError User_not_found "Es konnte kein Nutzer mit der ID gefunden werden."
 * @apiError Permission_denied "401 - Unauthorized"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "An Error has occurred: error" }
 *
 * @apiErrorExample {json} User_not_found:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Es konnte kein Nutzer mit der ID gefunden werden." }
 *
 * @apiErrorExample {json} No_ID_delivered:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben." }
 *
 * @apiErrorExample {json} Permission_denied:
 *     HTTP/1.1 401 Unauthorized
 *     { "message": "401 - Unauthorized" }
 *
 * @apiParam {Number} id ID eines Benutzerkontos.
 *
 *
 * @apiSuccess {User[]} user Nested Array-JSON [{user{ID, forename, surname, email}}]
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {message: "Der Nutzer mit der ID %s konnte erfolgreich gefunden werden.",
 *     user:
 *     [
 *           {
 *             "ID": 3,
 *             "forename": "Sebastian",
 *             "surname": "Enns",
 *             "nickname": "datsepp",
 *             "email": "datsepp@gmail.com",
 *             "password": null,
 *             "usergroup_label": "Admin",
 *             "street": "Gierlichstra%C3%9Fe",
 *             "housenumber": "25",
 *             "postcode": "35683",
 *             "town": "Dillenburg",
 *             "telefone": "02771%20819441",
 *             "mobile": "01631524710",
 *             "fax": "",
 *             "date_of_birth": "",
 *             "IBAN": "",
 *             "BIC": ""
 *           }
 *      ]}
 *
 * @apiVersion 1.2.0
 */
router.route("/user/:id").get(userCRUD_Route.userGetByID);

/**
 * @api {put} /user/:id Update
 * @apiName Update
 * @apiGroup CRUD_USER
 * @apiPermission admin
 * @apiPermission user.ID
 *
 * @apiDescription Endpunkt zum Aktualisieren von Nutzerdaten.
 *
 * @apiError No_ID_delivered "Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben."
 * @apiError Internal_Server_Error "An Error has occurred: error"
 * @apiError Email_already_exists "Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!"
 * @apiError User_not_found "Es konnte kein Nutzer mit der ID gefunden werden."
 * @apiError Permission_denied "401 - Unauthorized"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "An Error has occurred: error" }
 *
 * @apiErrorExample {json} Nothing_has_changed:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Es wurden keine Änderungen an dem Nutzer vorgenommen." }
 *
 * @apiErrorExample {json} No_ID_delivered:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben." }
 *
 * @apiErrorExample {json} Email_already_exists:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!" }
 *
 * @apiErrorExample {json} Permission_denied:
 *     HTTP/1.1 401 Unauthorized
 *     { "message": "401 - Unauthorized" }
 *
 * @apiParam {Number} id ID eines Benutzerkontos.
 * @apiParam {Any} user Array Objekt mit Nutzerdaten, die aktualisiert werden müssen.
 *
 * @apiSuccess {User[]} user Nested Array-JSON mit aktualisierten Nutzerdaten.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     { message: "Der Nutzer mit der ID %s konnte erfolgreich gefunden werden.",
 *     user:
 *     [
 *           {
 *             "ID": 3,
 *             "forename": "Sebastian",
 *             "surname": "Enns",
 *             "nickname": "datsepp",
 *             "email": "datsepp@gmail.com",
 *             "password": null,
 *             "usergroup_label": "Admin",
 *             "street": "Gierlichstra%C3%9Fe",
 *             "housenumber": "25",
 *             "postcode": "35683",
 *             "town": "Dillenburg",
 *             "telefone": "02771%20819441",
 *             "mobile": "01631524710",
 *             "fax": "",
 *             "date_of_birth": "",
 *             "IBAN": "",
 *             "BIC": ""
 *           }
 *      ]}
 *
 * @apiVersion 1.2.0
 */
router.route("/user/:id").put(userCRUD_Route.userUpdateByID);

/**
 * @api {delete} /delete Delete
 * @apiName Delete
 * @apiGroup CRUD_USER
 * @apiPermission admin
 *
 * @apiDescription Endpunkt zum Löschen von Benutzerkonten.
 *
 * @apiError No_ID_delivered "Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben."
 * @apiError Internal_Server_Error "An Error has occurred: error"
 * @apiError Nothing_happened_Woopsy_Doopsy "Wooops, Doopsy, sooomy wooomy thingy went wrongyyy, its time to wibbly wobbly fixy it..."
 * @apiError User_not_found "Es konnte kein Nutzer mit der ID gefunden werden."
 * @apiError Permission_denied "401 - Unauthorized"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "An Error has occurred: error" }
 *
 * @apiErrorExample {json} Nothing_happened_Woopsy_Doopsy:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Wooops, Doopsy, sooomy wooomy thingy went wrongyyy, its time to wibbly wobbly fixy it..." }
 *
 * @apiErrorExample {json} No_ID_delivered:
 *     HTTP/1.1 400 Bad Request
 *     { "message": "Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben." }
 *
 * @apiErrorExample {json} Permission_denied:
 *     HTTP/1.1 401 Unauthorized
 *     { "message": "401 - Unauthorized" }
 *
 * @apiParam {Number} id ID eines Benutzerkontos.
 *
 * @apiSuccess {Any[]} Bestätigungsnachricht. :). // smile // comment
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     { message: "Der Nutzer wurde erfolgreich gelöscht." }
 *
 * @apiVersion 1.2.0
 */
router.route("/user/:id").delete(userCRUD_Route.userDeleteByID);





/* * * * * * * * * P R O D U C T   C R U D * * * * * * * * * * */
/**
 * @api {GET} /productList Read All
 * @apiName Read All
 * @apiGroup CRUD_PRODUCT
 *
 * @apiDescription Endpunkt zum Empfangen aller Produkte in der Datenbank samt technischen Spezifikationen, Bewertungen
 * und informativen Details.
 *
 * @apiError Internal_Server_Error "Select all error: error"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Select all error: error" }
 *
 * @apiSuccess {ProductList[]} productList Nested Array-JSON mit Produkten.
 *
 * @apiSuccessExample {json} No_products_found:
 *     HTTP/1.1 200 OK
 *     { "message": "Es wurden keine Produkte gefunden." }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       productList: [
 *         {
 *             "ID": 1,
 *             "price": 2699,
 *             "name": "Wacom Mobile Studio Pro 13 [512GB, i7]",
 *             "description": "Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)"
 *             "length": "36,7cm",
 *             "width": "1,6cm",
 *             "height": "22,9cm",
 *             "display_size": "13,3 Zoll",
 *             "resolution": "2560x1440",
 *             "advanced_controls": "8192 Druckpunkte",
 *             "productivity_supplies": "5",
 *             "processor": "Intel Core i7",
 *             "graphics_card": "Intel Iris Graphics 550",
 *             "memory": "512GB SSD",
 *             "product_type": "Pen-Computer",
 *             "micro_sd_slot": "1",
 *             "ram": "16GB DDR3",
 *             "extensible_memory": 1,
 *             "picture": "wacommobilstudiopro13.jpg"
 *         }]
 *     }
 *
 * @apiVersion 1.2.0
 */
router.route("/productList").get(productCrudRoute.productList);

/**
 * @api {POST} /createProduct Create
 * @apiName Create
 * @apiGroup CRUD_PRODUCT
 *
 * @apiDescription Endpunkt zum Erstellen von Produkten.
 *
 * @apiParam {Product[]} Product Array-Objekt mit allen benötigten Informationen
 *
 * @apiError Insert_Error "Insert error"
 * @apiError Internal_Server_Error "Select all error: error"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Select all error: error" }
 *
 * @apiErrorExample {json} Insert_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Insert error" }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 CREATED
 *     { message: "Produkt %s erfolgreich hinzugefügt!",
 *
 * @apiVersion 1.2.0
 */
router.route("/createProduct").post(productCrudRoute.createProduct); // TODO PERMISSIONS, IF BODY IS EMPTY?, PERFORMANCE

/**
 * @api {GET} /findProductById/:id Read
 * @apiName Read
 * @apiGroup CRUD_PRODUCT
 *
 * @apiDescription Endpunkt zum Auslesen eines Produktes.
 *
 * @apiParam {Number} id ID eines Produktes.
 *
 * @apiError Internal_Server_Error "Select all error: error"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Select all error: error" }
 *
 *
 * @apiSuccess {Product[]} productList Nested Array-JSON mit einem Produkt.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 CREATED
 *     {  productList: [
 *         {
 *             "ID": 1,
 *             "price": 2699,
 *             "name": "Wacom Mobile Studio Pro 13 [512GB, i7]",
 *             "description": "Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)"
 *             "length": "36,7cm",
 *             "width": "1,6cm",
 *             "height": "22,9cm",
 *             "display_size": "13,3 Zoll",
 *             "resolution": "2560x1440",
 *             "advanced_controls": "8192 Druckpunkte",
 *             "productivity_supplies": "5",
 *             "processor": "Intel Core i7",
 *             "graphics_card": "Intel Iris Graphics 550",
 *             "memory": "512GB SSD",
 *             "product_type": "Pen-Computer",
 *             "micro_sd_slot": "1",
 *             "ram": "16GB DDR3",
 *             "extensible_memory": 1,
 *             "picture": "wacommobilstudiopro13.jpg"
 *         }]
 *     }
 *
 * @apiVersion 1.2.0
 */
router.route("/findProductById/:id").get(productCrudRoute.findProductById); // TODO IF BODY IS EMPTY, PERFORMANCE

/**
 * @api {PUT} /updateProductById/:id Update
 * @apiName Update
 * @apiGroup CRUD_PRODUCT
 *
 * @apiDescription Endpunkt zum Ändern eines Produktes.
 *
 * @apiParam {Number} id ID eines Produktes.
 *
 * @apiError Insert_Error "Insert error"
 * @apiError Internal_Server_Error "Select all error: error"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Select all error: error" }
 *
 * @apiErrorExample {json} Insert_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Insert error" }
 *
 * @apiSuccess {ProductList[]} productList Nested Array-JSON mit allen Produkten
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 CREATED
 *     { message: "Produkt erfolgreich geändert!",
 *       productList: [
 *         {
 *             "ID": 1,
 *             "price": 2699,
 *             "name": "Wacom Mobile Studio Pro 13 [512GB, i7]",
 *             "description": "Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)"
 *             "length": "36,7cm",
 *             "width": "1,6cm",
 *             "height": "22,9cm",
 *             "display_size": "13,3 Zoll",
 *             "resolution": "2560x1440",
 *             "advanced_controls": "8192 Druckpunkte",
 *             "productivity_supplies": "5",
 *             "processor": "Intel Core i7",
 *             "graphics_card": "Intel Iris Graphics 550",
 *             "memory": "512GB SSD",
 *             "product_type": "Pen-Computer",
 *             "micro_sd_slot": "1",
 *             "ram": "16GB DDR3",
 *             "extensible_memory": 1,
 *             "picture": "wacommobilstudiopro13.jpg"
 *         }]
 *     }
 *
 * @apiVersion 1.2.0
 */
router.route("/updateProductById/:id").put(productCrudRoute.updateProduct); // TODO PERMISSIONS, IF BODY IS EMPTY, PERFORMANCE

/**
 * @api {DELETE} /deleteProductById/:id Delete
 * @apiName Delete
 * @apiGroup CRUD_PRODUCT
 *
 * @apiDescription Endpunkt zum Löschen eines Produktes.
 *
 * @apiParam {Number} id ID eines Produktes.
 *
 * @apiError Internal_Server_Error "Select all error: error"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Select all error: error" }
 *
 * @apiSuccess {Product[]} productList Nested Array-JSON mit einem Produkt.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 CREATED
 *     {  message: "Product mit der ID %s erfolgreich gelöscht.",
 *     productList: [
 *         {
 *             "ID": 1,
 *             "price": 2699,
 *             "name": "Wacom Mobile Studio Pro 13 [512GB, i7]",
 *             "description": "Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)"
 *             "length": "36,7cm",
 *             "width": "1,6cm",
 *             "height": "22,9cm",
 *             "display_size": "13,3 Zoll",
 *             "resolution": "2560x1440",
 *             "advanced_controls": "8192 Druckpunkte",
 *             "productivity_supplies": "5",
 *             "processor": "Intel Core i7",
 *             "graphics_card": "Intel Iris Graphics 550",
 *             "memory": "512GB SSD",
 *             "product_type": "Pen-Computer",
 *             "micro_sd_slot": "1",
 *             "ram": "16GB DDR3",
 *             "extensible_memory": 1,
 *             "picture": "wacommobilstudiopro13.jpg"
 *         }]
 *     }
 *
 * @apiVersion 1.2.0
 */
router.route("/deleteProductById/:id").delete(productCrudRoute.deleteProduct); // TODO PERMISSIONS





/* * * * * * * * * F A Q L I S T * * * * * * * * * * */
/**
 * @api {GET} /faq/:category Read
 * @apiName Read
 * @apiGroup FAQ
 *
 * @apiDescription Endpunkt zum Auslesen der häufig gestellten Fragen in einer Kategorie.
 *
 * @apiParam {String} category Kategorien: rent, delivery, reapir
 *
 * @apiError No_Data "Nothing found."
 * @apiError Internal_Server_Error "Error: error"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Error: error" }
 *
 * @apiErrorExample {json} Email_already_exists:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "Nothing found." }
 *
 * @apiSuccess {FAQ[]} faq Nested Array-JSON mit Fragen und Antworten
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 CREATED
 *      {
 *          "faq": {
 *             "faq": [
 *                 {
 *                   "questi": "Wie lange kann ich einen Artikel mieten?",
 *                   "answer": "Sie können zwischen verschiedenen Laufzeiten wählen..."
 *                 },
 *                 {
 *                   "questi": "Kann ich die Mietzeit verlängern?",
 *                   "answer": "Ja, das geht..."
 *                 }
 *             ]}
 *          }
 *       }
 *
 *
 * @apiVersion 1.2.0
 */
router.route("/FAQ/:category").get(faqRoute.FAQ);





/* * * * * * * * * P R I C E C A L C U L A T O R * * * * * * * * * * */
/**
 * @api {GET} /calc Calculator
 * @apiName Calculator
 * @apiGroup PRICING
 *
 * @apiDescription Endpunkt zum Berechnen von Mietpreisen für Wochen und Monate
 *
 * @apiParam {Number} time Mietzeit in Tagen - Gültig sind 7er Schritte für Wochen und 30er Schritte für Monate
 * @apiParam {Number} price Neupreis eines Produktes
 *
 * @apiError Internal_Server_Error "500 iNtErNaL sErVeR ErRoRrRr"
 *
 * @apiErrorExample {json} Internal_Server_Error:
 *     HTTP/1.1 500 Internal Server Error
 *     { "message": "500 iNtErNaL sErVeR ErRoRrRr" }
 *
 * @apiSuccess {Number} calc Wochen/Monatspreis für einen Artikel
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 CREATED
 *     { calc : 99 }
 *
 * @apiVersion 1.2.0
 */
router.route("/calc").get(calcRoute.calcPriceOfProduct);


