module.exports = {
    SQL_LOGIN_SELECT : "SELECT user.ID,nickname, forename, surname, usergroup_label, user.ID_producer FROM user, usergroup " +
                        "WHERE user.email='%s' AND user.ID_usergroup = usergroup.ID;",

    SUCC_LOGOUT : "Sie wurden erfolgreich aus Ihrem Benutzerkonto ausgeloggt.",
    SUCC_REGISTER : "Ihre Registrierung war erfolgreich %s! Sie können sich nun in Ihr Benutzerkonto einloggen.",
    SUCC_LOGIN : "Sie haben sich erfolgreich eingeloggt.",

    ERR_LOGIN : "Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist nicht korrekt.",
    ERR_LOGOUT : "Es ist ein Fehler aufgetreten.. Versuchen Sie sich nochmal auszuloggen. Sollten weiterhin Schwierigkeiten auftreten, " +
    "wenden Sie sich an die Administration. #Fehler : %s\",",
    ERR_LOGOUT_MISSING_SESSION : "Es konnte keine Session auf Ihren Namen gefunden werden.",
    ERR_INVALIDA_DATA : "Die eingegebene E-Mail Adresse " +
    "oder das eingegebene Passwort ist ungültig. Bitte korrigieren Sie Ihre Eingaben und versuchen Sie es erneut.",

    ERR_CATCH : "An Error has occurred: %s",

    USERGROUP_ADMIN : "Admin",
    USERGROUP_PROVIDER : "Anbieter",
};