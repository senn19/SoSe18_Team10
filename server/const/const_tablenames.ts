module.exports = {
    // tablenames
    TABLE_ADDRESS: "address",
    TABLE_CATEGORIES: "categories",
    TABLE_FAQ_CATEGORIES: "faq_categories",
    TABLE_FAQ_QUESTIONS_ANSWERS: "faq_questions_answers",
    TABLE_ORDER_WISHLIST: "order_wishlist",
    TABLE_PRIVILEG: "privileg",
    TABLE_PRODUCER: "producer",
    TABLE_PRODUCT: "product",
    TABLE_TEMPLATE: "template",
    TABLE_USER: "user",
    TABLE_USERGRUOP: "usergroup",
    TABLE_USERGROUP_PRIVILEG: "usergroup_privileg",
};