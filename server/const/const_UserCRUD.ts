module.exports = {
    SQL_USERLIST_SELECT : "SELECT user.ID, forename, surname, nickname, email, usergroup_label FROM user, usergroup " +
    "WHERE usergroup.ID = user.ID_usergroup;",
    SQL_EMAIL_SELECT : "SELECT * FROM user WHERE email='%s';",
    SQL_UPDATE_EMAIL_SELECT : "SELECT * FROM user WHERE email='%s' AND NOT user.ID = %s;",
    SQL_USER_INSERT : "INSERT INTO user(forename, surname, nickname, email, password, ID_usergroup) VALUES('%s','%s','%s','%s','%s','%s');",
    SQL_ADDRESS_INSERT : "INSERT INTO address(ID_user) VALUES(%s)",
    SQL_GROUPLABEL_SELECT : "SELECT ID from usergroup WHERE usergroup_label = '%s'",
    SQL_USER_ADDRESS_SELECT : "SELECT * FROM user, address, usergroup WHERE user.ID =%s AND user.ID = address.ID_user " +
    "AND user.ID_usergroup = usergroup.ID;",
    SQL_USER_DELETE_ADDRESS : "DELETE FROM address WHERE address.ID_user = %s",
    SQL_USER_DELETE_USER : "DELETE FROM user WHERE user.ID = %s",

    ERR_EMAIL_EXIST : "Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!",
    ERR_GROUP_LABEL : "Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden.",
    ERR_USER_CREATE : "Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten.",
    ERR_USER_NOT_FOUND : "Es konnten keine Benutzerkonten in der Nutzerliste gefunden werden.",
    ERR_CATCH : "An Error has occurred: %s",
    ERR_NO_EMAIL : "Nutzer kann nicht ohne eine E-Mail Adresse erzeugt werden.",
    ERR_NO_USERID : "Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.",
    ERR_NO_USER_WITH_ID : "Es konnte kein Nutzer mit der ID gefunden werden.",
    ERR_DATA_NOTUPDATED : "Es wurden keine Änderungen an dem Nutzer vorgenommen.",
    ERR_USER_DELETE : "Wooops, Doopsy, sooomy wooomy thingy went wrongyyy, its time to wibbly wobbly fixy it...",
    ERR_NO_PERMISSION : "401 - Unauthorized",

    SUCC_USER_CREATE : "Der Nutzer %s konnte erfolgreich hinzugefügt werden!",
    SUCC_USER_LOAD : "Die Benutzerliste konnte erfolgreich geladen werden.",
    SUCC_USER_FOUND : "Der Nutzer mit der ID %s konnte erfolgreich gefunden werden.",
    SUCC_USER_DELETE : "Der Nutzer wurde erfolgreich gelöscht.",
    SUCC_USER_UPDATED : "Der ausgewählte Nutzer wurde erfolgreich aktualisiert.",

    LABEL_ADMIN : "Admin",
    LABEL_USER : "Nutzer",
    LABEL_PROVIDER : "Anbieter",
    LABEL_MASTER : "Portalbetreiber",
};

