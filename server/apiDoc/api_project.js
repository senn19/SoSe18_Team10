define({
  "name": "CreativeMinds API",
  "version": "1.0.0",
  "description": "RESTful web-API",
  "title": "CreativeMinds - RESTful web-API",
  "url": "http://localhost:8080",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-07-07T19:34:28.659Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
