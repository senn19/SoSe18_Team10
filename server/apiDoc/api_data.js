define({ "api": [
  {
    "type": "POST",
    "url": "/createProduct",
    "title": "Create",
    "name": "Create",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Erstellen von Produkten.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Product[]",
            "optional": false,
            "field": "Product",
            "description": "<p>Array-Objekt mit allen benötigten Informationen</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Insert_Error",
            "description": "<p>&quot;Insert error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        },
        {
          "title": "Insert_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Insert error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{ message: \"Produkt %s erfolgreich hinzugefügt!\",",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "POST",
    "url": "/createProduct",
    "title": "Create",
    "name": "Create",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Erstellen von Produkten.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Product[]",
            "optional": false,
            "field": "Product",
            "description": "<p>Array-Objekt mit allen benötigten Informationen</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Insert_Error",
            "description": "<p>&quot;Insert error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        },
        {
          "title": "Insert_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Insert error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{ message: \"Produkt %s erfolgreich hinzugefügt!\",",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "DELETE",
    "url": "/deleteProductById/:id",
    "title": "Delete",
    "name": "Delete",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Löschen eines Produktes.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Produktes.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Product[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON mit einem Produkt.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{  message: \"Product mit der ID %s erfolgreich gelöscht.\",\nproductList: [\n    {\n        \"ID\": 1,\n        \"price\": 2699,\n        \"name\": \"Wacom Mobile Studio Pro 13 [512GB, i7]\",\n        \"description\": \"Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)\"\n        \"length\": \"36,7cm\",\n        \"width\": \"1,6cm\",\n        \"height\": \"22,9cm\",\n        \"display_size\": \"13,3 Zoll\",\n        \"resolution\": \"2560x1440\",\n        \"advanced_controls\": \"8192 Druckpunkte\",\n        \"productivity_supplies\": \"5\",\n        \"processor\": \"Intel Core i7\",\n        \"graphics_card\": \"Intel Iris Graphics 550\",\n        \"memory\": \"512GB SSD\",\n        \"product_type\": \"Pen-Computer\",\n        \"micro_sd_slot\": \"1\",\n        \"ram\": \"16GB DDR3\",\n        \"extensible_memory\": 1,\n        \"picture\": \"wacommobilstudiopro13.jpg\"\n    }]\n}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "DELETE",
    "url": "/deleteProductById/:id",
    "title": "Delete",
    "name": "Delete",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Löschen eines Produktes.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Produktes.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Product[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON mit einem Produkt.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{  message: \"Product mit der ID %s erfolgreich gelöscht.\",\nproductList: [\n    {\n        \"ID\": 1,\n        \"price\": 2699,\n        \"name\": \"Wacom Mobile Studio Pro 13 [512GB, i7]\",\n        \"description\": \"Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)\"\n        \"length\": \"36,7cm\",\n        \"width\": \"1,6cm\",\n        \"height\": \"22,9cm\",\n        \"display_size\": \"13,3 Zoll\",\n        \"resolution\": \"2560x1440\",\n        \"advanced_controls\": \"8192 Druckpunkte\",\n        \"productivity_supplies\": \"5\",\n        \"processor\": \"Intel Core i7\",\n        \"graphics_card\": \"Intel Iris Graphics 550\",\n        \"memory\": \"512GB SSD\",\n        \"product_type\": \"Pen-Computer\",\n        \"micro_sd_slot\": \"1\",\n        \"ram\": \"16GB DDR3\",\n        \"extensible_memory\": 1,\n        \"picture\": \"wacommobilstudiopro13.jpg\"\n    }]\n}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "GET",
    "url": "/findProductById/:id",
    "title": "Read",
    "name": "Read",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Auslesen eines Produktes.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Produktes.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Product[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON mit einem Produkt.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{  productList: [\n    {\n        \"ID\": 1,\n        \"price\": 2699,\n        \"name\": \"Wacom Mobile Studio Pro 13 [512GB, i7]\",\n        \"description\": \"Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)\"\n        \"length\": \"36,7cm\",\n        \"width\": \"1,6cm\",\n        \"height\": \"22,9cm\",\n        \"display_size\": \"13,3 Zoll\",\n        \"resolution\": \"2560x1440\",\n        \"advanced_controls\": \"8192 Druckpunkte\",\n        \"productivity_supplies\": \"5\",\n        \"processor\": \"Intel Core i7\",\n        \"graphics_card\": \"Intel Iris Graphics 550\",\n        \"memory\": \"512GB SSD\",\n        \"product_type\": \"Pen-Computer\",\n        \"micro_sd_slot\": \"1\",\n        \"ram\": \"16GB DDR3\",\n        \"extensible_memory\": 1,\n        \"picture\": \"wacommobilstudiopro13.jpg\"\n    }]\n}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "GET",
    "url": "/findProductById/:id",
    "title": "Read",
    "name": "Read",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Auslesen eines Produktes.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Produktes.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Product[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON mit einem Produkt.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{  productList: [\n    {\n        \"ID\": 1,\n        \"price\": 2699,\n        \"name\": \"Wacom Mobile Studio Pro 13 [512GB, i7]\",\n        \"description\": \"Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)\"\n        \"length\": \"36,7cm\",\n        \"width\": \"1,6cm\",\n        \"height\": \"22,9cm\",\n        \"display_size\": \"13,3 Zoll\",\n        \"resolution\": \"2560x1440\",\n        \"advanced_controls\": \"8192 Druckpunkte\",\n        \"productivity_supplies\": \"5\",\n        \"processor\": \"Intel Core i7\",\n        \"graphics_card\": \"Intel Iris Graphics 550\",\n        \"memory\": \"512GB SSD\",\n        \"product_type\": \"Pen-Computer\",\n        \"micro_sd_slot\": \"1\",\n        \"ram\": \"16GB DDR3\",\n        \"extensible_memory\": 1,\n        \"picture\": \"wacommobilstudiopro13.jpg\"\n    }]\n}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "GET",
    "url": "/productList",
    "title": "Read All",
    "name": "Read_All",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Empfangen aller Produkte in der Datenbank samt technischen Spezifikationen, Bewertungen und informativen Details.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ProductList[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON mit Produkten.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "No_products_found:",
          "content": "HTTP/1.1 200 OK\n{ \"message\": \"Es wurden keine Produkte gefunden.\" }",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  productList: [\n    {\n        \"ID\": 1,\n        \"price\": 2699,\n        \"name\": \"Wacom Mobile Studio Pro 13 [512GB, i7]\",\n        \"description\": \"Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)\"\n        \"length\": \"36,7cm\",\n        \"width\": \"1,6cm\",\n        \"height\": \"22,9cm\",\n        \"display_size\": \"13,3 Zoll\",\n        \"resolution\": \"2560x1440\",\n        \"advanced_controls\": \"8192 Druckpunkte\",\n        \"productivity_supplies\": \"5\",\n        \"processor\": \"Intel Core i7\",\n        \"graphics_card\": \"Intel Iris Graphics 550\",\n        \"memory\": \"512GB SSD\",\n        \"product_type\": \"Pen-Computer\",\n        \"micro_sd_slot\": \"1\",\n        \"ram\": \"16GB DDR3\",\n        \"extensible_memory\": 1,\n        \"picture\": \"wacommobilstudiopro13.jpg\"\n    }]\n}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "GET",
    "url": "/productList",
    "title": "Read All",
    "name": "Read_All",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Empfangen aller Produkte in der Datenbank samt technischen Spezifikationen, Bewertungen und informativen Details.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ProductList[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON mit Produkten.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "No_products_found:",
          "content": "HTTP/1.1 200 OK\n{ \"message\": \"Es wurden keine Produkte gefunden.\" }",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  productList: [\n    {\n        \"ID\": 1,\n        \"price\": 2699,\n        \"name\": \"Wacom Mobile Studio Pro 13 [512GB, i7]\",\n        \"description\": \"Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)\"\n        \"length\": \"36,7cm\",\n        \"width\": \"1,6cm\",\n        \"height\": \"22,9cm\",\n        \"display_size\": \"13,3 Zoll\",\n        \"resolution\": \"2560x1440\",\n        \"advanced_controls\": \"8192 Druckpunkte\",\n        \"productivity_supplies\": \"5\",\n        \"processor\": \"Intel Core i7\",\n        \"graphics_card\": \"Intel Iris Graphics 550\",\n        \"memory\": \"512GB SSD\",\n        \"product_type\": \"Pen-Computer\",\n        \"micro_sd_slot\": \"1\",\n        \"ram\": \"16GB DDR3\",\n        \"extensible_memory\": 1,\n        \"picture\": \"wacommobilstudiopro13.jpg\"\n    }]\n}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "PUT",
    "url": "/updateProductById/:id",
    "title": "Update",
    "name": "Update",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Ändern eines Produktes.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Produktes.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Insert_Error",
            "description": "<p>&quot;Insert error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        },
        {
          "title": "Insert_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Insert error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ProductList[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON mit allen Produkten</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{ message: \"Produkt erfolgreich geändert!\",\n  productList: [\n    {\n        \"ID\": 1,\n        \"price\": 2699,\n        \"name\": \"Wacom Mobile Studio Pro 13 [512GB, i7]\",\n        \"description\": \"Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)\"\n        \"length\": \"36,7cm\",\n        \"width\": \"1,6cm\",\n        \"height\": \"22,9cm\",\n        \"display_size\": \"13,3 Zoll\",\n        \"resolution\": \"2560x1440\",\n        \"advanced_controls\": \"8192 Druckpunkte\",\n        \"productivity_supplies\": \"5\",\n        \"processor\": \"Intel Core i7\",\n        \"graphics_card\": \"Intel Iris Graphics 550\",\n        \"memory\": \"512GB SSD\",\n        \"product_type\": \"Pen-Computer\",\n        \"micro_sd_slot\": \"1\",\n        \"ram\": \"16GB DDR3\",\n        \"extensible_memory\": 1,\n        \"picture\": \"wacommobilstudiopro13.jpg\"\n    }]\n}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "PUT",
    "url": "/updateProductById/:id",
    "title": "Update",
    "name": "Update",
    "group": "CRUD_PRODUCT",
    "description": "<p>Endpunkt zum Ändern eines Produktes.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Produktes.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Insert_Error",
            "description": "<p>&quot;Insert error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Select all error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: error\" }",
          "type": "json"
        },
        {
          "title": "Insert_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Insert error\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ProductList[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON mit allen Produkten</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{ message: \"Produkt erfolgreich geändert!\",\n  productList: [\n    {\n        \"ID\": 1,\n        \"price\": 2699,\n        \"name\": \"Wacom Mobile Studio Pro 13 [512GB, i7]\",\n        \"description\": \"Das Wacom MobileStudio Pro 13 bietet ein gesamtes Atelier (...)\"\n        \"length\": \"36,7cm\",\n        \"width\": \"1,6cm\",\n        \"height\": \"22,9cm\",\n        \"display_size\": \"13,3 Zoll\",\n        \"resolution\": \"2560x1440\",\n        \"advanced_controls\": \"8192 Druckpunkte\",\n        \"productivity_supplies\": \"5\",\n        \"processor\": \"Intel Core i7\",\n        \"graphics_card\": \"Intel Iris Graphics 550\",\n        \"memory\": \"512GB SSD\",\n        \"product_type\": \"Pen-Computer\",\n        \"micro_sd_slot\": \"1\",\n        \"ram\": \"16GB DDR3\",\n        \"extensible_memory\": 1,\n        \"picture\": \"wacommobilstudiopro13.jpg\"\n    }]\n}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_PRODUCT"
  },
  {
    "type": "post",
    "url": "/user",
    "title": "Create",
    "name": "Create",
    "group": "CRUD_USER",
    "description": "<p>Endpunkt zum Erstellen von Benutzerkonten. Daten werden sowohl im Client als auch im Endpunkt verschlüsselt und verarbeitet.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Benutzerkonten E-Mail Adresse</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>AES verschlüsseltes Benutzerkonten Passwort</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "forename",
            "description": "<p>Vorname des Benutzerkontos</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "surname",
            "description": "<p>Nachname des Benutzerkontos</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Invalid_Data",
            "description": "<p>&quot;Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist ungültig. Bitte korrigieren Sie Ihre Eingaben und versuchen Sie es erneut.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Email_already_exists",
            "description": "<p>&quot;Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserGroup_not_found",
            "description": "<p>&quot;Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_Create_failed",
            "description": "<p>&quot;Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: %s&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Email_already_exists:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!\" }",
          "type": "json"
        },
        {
          "title": "UserGroup_not_found:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden.\" }",
          "type": "json"
        },
        {
          "title": "Invalid_Data:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten.\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "User[]",
            "optional": false,
            "field": "user",
            "description": "<p>Nested Array-JSON [{user{ID, nickname, forename, surname, email}]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{ message: \"Der Nutzer user.nickname konnte erfolgreich hinzugefügt werden!\",\nuserList:\n[\n     {\n         \"forename\":\"Sebastian\",\n         \"surname\":\"Enns\",\n         \"nickname\":\"datsepp\",\n         \"email\":\"datsepp@gmail.com\",\n      }\n ]}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_USER"
  },
  {
    "type": "post",
    "url": "/user",
    "title": "Create",
    "name": "Create",
    "group": "CRUD_USER",
    "description": "<p>Endpunkt zum Erstellen von Benutzerkonten. Daten werden sowohl im Client als auch im Endpunkt verschlüsselt und verarbeitet.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Benutzerkonten E-Mail Adresse</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>AES verschlüsseltes Benutzerkonten Passwort</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "forename",
            "description": "<p>Vorname des Benutzerkontos</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "surname",
            "description": "<p>Nachname des Benutzerkontos</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Invalid_Data",
            "description": "<p>&quot;Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist ungültig. Bitte korrigieren Sie Ihre Eingaben und versuchen Sie es erneut.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Email_already_exists",
            "description": "<p>&quot;Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserGroup_not_found",
            "description": "<p>&quot;Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_Create_failed",
            "description": "<p>&quot;Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: %s&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Email_already_exists:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!\" }",
          "type": "json"
        },
        {
          "title": "UserGroup_not_found:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden.\" }",
          "type": "json"
        },
        {
          "title": "Invalid_Data:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten.\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "User[]",
            "optional": false,
            "field": "user",
            "description": "<p>Nested Array-JSON [{user{ID, nickname, forename, surname, email}]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{ message: \"Der Nutzer user.nickname konnte erfolgreich hinzugefügt werden!\",\nuserList:\n[\n     {\n         \"forename\":\"Sebastian\",\n         \"surname\":\"Enns\",\n         \"nickname\":\"datsepp\",\n         \"email\":\"datsepp@gmail.com\",\n      }\n ]}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_USER"
  },
  {
    "type": "delete",
    "url": "/delete",
    "title": "Delete",
    "name": "Delete",
    "group": "CRUD_USER",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>Endpunkt zum Löschen von Benutzerkonten.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No_ID_delivered",
            "description": "<p>&quot;Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Nothing_happened_Woopsy_Doopsy",
            "description": "<p>&quot;Wooops, Doopsy, sooomy wooomy thingy went wrongyyy, its time to wibbly wobbly fixy it...&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_not_found",
            "description": "<p>&quot;Es konnte kein Nutzer mit der ID gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Permission_denied",
            "description": "<p>&quot;401 - Unauthorized&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Nothing_happened_Woopsy_Doopsy:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Wooops, Doopsy, sooomy wooomy thingy went wrongyyy, its time to wibbly wobbly fixy it...\" }",
          "type": "json"
        },
        {
          "title": "No_ID_delivered:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.\" }",
          "type": "json"
        },
        {
          "title": "Permission_denied:",
          "content": "HTTP/1.1 401 Unauthorized\n{ \"message\": \"401 - Unauthorized\" }",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Benutzerkontos.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Any[]",
            "optional": false,
            "field": "Best",
            "description": "<p>ätigungsnachricht. :). // smile // comment</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ message: \"Der Nutzer wurde erfolgreich gelöscht.\" }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_USER"
  },
  {
    "type": "delete",
    "url": "/delete",
    "title": "Delete",
    "name": "Delete",
    "group": "CRUD_USER",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>Endpunkt zum Löschen von Benutzerkonten.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No_ID_delivered",
            "description": "<p>&quot;Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Nothing_happened_Woopsy_Doopsy",
            "description": "<p>&quot;Wooops, Doopsy, sooomy wooomy thingy went wrongyyy, its time to wibbly wobbly fixy it...&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_not_found",
            "description": "<p>&quot;Es konnte kein Nutzer mit der ID gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Permission_denied",
            "description": "<p>&quot;401 - Unauthorized&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Nothing_happened_Woopsy_Doopsy:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Wooops, Doopsy, sooomy wooomy thingy went wrongyyy, its time to wibbly wobbly fixy it...\" }",
          "type": "json"
        },
        {
          "title": "No_ID_delivered:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.\" }",
          "type": "json"
        },
        {
          "title": "Permission_denied:",
          "content": "HTTP/1.1 401 Unauthorized\n{ \"message\": \"401 - Unauthorized\" }",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Benutzerkontos.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Any[]",
            "optional": false,
            "field": "Best",
            "description": "<p>ätigungsnachricht. :). // smile // comment</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ message: \"Der Nutzer wurde erfolgreich gelöscht.\" }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_USER"
  },
  {
    "type": "get",
    "url": "/user/:id",
    "title": "Read",
    "name": "Read",
    "group": "CRUD_USER",
    "permission": [
      {
        "name": "admin"
      },
      {
        "name": "user.ID"
      }
    ],
    "description": "<p>Endpunkt zum Auslesen aller Nutzerdaten eines Benutzerkontos. Personengeschützte Daten werden nicht übermittelt. Diese Datensätze bleiben 'null'.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No_ID_delivered",
            "description": "<p>&quot;Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_not_found",
            "description": "<p>&quot;Es konnte kein Nutzer mit der ID gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Permission_denied",
            "description": "<p>&quot;401 - Unauthorized&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "User_not_found:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es konnte kein Nutzer mit der ID gefunden werden.\" }",
          "type": "json"
        },
        {
          "title": "No_ID_delivered:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.\" }",
          "type": "json"
        },
        {
          "title": "Permission_denied:",
          "content": "HTTP/1.1 401 Unauthorized\n{ \"message\": \"401 - Unauthorized\" }",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Benutzerkontos.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "User[]",
            "optional": false,
            "field": "user",
            "description": "<p>Nested Array-JSON [{user{ID, forename, surname, email}}]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{message: \"Der Nutzer mit der ID %s konnte erfolgreich gefunden werden.\",\nuser:\n[\n      {\n        \"ID\": 3,\n        \"forename\": \"Sebastian\",\n        \"surname\": \"Enns\",\n        \"nickname\": \"datsepp\",\n        \"email\": \"datsepp@gmail.com\",\n        \"password\": null,\n        \"usergroup_label\": \"Admin\",\n        \"street\": \"Gierlichstra%C3%9Fe\",\n        \"housenumber\": \"25\",\n        \"postcode\": \"35683\",\n        \"town\": \"Dillenburg\",\n        \"telefone\": \"02771%20819441\",\n        \"mobile\": \"01631524710\",\n        \"fax\": \"\",\n        \"date_of_birth\": \"\",\n        \"IBAN\": \"\",\n        \"BIC\": \"\"\n      }\n ]}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_USER"
  },
  {
    "type": "get",
    "url": "/user/:id",
    "title": "Read",
    "name": "Read",
    "group": "CRUD_USER",
    "permission": [
      {
        "name": "admin"
      },
      {
        "name": "user.ID"
      }
    ],
    "description": "<p>Endpunkt zum Auslesen aller Nutzerdaten eines Benutzerkontos. Personengeschützte Daten werden nicht übermittelt. Diese Datensätze bleiben 'null'.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No_ID_delivered",
            "description": "<p>&quot;Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_not_found",
            "description": "<p>&quot;Es konnte kein Nutzer mit der ID gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Permission_denied",
            "description": "<p>&quot;401 - Unauthorized&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "User_not_found:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es konnte kein Nutzer mit der ID gefunden werden.\" }",
          "type": "json"
        },
        {
          "title": "No_ID_delivered:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.\" }",
          "type": "json"
        },
        {
          "title": "Permission_denied:",
          "content": "HTTP/1.1 401 Unauthorized\n{ \"message\": \"401 - Unauthorized\" }",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Benutzerkontos.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "User[]",
            "optional": false,
            "field": "user",
            "description": "<p>Nested Array-JSON [{user{ID, forename, surname, email}}]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{message: \"Der Nutzer mit der ID %s konnte erfolgreich gefunden werden.\",\nuser:\n[\n      {\n        \"ID\": 3,\n        \"forename\": \"Sebastian\",\n        \"surname\": \"Enns\",\n        \"nickname\": \"datsepp\",\n        \"email\": \"datsepp@gmail.com\",\n        \"password\": null,\n        \"usergroup_label\": \"Admin\",\n        \"street\": \"Gierlichstra%C3%9Fe\",\n        \"housenumber\": \"25\",\n        \"postcode\": \"35683\",\n        \"town\": \"Dillenburg\",\n        \"telefone\": \"02771%20819441\",\n        \"mobile\": \"01631524710\",\n        \"fax\": \"\",\n        \"date_of_birth\": \"\",\n        \"IBAN\": \"\",\n        \"BIC\": \"\"\n      }\n ]}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_USER"
  },
  {
    "type": "get",
    "url": "/userList",
    "title": "Read All",
    "name": "Read_All",
    "group": "CRUD_USER",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>Endpunkt gibt alle bereits vorhandenen Benutzerkonten samt wichtigen Nutzeroberflächendaten aus.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_Not_Found",
            "description": "<p>&quot;Es konnten keine Benutzerkonten in der Nutzerliste gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Permission_denied",
            "description": "<p>&quot;401 - Unauthorized&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Permission_denied:",
          "content": "HTTP/1.1 401 Unauthorized\n{ \"message\": \"401 - Unauthorized\" }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es konnten keine Benutzerkonten in der Nutzerliste gefunden werden.\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "UserList[]",
            "optional": false,
            "field": "userList",
            "description": "<p>Nested Array-JSON [{user{ID, forename, surname, email}}]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{message: \"Die Benutzerliste konnte erfolgreich geladen werden.\",\nuserList:\n[\n     {\n         \"ID\":1,\n         \"forename\":\"Sebastian\",\n         \"surname\":\"Enns\",\n         \"nickname\":\"datsepp\",\n         \"email\":\"datsepp@gmail.com\",\n         \"usergroup_label\":\"Admin\"\n      }\n ]}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_USER"
  },
  {
    "type": "get",
    "url": "/userList",
    "title": "Read All",
    "name": "Read_All",
    "group": "CRUD_USER",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>Endpunkt gibt alle bereits vorhandenen Benutzerkonten samt wichtigen Nutzeroberflächendaten aus.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_Not_Found",
            "description": "<p>&quot;Es konnten keine Benutzerkonten in der Nutzerliste gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Permission_denied",
            "description": "<p>&quot;401 - Unauthorized&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Permission_denied:",
          "content": "HTTP/1.1 401 Unauthorized\n{ \"message\": \"401 - Unauthorized\" }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es konnten keine Benutzerkonten in der Nutzerliste gefunden werden.\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "UserList[]",
            "optional": false,
            "field": "userList",
            "description": "<p>Nested Array-JSON [{user{ID, forename, surname, email}}]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{message: \"Die Benutzerliste konnte erfolgreich geladen werden.\",\nuserList:\n[\n     {\n         \"ID\":1,\n         \"forename\":\"Sebastian\",\n         \"surname\":\"Enns\",\n         \"nickname\":\"datsepp\",\n         \"email\":\"datsepp@gmail.com\",\n         \"usergroup_label\":\"Admin\"\n      }\n ]}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_USER"
  },
  {
    "type": "put",
    "url": "/user/:id",
    "title": "Update",
    "name": "Update",
    "group": "CRUD_USER",
    "permission": [
      {
        "name": "admin"
      },
      {
        "name": "user.ID"
      }
    ],
    "description": "<p>Endpunkt zum Aktualisieren von Nutzerdaten.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No_ID_delivered",
            "description": "<p>&quot;Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Email_already_exists",
            "description": "<p>&quot;Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_not_found",
            "description": "<p>&quot;Es konnte kein Nutzer mit der ID gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Permission_denied",
            "description": "<p>&quot;401 - Unauthorized&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Nothing_has_changed:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es wurden keine Änderungen an dem Nutzer vorgenommen.\" }",
          "type": "json"
        },
        {
          "title": "No_ID_delivered:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.\" }",
          "type": "json"
        },
        {
          "title": "Email_already_exists:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!\" }",
          "type": "json"
        },
        {
          "title": "Permission_denied:",
          "content": "HTTP/1.1 401 Unauthorized\n{ \"message\": \"401 - Unauthorized\" }",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Benutzerkontos.</p>"
          },
          {
            "group": "Parameter",
            "type": "Any",
            "optional": false,
            "field": "user",
            "description": "<p>Array Objekt mit Nutzerdaten, die aktualisiert werden müssen.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "User[]",
            "optional": false,
            "field": "user",
            "description": "<p>Nested Array-JSON mit aktualisierten Nutzerdaten.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ message: \"Der Nutzer mit der ID %s konnte erfolgreich gefunden werden.\",\nuser:\n[\n      {\n        \"ID\": 3,\n        \"forename\": \"Sebastian\",\n        \"surname\": \"Enns\",\n        \"nickname\": \"datsepp\",\n        \"email\": \"datsepp@gmail.com\",\n        \"password\": null,\n        \"usergroup_label\": \"Admin\",\n        \"street\": \"Gierlichstra%C3%9Fe\",\n        \"housenumber\": \"25\",\n        \"postcode\": \"35683\",\n        \"town\": \"Dillenburg\",\n        \"telefone\": \"02771%20819441\",\n        \"mobile\": \"01631524710\",\n        \"fax\": \"\",\n        \"date_of_birth\": \"\",\n        \"IBAN\": \"\",\n        \"BIC\": \"\"\n      }\n ]}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "CRUD_USER"
  },
  {
    "type": "put",
    "url": "/user/:id",
    "title": "Update",
    "name": "Update",
    "group": "CRUD_USER",
    "permission": [
      {
        "name": "admin"
      },
      {
        "name": "user.ID"
      }
    ],
    "description": "<p>Endpunkt zum Aktualisieren von Nutzerdaten.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No_ID_delivered",
            "description": "<p>&quot;Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: error&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Email_already_exists",
            "description": "<p>&quot;Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_not_found",
            "description": "<p>&quot;Es konnte kein Nutzer mit der ID gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Permission_denied",
            "description": "<p>&quot;401 - Unauthorized&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Nothing_has_changed:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es wurden keine Änderungen an dem Nutzer vorgenommen.\" }",
          "type": "json"
        },
        {
          "title": "No_ID_delivered:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Es wurde kein Parameter übermittelt. Überprüfen Sie Ihre Angaben.\" }",
          "type": "json"
        },
        {
          "title": "Email_already_exists:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!\" }",
          "type": "json"
        },
        {
          "title": "Permission_denied:",
          "content": "HTTP/1.1 401 Unauthorized\n{ \"message\": \"401 - Unauthorized\" }",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID eines Benutzerkontos.</p>"
          },
          {
            "group": "Parameter",
            "type": "Any",
            "optional": false,
            "field": "user",
            "description": "<p>Array Objekt mit Nutzerdaten, die aktualisiert werden müssen.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "User[]",
            "optional": false,
            "field": "user",
            "description": "<p>Nested Array-JSON mit aktualisierten Nutzerdaten.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ message: \"Der Nutzer mit der ID %s konnte erfolgreich gefunden werden.\",\nuser:\n[\n      {\n        \"ID\": 3,\n        \"forename\": \"Sebastian\",\n        \"surname\": \"Enns\",\n        \"nickname\": \"datsepp\",\n        \"email\": \"datsepp@gmail.com\",\n        \"password\": null,\n        \"usergroup_label\": \"Admin\",\n        \"street\": \"Gierlichstra%C3%9Fe\",\n        \"housenumber\": \"25\",\n        \"postcode\": \"35683\",\n        \"town\": \"Dillenburg\",\n        \"telefone\": \"02771%20819441\",\n        \"mobile\": \"01631524710\",\n        \"fax\": \"\",\n        \"date_of_birth\": \"\",\n        \"IBAN\": \"\",\n        \"BIC\": \"\"\n      }\n ]}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "CRUD_USER"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "server/apiDoc/main.js",
    "group": "C__Users_mails_WebstormProjects_SoSe18_Team10_server_apiDoc_main_js",
    "groupTitle": "C__Users_mails_WebstormProjects_SoSe18_Team10_server_apiDoc_main_js",
    "name": ""
  },
  {
    "type": "GET",
    "url": "/faq/:category",
    "title": "Read",
    "name": "Read",
    "group": "FAQ",
    "description": "<p>Endpunkt zum Auslesen der häufig gestellten Fragen in einer Kategorie.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Kategorien: rent, delivery, reapir</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No_Data",
            "description": "<p>&quot;Nothing found.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Error: error\" }",
          "type": "json"
        },
        {
          "title": "Email_already_exists:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Nothing found.\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "FAQ[]",
            "optional": false,
            "field": "faq",
            "description": "<p>Nested Array-JSON mit Fragen und Antworten</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n {\n     \"faq\": {\n        \"faq\": [\n            {\n              \"questi\": \"Wie lange kann ich einen Artikel mieten?\",\n              \"answer\": \"Sie können zwischen verschiedenen Laufzeiten wählen...\"\n            },\n            {\n              \"questi\": \"Kann ich die Mietzeit verlängern?\",\n              \"answer\": \"Ja, das geht...\"\n            }\n        ]}\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "FAQ"
  },
  {
    "type": "GET",
    "url": "/faq/:category",
    "title": "Read",
    "name": "Read",
    "group": "FAQ",
    "description": "<p>Endpunkt zum Auslesen der häufig gestellten Fragen in einer Kategorie.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Kategorien: rent, delivery, reapir</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No_Data",
            "description": "<p>&quot;Nothing found.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Error: error&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Error: error\" }",
          "type": "json"
        },
        {
          "title": "Email_already_exists:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Nothing found.\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "FAQ[]",
            "optional": false,
            "field": "faq",
            "description": "<p>Nested Array-JSON mit Fragen und Antworten</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n {\n     \"faq\": {\n        \"faq\": [\n            {\n              \"questi\": \"Wie lange kann ich einen Artikel mieten?\",\n              \"answer\": \"Sie können zwischen verschiedenen Laufzeiten wählen...\"\n            },\n            {\n              \"questi\": \"Kann ich die Mietzeit verlängern?\",\n              \"answer\": \"Ja, das geht...\"\n            }\n        ]}\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "FAQ"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "name": "Login",
    "group": "LoginSys",
    "description": "<p>Endpunkt führt einen Login aus, sofern die übergebenen Parameter korrekt verschlüsselt, übermittelt und mit den Datensätzen in der Datenbank übereinstimmen.</p>",
    "sampleRequest": [
      {
        "url": "/login"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Benutzerkonten-Email - Parameter explizit notwendig.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>AES-verschlüsseltes Passwort eines Nutzerkontos.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Invalid_Data",
            "description": "<p>&quot;Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist nicht korrekt.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: code&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal Server Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\"message\": \"An Error has occurred: error\"}",
          "type": "json"
        },
        {
          "title": "Invalid Data:",
          "content": "HTTP/1.1 400 Bad Request\n{\"message\": \"Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist nicht korrekt.\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "login",
            "description": "<p>Boolean Wert zum Bestätigen eines erfolgreichen/fehlgeschlagenen Logins</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Logged In:",
          "content": "HTTP/1.1 200 OK\n{ message: \"Sie haben sich erfolgreich eingeloggt.\", login: true }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "LoginSys"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "name": "Login",
    "group": "LoginSys",
    "description": "<p>Endpunkt führt einen Login aus, sofern die übergebenen Parameter korrekt verschlüsselt, übermittelt und mit den Datensätzen in der Datenbank übereinstimmen.</p>",
    "sampleRequest": [
      {
        "url": "/login"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Benutzerkonten-Email - Parameter explizit notwendig.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>AES-verschlüsseltes Passwort eines Nutzerkontos.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Invalid_Data",
            "description": "<p>&quot;Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist nicht korrekt.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: code&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal Server Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\"message\": \"An Error has occurred: error\"}",
          "type": "json"
        },
        {
          "title": "Invalid Data:",
          "content": "HTTP/1.1 400 Bad Request\n{\"message\": \"Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist nicht korrekt.\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "login",
            "description": "<p>Boolean Wert zum Bestätigen eines erfolgreichen/fehlgeschlagenen Logins</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Logged In:",
          "content": "HTTP/1.1 200 OK\n{ message: \"Sie haben sich erfolgreich eingeloggt.\", login: true }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "LoginSys"
  },
  {
    "type": "post",
    "url": "/logout",
    "title": "Logout",
    "name": "Logout",
    "group": "LoginSys",
    "description": "<p>Endpunkt zerstört eine vorhandene Session eines Benutzers, sofern vorhanden.</p>",
    "sampleRequest": [
      {
        "url": "/logout"
      }
    ],
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Es ist ein Fehler aufgetreten.. Versuchen Sie es nocheinmal. Sollten weiterhin Schwierigkeiten auftreten, wenden Sie sich an die Administration. #Fehler : Fehlercode&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Session_not_found",
            "description": "<p>&quot;Es konnte keine Session auf Ihren Namen gefunden werden.&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Bad Internal Server Error\n{ message : \"Es ist ein Fehler aufgetreten.. Versuchen Sie es nocheinmal. Sollten weiterhin\" +\n              \"Schwierigkeiten auftreten, wenden Sie sich an die Administration. #Fehler : \" + err,\n      logout : false }",
          "type": "json"
        },
        {
          "title": "Session_not_found:",
          "content": "HTTP/1.1 400 Bad Request\n{ message : \"Es konnte keine Session auf Ihren Namen gefunden werden.\" + err,\n      logout : false }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "logout",
            "description": "<p>Boolean Wert zum Bestätigen eines erfolgreichen Logouts</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ message : \"Sie wurden erfolgreich ausgeloggt.\", logout: true }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "LoginSys"
  },
  {
    "type": "post",
    "url": "/logout",
    "title": "Logout",
    "name": "Logout",
    "group": "LoginSys",
    "description": "<p>Endpunkt zerstört eine vorhandene Session eines Benutzers, sofern vorhanden.</p>",
    "sampleRequest": [
      {
        "url": "/logout"
      }
    ],
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;Es ist ein Fehler aufgetreten.. Versuchen Sie es nocheinmal. Sollten weiterhin Schwierigkeiten auftreten, wenden Sie sich an die Administration. #Fehler : Fehlercode&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Session_not_found",
            "description": "<p>&quot;Es konnte keine Session auf Ihren Namen gefunden werden.&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Bad Internal Server Error\n{ message : \"Es ist ein Fehler aufgetreten.. Versuchen Sie es nocheinmal. Sollten weiterhin\" +\n              \"Schwierigkeiten auftreten, wenden Sie sich an die Administration. #Fehler : \" + err,\n      logout : false }",
          "type": "json"
        },
        {
          "title": "Session_not_found:",
          "content": "HTTP/1.1 400 Bad Request\n{ message : \"Es konnte keine Session auf Ihren Namen gefunden werden.\" + err,\n      logout : false }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "logout",
            "description": "<p>Boolean Wert zum Bestätigen eines erfolgreichen Logouts</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ message : \"Sie wurden erfolgreich ausgeloggt.\", logout: true }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "LoginSys"
  },
  {
    "type": "post",
    "url": "/register",
    "title": "Register",
    "name": "Register",
    "group": "LoginSys",
    "sampleRequest": [
      {
        "url": "/register"
      }
    ],
    "description": "<p>Öffentlicher Endpunkt zum Anlegen von Benutzerkonten. Parameter werden verschlüsselt und entkodiert entgegengenommen. Endpunkt verarbeitet einkommende Daten und sendet ein Bad Request aus, sollten ankommende Daten nicht den Vorgaben entsprechen.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Benutzerkonten E-Mail Adresse</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>AES verschlüsseltes Benutzerkonten Passwort</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "forename",
            "description": "<p>Vorname des Benutzerkontos</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "surname",
            "description": "<p>Nachname des Benutzerkontos</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Invalid_Data",
            "description": "<p>&quot;Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist ungültig. Bitte korrigieren Sie Ihre Eingaben und versuchen Sie es erneut.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Email_already_exists",
            "description": "<p>&quot;Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserGroup_not_found",
            "description": "<p>&quot;Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_Create_failed",
            "description": "<p>&quot;Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: %s&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Email_already_exists:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!\" }",
          "type": "json"
        },
        {
          "title": "UserGroup_not_found:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden.\" }",
          "type": "json"
        },
        {
          "title": "Invalid_Data:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten.\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "user",
            "description": "<p>Boolean Wert zum Bestätigen einer erfolgreichen/gescheiterten Registrierung.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ message : \"Ihre Registrierung war erfolgreich %s! Sie können sich nun in Ihr Benutzerkonto einloggen.\", user : true }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "LoginSys"
  },
  {
    "type": "post",
    "url": "/register",
    "title": "Register",
    "name": "Register",
    "group": "LoginSys",
    "sampleRequest": [
      {
        "url": "/register"
      }
    ],
    "description": "<p>Öffentlicher Endpunkt zum Anlegen von Benutzerkonten. Parameter werden verschlüsselt und entkodiert entgegengenommen. Endpunkt verarbeitet einkommende Daten und sendet ein Bad Request aus, sollten ankommende Daten nicht den Vorgaben entsprechen.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Benutzerkonten E-Mail Adresse</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>AES verschlüsseltes Benutzerkonten Passwort</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "forename",
            "description": "<p>Vorname des Benutzerkontos</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "surname",
            "description": "<p>Nachname des Benutzerkontos</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Invalid_Data",
            "description": "<p>&quot;Die eingegebene E-Mail Adresse oder das eingegebene Passwort ist ungültig. Bitte korrigieren Sie Ihre Eingaben und versuchen Sie es erneut.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Email_already_exists",
            "description": "<p>&quot;Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserGroup_not_found",
            "description": "<p>&quot;Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User_Create_failed",
            "description": "<p>&quot;Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten.&quot;</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;An Error has occurred: %s&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"An Error has occurred: error\" }",
          "type": "json"
        },
        {
          "title": "Email_already_exists:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Die von Ihnen eingegebene E-Mail Adresse existiert bereits in der Datenbank!\" }",
          "type": "json"
        },
        {
          "title": "UserGroup_not_found:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Ein Fehler ist aufgetreten, es konnte keine Gruppe gefunden werden.\" }",
          "type": "json"
        },
        {
          "title": "Invalid_Data:",
          "content": "HTTP/1.1 400 Bad Request\n{ \"message\": \"Beim Erstellen des Benutzerkontos ist ein Fehler aufgetreten.\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "user",
            "description": "<p>Boolean Wert zum Bestätigen einer erfolgreichen/gescheiterten Registrierung.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ message : \"Ihre Registrierung war erfolgreich %s! Sie können sich nun in Ihr Benutzerkonto einloggen.\", user : true }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "LoginSys"
  },
  {
    "type": "GET",
    "url": "/calc",
    "title": "Calculator",
    "name": "Calculator",
    "group": "PRICING",
    "description": "<p>Endpunkt zum Berechnen von Mietpreisen für Wochen und Monate</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "time",
            "description": "<p>Mietzeit in Tagen - Gültig sind 7er Schritte für Wochen und 30er Schritte für Monate</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Neupreis eines Produktes</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;500 iNtErNaL sErVeR ErRoRrRr&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"500 iNtErNaL sErVeR ErRoRrRr\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "calc",
            "description": "<p>Wochen/Monatspreis für einen Artikel</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{ calc : 99 }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "PRICING"
  },
  {
    "type": "GET",
    "url": "/calc",
    "title": "Calculator",
    "name": "Calculator",
    "group": "PRICING",
    "description": "<p>Endpunkt zum Berechnen von Mietpreisen für Wochen und Monate</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "time",
            "description": "<p>Mietzeit in Tagen - Gültig sind 7er Schritte für Wochen und 30er Schritte für Monate</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Neupreis eines Produktes</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal_Server_Error",
            "description": "<p>&quot;500 iNtErNaL sErVeR ErRoRrRr&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal_Server_Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"500 iNtErNaL sErVeR ErRoRrRr\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "calc",
            "description": "<p>Wochen/Monatspreis für einen Artikel</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 CREATED\n{ calc : 99 }",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "PRICING"
  },
  {
    "type": "get",
    "url": "/shop",
    "title": "ShopList",
    "name": "ShopList",
    "group": "ShopSys",
    "description": "<p>Endpunkt gibt eine Liste an Shop Produkten als nested Array in JSON zurück. Anhand von Parametern die an den Endpunkt übergeben werden, können bereits Werte zurückgegeben werden.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "category",
            "description": "<p>Parameter zum Filtern nach Kategorien</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "producer",
            "description": "<p>Parameter zum Filtern nach Anbietern</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/shop"
      }
    ],
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal",
            "description": "<p>Server Error</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal Server Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: \" + err.code\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Any[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON: [{product : {ID, name, picture}}]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Products found:",
          "content": "HTTP/1.1 200 OK\n{\"productList\" : \"productList\"}",
          "type": "json"
        },
        {
          "title": "No Products found:",
          "content": "HTTP/1.1 200 OK\n{message: \"Es wurden keine Produkte im Shop gefunden.\"}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.ts",
    "groupTitle": "ShopSys"
  },
  {
    "type": "get",
    "url": "/shop",
    "title": "ShopList",
    "name": "ShopList",
    "group": "ShopSys",
    "description": "<p>Endpunkt gibt eine Liste an Shop Produkten als nested Array in JSON zurück. Anhand von Parametern die an den Endpunkt übergeben werden, können bereits Werte zurückgegeben werden.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "category",
            "description": "<p>Parameter zum Filtern nach Kategorien</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "producer",
            "description": "<p>Parameter zum Filtern nach Anbietern</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/shop"
      }
    ],
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Internal",
            "description": "<p>Server Error</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Internal Server Error:",
          "content": "HTTP/1.1 500 Internal Server Error\n{ \"message\": \"Select all error: \" + err.code\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Any[]",
            "optional": false,
            "field": "productList",
            "description": "<p>Nested Array-JSON: [{product : {ID, name, picture}}]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Products found:",
          "content": "HTTP/1.1 200 OK\n{\"productList\" : \"productList\"}",
          "type": "json"
        },
        {
          "title": "No Products found:",
          "content": "HTTP/1.1 200 OK\n{message: \"Es wurden keine Produkte im Shop gefunden.\"}",
          "type": "json"
        }
      ]
    },
    "version": "1.2.0",
    "filename": "server/server.js",
    "groupTitle": "ShopSys"
  }
] });
