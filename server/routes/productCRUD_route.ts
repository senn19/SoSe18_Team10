import {Request, Response} from "express";
import {MySQL_Handler} from "../classes/MySQL_Handler";
import {Product} from "../classes/Product";
import {ProductList} from "../classes/ProductList";

const TABLE_PROD = require("../const/const_table_column_names");


let mySQL = new MySQL_Handler();

module.exports = {

    productList: (req: Request, res: Response) => {
        let productList = new ProductList();
        let sqlArgs : string = "";
        if(req.session.provider) {
            sqlArgs = "JOIN producer ON product.ID_producer = producer.ID "+
                "LEFT JOIN user ON producer.ID = user.ID_producer " +
                "WHERE user.ID ="+req.session.userID;
        }
        productList.getProductList(sqlArgs).then(productList => {
            if (productList.length == 0) {
                res.status(200).json({message: "Es wurden keine Produkte gefunden."});
            }
            res.status(200).json({productList: productList});
        }).catch(err => {
            res.status(500).json({message: "Select all error: " + err.code});
        });
    },

    findProductById: (req: Request, res: Response) => {
        let id = req.params.id;

        if (isNaN(id)) {
            return res.status(400).json({message: "Für ID bitte Zahl eingeben."});
        }

        let productList = new ProductList();
        let query: string = 'SELECT * FROM product WHERE ID = ' + "'" + id + "';";

        mySQL.query(query).then(rows => {
            let result: any = rows;
            if (result.affectedRows != 0) {
                productList.getProductListByID(id).then(productList => {
                    if (productList.length == 0) res.status(500).json({message: "Es wurden keine Produkte gefunden."});
                    res.status(200).json({productList: productList});
                }).catch(err => {
                    res.status(500).json({message: "Select all error: " + err.code});
                });
            }
        }).catch(err => {
            res.status(500).json({message: "Select all error: " + err.code});
        });
    },

    deleteProduct: (req: Request, res: Response) => {
        let id = req.params.id;

        if (isNaN(id)) {
            return res.status(400).json({message: "Für ID bitte Zahl eingeben."});
        }

        let query = 'DELETE FROM product WHERE ID = ' + '"' + id + '"';

        mySQL.query(query).then(rows => {
            let result: any = rows;
            if (result.affectedRows != 0) {
                let productList = new ProductList();
                let sqlArgs : string = "";
                if(req.session.provider) {
                    sqlArgs = "JOIN producer ON product.ID_producer = producer.ID "+
                        "LEFT JOIN user ON producer.ID = user.ID_producer " +
                        "WHERE user.ID ="+req.session.userID;
                }
                return productList.getProductList(sqlArgs);
            }
        }).then(productList => {
            if (productList.length == 0) {
                return res.status(500).json({message: "Keine Produktliste vorhanden."});
            } else {
                res.status(200).json({
                    message: "Product mit der ID " + id + " erfolgreich gelöscht.",
                    productList: productList
                });
            }
        }).catch(err => {
            res.status(400).json({message: "Select all error: " + err.code});
        });
    },

    createProduct: (req: Request, res: Response) => {

        if (!req.body) {
            return res.status(500).json({message: "Bitte alle Felder ausfüllen"});
        }

        let newProduct = new Product(
            null, (req.body.id_provider ? req.body.id_provider : req.session.producerID), req.body.id_cat, req.body.pPrice, req.body.pName, req.body.pDescription, req.body.pLenght, req.body.pWidth, req.body.pHeight,
            req.body.pDisplaySize, req.body.pResolution, req.body.pAdvancedCtrls, req.body.pProdSupl,
            req.body.pProcessor, req.body.pGraphicsCard, req.body.pMemory, (req.body.pProductType ? req.body.pProductType : ""), (req.body.pMicroSD ? req.body.MicroSD : ""),
            req.body.pRam, (req.body.pExtMem ? req.body.pExtMem : ""), req.body.pPic
        );

        let insertData: [number, number, number, number, string, string, string, string, string, string, string, string, string, string,
            string, string, string, string, string, string, string] = [
            null, newProduct.id_provider, newProduct.id_category, newProduct.price, newProduct.name, newProduct.description,
            newProduct.length, newProduct.width, newProduct.height,
            newProduct.display_size, newProduct.resolution, newProduct.advanced_controls, newProduct.productivity_supplies,
            newProduct.processor, newProduct.graphics_card, newProduct.memory, newProduct.product_type,
            newProduct.micro_sd_slot, newProduct.ram, newProduct.extensible_memory, newProduct.picture];

        let query = 'INSERT INTO product (' +
            TABLE_PROD.PROD_ID + ',' +
            TABLE_PROD.PROD_ID_PROVIDER + ',' +
            TABLE_PROD.PROD_ID_CATEGORY + ',' +
            TABLE_PROD.PROD_PRICE + ',' +
            TABLE_PROD.PROD_NAME + ',' +
            TABLE_PROD.PROD_DESCRIPTION + ',' +
            TABLE_PROD.PROD_LENGTH + ',' +
            TABLE_PROD.PROD_WIDTH + ',' +
            TABLE_PROD.PROD_HEIGHT + ',' +
            TABLE_PROD.PROD_DISPLAY_SIZE + ',' +
            TABLE_PROD.PROD_RESOLUTION + ',' +
            TABLE_PROD.PROD_ADVANCED_CONTROLS + ',' +
            TABLE_PROD.PROD_PRODUCTIVITY_SUPPLIES + ',' +
            TABLE_PROD.PROD_PROCESSOR + ',' +
            TABLE_PROD.PROD_GRAPHICS_CARD + ',' +
            TABLE_PROD.PROD_MEMORY + ',' +
            TABLE_PROD.PROD_PRODUCT_TYPE + ',' +
            TABLE_PROD.PROD_MICRO_SD_SLOT + ',' +
            TABLE_PROD.PROD_RAM + ',' +
            TABLE_PROD.PROD_EXTENSIBLE_MEMORY + ',' +
            TABLE_PROD.PROD_PICTURE + ') ' +
            'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';

        mySQL.query(query, insertData).then(rows => {
            if (rows[0]) {
                return res.status(500).json({message: "Insert error"});
            }
        }).then(rows => {
            let productList = new ProductList();
            let sqlArgs : string = "";
            if(req.session.provider) {
                sqlArgs = "JOIN producer ON product.ID_producer = producer.ID "+
                    "LEFT JOIN user ON producer.ID = user.ID_producer " +
                    "WHERE user.ID ="+req.session.userID;
            }
            return productList.getProductList(sqlArgs);
        }).then(productList => {
            if (productList.length == 0) res.status(400).json({message: "Keine Produkte konnten gefunden werden."});
            res.status(200).json({
                message: "Produkt " + newProduct.name + " erfolgreich hinzugefügt!",
                productList: productList
            })
        }).catch(err => {
            res.status(500).json({message: "Select all error: " + err.code});
        });
    },

    updateProduct: (req: Request, res: Response) => {
        let id: number = req.params.id;

        if (isNaN(id)) {
            return res.status(400).json({message: "Für ID bitte Zahl eingeben."});
        }

        if (!req.body) {
            return res.status(500).json({message: "Bitte alle Felder ausfüllen"});
        }

        let query2 = 'UPDATE product SET ';

        let attributes: string[] = [
            null,
            TABLE_PROD.PROD_ID_PROVIDER,
            TABLE_PROD.PROD_ID_CATEGORY,
            TABLE_PROD.PROD_PRICE,
            TABLE_PROD.PROD_NAME,
            TABLE_PROD.PROD_DESCRIPTION,
            TABLE_PROD.PROD_LENGTH,
            TABLE_PROD.PROD_WIDTH,
            TABLE_PROD.PROD_HEIGHT,
            TABLE_PROD.PROD_DISPLAY_SIZE,
            TABLE_PROD.PROD_RESOLUTION,
            TABLE_PROD.PROD_ADVANCED_CONTROLS,
            TABLE_PROD.PROD_PRODUCTIVITY_SUPPLIES,
            TABLE_PROD.PROD_PROCESSOR,
            TABLE_PROD.PROD_GRAPHICS_CARD,
            TABLE_PROD.PROD_MEMORY,
            TABLE_PROD.PROD_PRODUCT_TYPE,
            TABLE_PROD.PROD_MICRO_SD_SLOT,
            TABLE_PROD.PROD_RAM,
            TABLE_PROD.PROD_EXTENSIBLE_MEMORY,
            TABLE_PROD.PROD_PICTURE
        ];

        let request_body: any[] = [
            null,
            req.body.id_provider,
            req.body.id_cat,
            req.body.pPrice,
            req.body.pName,
            req.body.pDescription,
            req.body.pLenght,
            req.body.pWidth,
            req.body.pHeight,
            req.body.pDisplaySize,
            req.body.pResolution,
            req.body.pAdvancedCtrls,
            req.body.pProdSupl,
            req.body.pProcessor,
            req.body.pGraphicsCard,
            req.body.pMemory,
            req.body.pProductType,
            req.body.pMicroSD,
            req.body.pRam,
            req.body.pExtMem,
            req.body.pPic];

        // build query
        for (let i = 0; i < request_body.length; i++) {
            if (request_body[i]) {
                query2 += attributes[i] + " = '" + request_body[i] + "', ";
            }
        }
        query2 = query2.substr(0, query2.length - 2) + ' WHERE ID = ' + '"' + id + '";';

        mySQL.query(query2).then(rows => {
            if (rows[0]) {
                return res.status(400).json({message: "Insert error"});
            }
        }).then(rows => {
            let productList = new ProductList();
            let sqlArgs : string = "";
            if(req.session.provider) {
                sqlArgs = "JOIN producer ON product.ID_producer = producer.ID "+
                    "LEFT JOIN user ON producer.ID = user.ID_producer " +
                    "WHERE user.ID ="+req.session.userID;
            }
            return productList.getProductList(sqlArgs);
        }).then(productList => {
            if (productList.length == 0) res.status(500).json({message: "Keine Produkte konnten gefunden werden."});
            res.status(200).json({
                message: "Produkt  erfolgreich geändert!",
                productList: productList
            });
        }).catch(err => {
            res.status(500).json({message: "Select all error: " + err.code});
        });

    }
};
