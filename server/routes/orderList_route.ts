import {Request, Response} from "express";
import {OrderList} from "../classes/OrderList";

module.exports = {
    orderList: (req: Request, res: Response) => {
        let userID: string = req.session.id;

        console.log("orderList");
        let orderList = new OrderList();
        orderList.getOrderList(userID).then(orderList => {
            if (!orderList) {
                res.status(500).json({message: "Ihr Warenkorb ist leer."});
            }
            res.status(200).json({orderList: orderList});
        }).catch(err => {
            res.status(500).json({message: "Select all error 1: " + err.code});
        });
    },

    deleteProductFromOrderList: (req: Request, res: Response) => {
       // TODO
    },
}