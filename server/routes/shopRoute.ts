import {ShopSQLBuilder} from "../classes/ShopSQLBuilder";
import {ShopProduct} from "../classes/ShopProduct";
import {Request, Response} from "express";
import {MySQL_Handler} from "../classes/MySQL_Handler";

let mySQL = new MySQL_Handler();

module.exports = {
    getShopList : function (req: Request, res: Response) {
        let shopSQL = new ShopSQLBuilder();
        let productList : ShopProduct[] = [];
        let sql = shopSQL.getShopListSQL(req.query);

        // fetch data from database, iterate through products and response with res.code 200
        mySQL.query(sql).then(rows => {
            let result : any = rows;
            if(!result[0]) return res.status(200).json({message: "Es wurden keine Produkte im Shop gefunden."});
            for(let i: number = 0; i < result.length; i++) {
                if(result[i]) {
                    productList.push(new ShopProduct(result[i].ID,result[i].name,result[i].picture));
                }
            }
            return res.status(200).json({productList:productList});
        }).catch(err => {
            res.status(500).json({message : "Select all error: " + err.code});
        }).finally(mySQL.close);
    }
};