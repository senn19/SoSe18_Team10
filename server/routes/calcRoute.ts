import {Calc} from "../classes/Calc";
import {Request, Response} from "express";

module.exports = {
    calcPriceOfProduct : function(req: Request, res: Response)    {
        let calc : Calc = new Calc((!isNaN(req.query.time) ? Number(req.query.time) : 0),(!isNaN(req.query.price) ? Number(req.query.price) : 0));
        let cost : number = calc.calcPrice();
        if(cost == 0) return res.status(500).json({calc: "500 iNtErNaL sErVeR ErRoRrRr"});
        res.status(200).json({calc: Math.round(cost)});
    }
};