import {Request, Response} from "express";
import {User} from "../classes/User";
import {UserCRUD} from "../classes/UserCRUD";


const LANG_USER = require("../const/const_UserCRUD");
let userCRUD = new UserCRUD(),
    sprintf = require("sprintf-js").sprintf;

module.exports = {

    userList: (req: Request, res: Response) => {
        if(!(req.session.admin)) return res.status(401).json({message: LANG_USER.ERR_NO_PERMISSION}); // sc: admin logged in?
        userCRUD.getUserList().then(message => {
            if (!message.userList) return res.status(400).json(message);
            res.status(200).json(message);
        }).catch(err => {
            res.status(500).json({message: sprintf(LANG_USER.ERR_CATCH, err)});
        });
    },

    userCreate: (req: Request, res: Response) => {
        if (!req.body.email) return res.status(400).json({message: LANG_USER.ERR_NO_EMAIL});
        let user: User = new User(req.body.email.trim(), req.body.password,
            (req.body.forename ? req.body.forename.trim() : ""),
            (req.body.surname ? req.body.surname.trim() : ""),
            (req.body.nickname ? req.body.nickname.trim() : ""),
            (req.body.groupLabel ? req.body.groupLabel : "Nutzer"));
        userCRUD.createUser(user).then(message => {
            if (!message.user) return res.status(400).json(message);
            res.status(201).json(message);
        }).catch(err => {
            res.status(500).json({message: sprintf(LANG_USER.ERR_CATCH, err)});
        });
    },

    userGetByID: (req: Request, res: Response) => {
        if(!req.session.admin && req.session.userID != req.params.id)
            return res.status(401).json({message: LANG_USER.ERR_NO_PERMISSION}); // security check, if user/admin is logged in
        if (!req.params.id) return res.status(400).json({message: LANG_USER.ERR_NO_USERID});
        userCRUD.getUser(req.params.id).then(message => {
            if (!message.user) return res.status(400).json(message);
            res.status(200).json(message);
        }).catch(err => {
            res.status(500).json({message: sprintf(LANG_USER.ERR_CATCH, err)});
        });
    },

    userUpdateByID: (req: Request, res: Response) => {
        if(!req.session.admin && req.session.userID != req.params.id)
            return res.status(401).json({message: LANG_USER.ERR_NO_PERMISSION}); // security check, if user/admin is logged in
        if (!req.params.id) return res.status(400).json({message: LANG_USER.ERR_NO_USERID});
        userCRUD.updateUser(req.body, req.params.id).then(message => {
            if (!message.user) return res.status(400).json(message);
            res.status(200).json(message);
        }).catch(err => {
            res.status(500).json({message: sprintf(LANG_USER.ERR_CATCH, err)});
        });
    },

    userDeleteByID: (req: Request, res: Response) => {
        if(!(req.session.admin) || req.params.id == 1 || req.params.id == 4) return res.status(401).json({message: LANG_USER.ERR_NO_PERMISSION}); // sc: admin logged in?
        if (!req.params.id) return res.status(400).json({message: LANG_USER.ERR_NO_USERID});
        userCRUD.deleteUser(req.params.id).then(message => {
            if (message.user) return res.status(400).json(message);
            res.status(200).json(message);
        }).catch(err => {
            res.status(500).json({message: sprintf(LANG_USER.ERR_CATCH, err)});
        });
    }
};