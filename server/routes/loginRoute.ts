import {LoginHandler} from "../classes/LoginHandler";
import {Request, Response} from "express";
import {MySQL_Handler} from "../classes/MySQL_Handler";
import {RegHandler} from "../classes/RegHandler";
import {User} from "../classes/User";
import {UserCRUD} from "../classes/UserCRUD";


const LOGIN = require("../const/const_LoginRoute");
let sprintf = require("sprintf-js").sprintf,
    mySQL = new MySQL_Handler();


module.exports = {
    userLogin: function (req: Request, res: Response) {
        if(!req.body.email || !req.body.password) return res.status(400).json({message: LOGIN.ERR_LOGIN, login: false});
        let loginHandler = new LoginHandler(req.body.email, req.body.password);

        loginHandler.authenticateLogin().then(authentication => {
            if (!authentication) return res.status(400).json({message: LOGIN.ERR_LOGIN, login: false});
            req.session.email = req.body.email;
            return mySQL.query(sprintf(LOGIN.SQL_LOGIN_SELECT, req.session.email));

        }).then(rows => { // rows = mySQL userData
            req.session.nickname = rows[0].nickname;
            req.session.forename = rows[0].forename;
            req.session.surname = rows[0].surname;
            req.session.userID = rows[0].ID;
            req.session.producerID = rows[0].ID_producer;
            if (rows[0].usergroup_label == LOGIN.USERGROUP_ADMIN) req.session.admin = true;
            if (rows[0].usergroup_label == LOGIN.USERGROUP_PROVIDER) req.session.provider = true;
            res.status(200).json({message: LOGIN.SUCC_LOGIN, login: true});

        }).catch(err => {
            res.status(500).json({message: sprintf(LOGIN.ERR_CATCH, err.code)});
        });
    },

    userLogOut: function (req: Request, res: Response) {
        if (!req.session.email) return res.status(400).json({message: LOGIN.ERR_LOGOUT_MISSING_SESSION});
        req.session.destroy((err) => {
            if (err) return res.status(500).json({message: sprintf(LOGIN.ERR_LOGOUT, err)});
            res.status(200).json({message: LOGIN.SUCC_LOGOUT, logout: true});
        });
    },

    userRegister: function (req: Request, res: Response) {
        if(!req.body.email || !req.body.password) return res.status(400).json({
            message: LOGIN.ERR_INVALIDA_DATA,
            register: false
        });
        let regHandler = new RegHandler(req.body.email, req.body.password);
        // validate registration data send via ajax, on valid data return success message and create account
        if (!regHandler.prepareAccount()) return res.status(400).json({
            message: LOGIN.ERR_INVALIDA_DATA,
            register: false
        });
        let user: User = new User(regHandler.email, regHandler.password, encodeURI(req.body.forename),
            encodeURI(req.body.surname), regHandler.nickname);
        let userCRUD: UserCRUD = new UserCRUD();
        userCRUD.createUser(user).then(message => {
            if (!message.user) return res.status(400).json(message);
            res.status(200).json({message: sprintf(LOGIN.SUCC_REGISTER,regHandler.nickname), user: true});
        }).catch(err => {
            res.status(500).json({message: sprintf(LOGIN.ERR_CATCH, err)});
        });
    }
};