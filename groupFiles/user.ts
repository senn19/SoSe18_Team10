$(function() {
    getUserList();

    function getUserList() {
        $.ajax({
            url: '/userList',
            type: 'GET',     // GET-request for DELETE
            dataType: 'json',    // expecting json
            error: (jqXHR) => {
                throw this;
            },
            success: (data) => {
                render(data.userList);
                // this.renderList(data.userList);// .this wurde entfernt
            },
        });
    }


    function render(userList): void {
        let buffer: string = "";
        if (userList.length > 0) {
            buffer += "<div class='container' id='userList'>\n"
            + "  <table style='width: 100%' class=\"table-striped\" border='1'>\n"
            + "    <tr>\n"
            + "      <th> ID </th>\n"
            + "      <th> Vorname  </th>\n"
            + "      <th> Nachname </th>\n"
            + "      <th> nickname </th>\n"
            + "      <th> email </th>\n"
            + "      <th> edit </th>\n"
            + "      <th> delete </th>\n"
            + "      <th> activate </th>\n"
            + "    </tr>\n";
            for (let user in userList) { // iterate through array "userData"
                if (userList[user] != null) {  // ignore array-elements that have been deleted
                    buffer += "    <tr id=user'" + userList[user].id + "'>\n"
                    + "      <td> " + userList[user].id + " </td>\n"
                    + "      <td> " + userList[user].forename + " </td>\n"
                    + "      <td> " + userList[user].surname + " </td>\n"
                    + "      <td> " + userList[user].nickname + " </td>\n"
                    + "      <td> " + userList[user].email + " </td>\n"
                    + "      <td id='updateBtn'><i class='fa fa-edit'></i> </td>\n"
                    + "      <td id='deleteBtn'><i class='fa fa-trash'></i> </td>\n"
                    + "      <td id='activateBtn'><i class='fa fa-power-off'></i> </td>\n"
                    + "    </tr>\n";
                }
            }
        }
        //--- close table (and div) ------------------------------------------------
        buffer += "  </table>\n";
        buffer += "</div>";
        $('#list').html(buffer);
        //--- put buffer-string into message and userlist --------------------------
        // $('#data').html(buffer);
    }

});

