// import of different modules
import express    = require ("express");
import { Request, Response } from "express";
import * as mysql from "mysql2";         // handles database connections
import {Connection, MysqlError} from "mysql2";


// import of different classes from other files
import {UserList} from "./classes/UserList";
import {MySQL_Handler} from "./classes/MySQL_Handler";


// initialising and declaring global variables
let router = express();
let mySQL = new MySQL_Handler();




// router/express() listens on port 8080
router.listen(8080, "localhost", function () {
    console.log(`
    -------------------------------------------------------------
    Server wurde erfolgreich gestartet und läuft unter:
    Single Pager:
    http://localhost:8080/
    
    phpMyAdmin:
    http://localhost/phpmyadmin/
    -------------------------------------------------------------
  `);
});

// routing different folders and main path. Folders contain stylesheets & scripts.
router.use("/",   express.static(__dirname + "/../"));
router.use("/jquery",  express.static(__dirname + "/../../node_modules/jquery/dist/"));
router.use("/popper.js",    express.static(__dirname + "/../../node_modules/popper.js/dist/"));
router.use("/bootstrap",    express.static(__dirname + "/../../node_modules/bootstrap/dist/"));
router.use("/font-awesome", express.static(__dirname + "/../../node_modules/@fortawesome/fontawesome-free/"));


router.get("/userList", function (req: Request, res: Response) {
    let userList : UserList[] = [];
    let sql : string = 'SELECT user.ID, forename, surname, nickname, email FROM user, adress WHERE adress.ID = user.ID_adress';

    mySQL.query(sql).then(rows => {
        let result : any = rows;
        if(!result[0]) return res.status(200).json({message: "Es wurden keine Daten in User gefunden."});
        for(let i: number = 0; i < result.length; i++) { //rows.length = Die Länge des Arrays mit den gezogenen Datensätzen
            if(result[i]) {
                userList.push(new UserList(result[i].ID,result[i].forename,result[i].surname,result[i].nickname,result[i].email));
            }
        }
        return res.status(200).json({userList:userList});
    }).catch(err => {
        res.status(500).json({message : "Select all error: " + err.code});
    });
});
