/* copyrighted by Andreas Arnd Aull und Tim David Weller */

export class UserList {
    id: number;
    forename: string;
    surname : string;
    nickname : string;
    email : string;

   constructor(id: number, forename: string, surname: string, nickname: string, email: string) {
        this.id = id;
        this.forename = forename;
        this.surname = surname;
        this.nickname = nickname;
        this.email = email;
    }
}