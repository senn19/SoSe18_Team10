import * as mysql from "mysql2";
import {Connection, MysqlError} from "mysql2";
import {Promise} from 'es6-promise'

export class MySQL_Handler {
    connection;

    constructor() {
         this.connection = mysql.createConnection({
             host: 'localhost',
             user: 'root',
             password: '',
             database: 'creativeminds'
         });
    }
    public query(sql, args?) {
        return new Promise( (resolve, reject) => {
            this.connection.query(sql, args, (err: MysqlError|null, rows: any) => {
                if (err) return reject(err);
                resolve(rows);
            });
        });
    }
    public close() {
        return new Promise((resolve, reject) => {
            this.connection.end(err => {
                if (err) return reject(err);
                resolve();
            });
        });
    }
}