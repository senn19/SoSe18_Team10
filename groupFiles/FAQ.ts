$(function () {

    let questionBlock1 : JQuery = $(".questionBlock1");
    let questionBlock2 : JQuery = $(".questionBlock2");
    let questionBlock3 : JQuery = $(".questionBlock3");


    $("#mieten").on("click",function (){
        $(".questionBlock1").not("hidden").toggleClass("hidden");
        if(questionBlock2.not("hidden")){
            questionBlock2.addClass("hidden");
        }
        if(questionBlock3.not("hidden")){
            questionBlock3.addClass("hidden");
        }
    });
    $("#lieferung").on("click",function (){
        $(".questionBlock2").not("hidden").toggleClass("hidden");
        if(questionBlock1.not("hidden")){
            questionBlock1.addClass("hidden");
        }
        if(questionBlock3.not("hidden")){
            questionBlock3.addClass("hidden");
        }
    });
    $("#reparatur").on("click",function (){
        $(".questionBlock3").not("hidden").toggleClass("hidden");
        if(questionBlock1.not("hidden")){
            questionBlock1.addClass("hidden");
        }
        if(questionBlock2.not("hidden")){
            questionBlock2.addClass("hidden");
        }
    });
});
