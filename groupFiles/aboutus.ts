$(function () {
    
    $("#firm").children(".col-2").on("click",function () {
        console.log($(this).attr("id"));
        let imgClicked : string = $(this).attr("id").split("-")[0];
        let imageTitleHasHidden : JQuery = $("#" + imgClicked + "-H1");
        $("#input").children().not(".hidden").toggleClass("hidden");
        $("#aboutHead").children().not(".hidden").toggleClass("hidden");
        $("#aboutText").children().not(".hidden").toggleClass("hidden");
        $("#aboutImage").children().not(".hidden").toggleClass("hidden");
        if(imageTitleHasHidden.hasClass("hidden")) {
            imageTitleHasHidden.toggleClass("hidden");
            $("#" + imgClicked + "-Text").toggleClass("hidden");
            $("#" + imgClicked + "-Input").toggleClass("hidden");
        }
    })
});