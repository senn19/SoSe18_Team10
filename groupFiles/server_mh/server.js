"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import of different modules
var express = require("express");
var mysql = require("mysql2"); // handles database connections
// import of different classes from other files
// initialising and declaring global variables
var router = express();
//Klassen
var Product = /** @class */ (function () {
    // Methoden
    function Product(id, name, picture) {
        this.id = id;
        this.name = name;
        this.picture = picture;
    }
    return Product;
}());
var productlist = [];
/*****************************************************************************
 ***  get db-connection (does not yet connect)                               *
 *****************************************************************************/
//---- Object with connection parameters --------------------------------------
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'creativeminds'
});
// router/express() listens on port 8080
router.listen(8080, "localhost", function () {
    console.log("\n    -------------------------------------------------------------\n    Server wurde erfolgreich gestartet und l\u00E4uft unter:\n    Single Pager:\n    http://localhost:8080/\n    \n    phpMyAdmin:\n    http://localhost/phpmyadmin/\n    -------------------------------------------------------------\n  ");
});
// routing different folders and main path. Folders contain stylesheets & scripts.
router.use("/", express.static(__dirname + "/../"));
router.use("/jquery", express.static(__dirname + "/../../node_modules/jquery/dist/"));
router.use("/popper.js", express.static(__dirname + "/../../node_modules/popper.js/dist/"));
router.use("/bootstrap", express.static(__dirname + "/../../node_modules/bootstrap/dist/"));
router.use("/font-awesome", express.static(__dirname + "/../../node_modules/@fortawesome/fontawesome-free/"));
//GETS
router.get("/shop", function (req, res) {
    productlist = [];
    var sql = 'SELECT ID,name,picture FROM product;';
    connection.query(sql, function (err, rows) {
        if (!err) {
            if (!rows[0])
                return res.status(200).json({ message: "Es gibt keine Produkte im SHOP" });
            for (var i = 0; i < rows.length; i++) { //rows.length = row:any --> Die Anzahl der Produkte die aus der DB gezogen werden
                if (rows[i]) {
                    console.log(productlist);
                    productlist.push(new Product(rows[i].ID, rows[i].name, rows[i].picture));
                }
            }
            return res.status(200).json({ message: "Alles OK", productlist: productlist });
        }
        else {
            res.status(200).json({ message: "Select all error: " + err.code });
        }
    });
});
