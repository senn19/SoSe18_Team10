// import of different modules
import express    = require ("express");
import { Request, Response } from "express";

import * as mysql from "mysql2";         // handles database connections
import {Connection, MysqlError} from "mysql2";

// import of different classes from other files


// initialising and declaring global variables
let router = express();

//Klassen
class Product {
    // Attribute der Klasse
    id: number;
    name: string;
    picture: string;

    // Methoden
    constructor(id: number, name: string, picture: string) {
        this.id = id;
        this.name = name;
        this.picture = picture;
    }
}

let productlist: Product[] = [];


/*****************************************************************************
 ***  get db-connection (does not yet connect)                               *
 *****************************************************************************/
//---- Object with connection parameters --------------------------------------
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'creativeminds'
});

// router/express() listens on port 8080
router.listen(8080, "localhost", function () {
    console.log(`
    -------------------------------------------------------------
    Server wurde erfolgreich gestartet und läuft unter:
    Single Pager:
    http://localhost:8080/
    
    phpMyAdmin:
    http://localhost/phpmyadmin/
    -------------------------------------------------------------
  `);
});



// routing different folders and main path. Folders contain stylesheets & scripts.
router.use("/",   express.static(__dirname + "/../"));
router.use("/jquery",  express.static(__dirname + "/../../node_modules/jquery/dist/"));
router.use("/popper.js",    express.static(__dirname + "/../../node_modules/popper.js/dist/"));
router.use("/bootstrap",    express.static(__dirname + "/../../node_modules/bootstrap/dist/"));
router.use("/font-awesome", express.static(__dirname + "/../../node_modules/@fortawesome/fontawesome-free/"));

//GETS

router.get("/shop", function (req: Request, res: Response) {
    productlist = [];
    let sql : string = 'SELECT ID,name,picture FROM product;';
    connection.query(sql,(err: MysqlError|null, rows: any) => {
        if (!err) {
            if(!rows[0]) return res.status(200).json({message : "Es gibt keine Produkte im SHOP"});
            for (let i: number = 0; i < rows.length; i++) { //rows.length = row:any --> Die Anzahl der Produkte die aus der DB gezogen werden
                if(rows[i]){
                    console.log(productlist);
                    productlist.push(new Product(rows[i].ID,rows[i].name,rows[i].picture));
                }
            }
            return res.status(200).json({message : "Alles OK", productlist:productlist});
        } else {
            res.status(200).json({message : "Select all error: " + err.code});
        }
    });
});


router.get("/Prod_Ansicht", function (req: Request, res: Response) {
    console.log(req.body);

});

router.post("Prod_Ansicht", function  (req: Request, res: Response) {
    console.log(req.body);

});