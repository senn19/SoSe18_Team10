import {Request, Response} from "express";
import {MySQL_Handler} from "../../../server/classes/MySQL_Handler";
import {Product} from "../classes/Product";
import {ProductList} from "../classes/ProductList";
const TABLE_PROD = require("../../../server/const/const_table_names");


let mySQL = new MySQL_Handler();

module.exports = {

    productList: (req: Request, res: Response) => {
        console.log("productList CRUD");
        let productList = new ProductList();
        productList.getProductList().then(productList => {
            if (!productList) res.status(500).json({message: "Es wurden keine Produkte gefunden."});
            res.status(200).json({productList: productList});
        }).catch(err => {
            res.status(500).json({message: "Select all error: " + err.code});
        });
    },

    findProductById: (req: Request, res: Response) => {
        console.log("findProd");
        let id = req.params.id;
        let productList = new ProductList();
        let query: string = 'SELECT * FROM product WHERE ID = ' + "'" + id + "';";
console.log("query: "+query);
        mySQL.query(query).then(rows => {
            let result: any = rows;
            if(result.affectedRows != 0){
                productList.getProductListByID(id).then(productList => {
                    if (!productList) res.status(500).json({message: "Es wurden keine Produkte gefunden."});
                    res.status(200).json({productList: productList});
                }).catch(err => {
                    res.status(500).json({message: "Select all error: " + err.code});
                });
            }
        }).catch(err => {
            res.status(500).json({message: "Select all error: " + err.code});
        });
    },

    deleteProduct: (req: Request, res: Response) => {
        console.log("deleteProduct CRUD");
        let id = req.params.id;
        let query = 'DELETE FROM product WHERE ID = ' + '"' + id + '"';

        mySQL.query(query).then(rows => {
            let result: any = rows;
            if(result.affectedRows != 0){
                let productList = new ProductList();
                return productList.getProductList();
            }
        }).then(productList => {
            if(!productList){
                return res.status(500).json({message: "Productlist not delivered"});
            } else {
                res.status(200).json({message: "Product with ID " + id + " successfully deleted.", productList: productList});
            }
        }).catch(err => {
            res.status(400).json({message: "Select all error " + err.code});
        });
    },

    createProduct: (req: Request, res: Response) => {
        console.log("create Product CRUD");
        let newProduct = new Product(
            null, req.body.id_user, req.body.id_cat, req.body.pPrice, req.body.pName, req.body.pLenght, req.body.pWidth, req.body.pHeight,
            req.body.pDisplaySize, req.body.pResolution, req.body.pAdvancedCtrls, req.body.pProdSupl,
            req.body.pProcessor, req.body.pGraphicsCard, req.body.pMemory, req.body.pProductType, req.body.pMicroSD,
            req.body.pRam, req.body.pExtMem, req.body.pPic
        );

        let insertData : [ number, number, number, number, string, string, string, string, string, string, string, string, string,
            string, string, string, string, string, string, string ] = [
                null, newProduct.id_user, newProduct.id_category, newProduct.price, newProduct.name, newProduct.length, newProduct.width, newProduct.height,
            newProduct.display_size, newProduct.resolution, newProduct.advanced_controls, newProduct.productivity_supplies,
            newProduct.processor, newProduct.graphics_card, newProduct.memory, newProduct.product_type,
            newProduct.micro_sd_slot, newProduct.ram, newProduct.extensible_memory, newProduct.picture];

        let query = 'INSERT INTO product (' +
            TABLE_PROD.PROD_ID + ',' +
            TABLE_PROD.PROD_ID_USER + ',' +
            TABLE_PROD.PROD_ID_CATEGORY + ',' +
            TABLE_PROD.PROD_PRICE + ',' +
            TABLE_PROD.PROD_NAME + ',' +
            TABLE_PROD.PROD_LENGTH + ',' +
            TABLE_PROD.PROD_WIDTH + ',' +
            TABLE_PROD.PROD_HEIGHT + ',' +
            TABLE_PROD.PROD_DISPLAY_SIZE + ',' +
            TABLE_PROD.PROD_RESOLUTION + ',' +
            TABLE_PROD.PROD_ADVANCED_CONTROLS + ',' +
            TABLE_PROD.PROD_PRODUCTIVITY_SUPPLIES + ',' +
            TABLE_PROD.PROD_PROCESSOR + ',' +
            TABLE_PROD.PROD_GRAPHICS_CARD + ',' +
            TABLE_PROD.PROD_MEMORY + ',' +
            TABLE_PROD.PROD_PRODUCT_TYPE + ',' +
            TABLE_PROD.PROD_MICRO_SD_SLOT + ',' +
            TABLE_PROD.PROD_RAM + ',' +
            TABLE_PROD.PROD_EXTENSIBLE_MEMORY + ',' +
            TABLE_PROD.PROD_PICTURE + ') ' +
            'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';

        /*let query = 'INSERT INTO product (' +
            'ID,' +
            'ID_user,' +
            'ID_categories,' +
            'price,' +
            'name,' +
            'length,' +
            'width,' +
            'height,' +
            'display_size,' +
            'resolution,' +
            'advanced_controls,' +
            'productivity_supplies,' +
            'processor,' +
            'graphics_card,' +
            'memory,' +
            'product_type,' +
            'micro_sd_slot,' +
            'ram,' +
            'extensible_memory,' +
            'picture) ' +
            'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';*/

        console.log("query create product: "+query);
        mySQL.query(query, insertData).then(rows => {
            if(rows[0]){
                return res.status(400).json({message: "Insert error"});
            }
        }).then(rows => {
           // let result: any = rows;
            console.log("crud_create_rows");

           // if (!result.affectedRows) res.status(500).json({message: "Ein Fehler ist aufgetreten. Es wurde nichts hinzugefügt"});
            let productList = new ProductList();
            console.log("ok");
            return productList.getProductList();
        }).then(productList => {
            console.log("product name: "+newProduct.name);
            if (!productList) res.status(500).json({message: "Keine Produkte konnten gefunden werden."});
            res.status(200).json({
                message: "Produkt " + newProduct.name + " erfolgreich hinzugefügt!",
                productList: productList
            })
        }).catch(err => {
            res.status(500).json({message: "Select all error: " + err.code});
        });
    },

    updateProduct: (req:Request, res:Response) => {
        console.log("update Product CRUD");
        let id: number = req.params.id;
        let newProduct = new Product(
            id, req.body.id_user, req.body.id_cat, req.body.pPrice, req.body.pName, req.body.pLenght, req.body.pWidth, req.body.pHeight,
            req.body.pDisplaySize, req.body.pResolution, req.body.pAdvancedCtrls, req.body.pProdSupl,
            req.body.pProcessor, req.body.pGraphicsCard, req.body.pMemory, req.body.pProductType, req.body.pMicroSD,
            req.body.pRam, req.body.pExtMem, req.body.pPic
        );

        let insertData : [ number, number, number, number, string, string, string, string, string, string, string, string, string, string,
            string, string, string, string, string, string ] = [
            newProduct.ID, newProduct.id_user, newProduct.id_category, newProduct.price, newProduct.name, newProduct.length,
            newProduct.width, newProduct.height, newProduct.display_size, newProduct.resolution, newProduct.advanced_controls,
            newProduct.productivity_supplies, newProduct.processor, newProduct.graphics_card, newProduct.memory, newProduct.product_type,
            newProduct.micro_sd_slot, newProduct.ram, newProduct.extensible_memory, newProduct.picture];

        let query = 'UPDATE product SET ' +
            'ID = ' + newProduct.ID + ', ' +
            'ID_user = ' + newProduct.id_user + ',' +
            'ID_categories = ' + newProduct.id_category + ', ' +
            'price = ' + newProduct.price + ', ' +
            'name = ' + newProduct.name + ', ' +
            'length = ' + newProduct.length + ', ' +
            'width = ' + newProduct.width + ', ' +
            'height = ' + newProduct.height + ', ' +
            'display_size = ' + newProduct.display_size + ', ' +
            'resolution = ' + newProduct.resolution + ', ' +
            'advanced_controls = ' + newProduct.advanced_controls + ', ' +
            'productivity_supplies = ' + newProduct.productivity_supplies + ', ' +
            'processor = ' + newProduct.processor + ', ' +
            'graphics_card = ' + newProduct.graphics_card + ', ' +
            'memory = ' + newProduct.memory + ', ' +
            'product_type = ' + newProduct.product_type + ', ' +
            'micro_sd_slot = ' + newProduct.micro_sd_slot + ', ' +
            'ram = ' + newProduct.ram + ', ' +
            'extensible_memory = ' + newProduct.extensible_memory + ', ' +
            'picture = ' + newProduct.picture +
            ' WHERE ID = ' + '"' + id + '";';
console.log("query: "+query);
        mySQL.query(query, insertData).then(rows => {
            if(rows[0]){
                return res.status(400).json({message: "Insert error"});
            }
        }).then(rows => {
            let productList = new ProductList();
            return productList.getProductList();
        }).then(productList => {
            if (!productList) res.status(500).json({message: "Keine Produkte konnten gefunden werden."});
            res.status(200).json({
                message: "Produkt " + newProduct.name + " erfolgreich geändert!",
                productList: productList
            });
        }).catch(err => {
            res.status(500).json({message: "Select all error: " + err.code});
        });

    }
};
