import {Request, Response} from "express";
import {MySQL_Handler} from "../../../server/classes/MySQL_Handler";
import {FAQ} from "../classes/FAQ";
import {FAQList} from "../classes/FAQList";

let faqList = new FAQList();

module.exports = {
    FAQ: (req: Request, res: Response) => {
        faqList.getFAQList(req.params.category).then(faq => {
            console.log(faq);
            let test: any = faq;
            if(!test){
                console.log("route: "+test.length);
                return res.status(400).json({message: "Nothing found"});
            } else return res.status(200).json({faq: test});
        }).catch(err => {
            res.status(500).json({message: "Error "+err.code});
        });
    }
};