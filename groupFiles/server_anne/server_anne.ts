// import of different modules
import express    = require ("express");
import { Request, Response } from "express";
import mysql = require("mysql2");
import {Connection, MysqlError, FieldInfo} from "mysql2";
//import user = require("./classes/User");
//import {User} from "../classes/User";
import * as cryptoJS from "crypto-js";

// import of different classes from other files


// initialising and declaring global variables
let router = express();

class User {
    // attributes
    id: number;
    vorname: string;
    nachname: string;
    time: string;  // e.g. 2017-10-29 17:33:08
    // methods
    // SuperComment: time verändert in :string und bei this.time in = time.
    constructor(id: number, vname: string, nname: string, time : string) {
        this.id = id;
        this.vorname = vname;
        this.nachname = nname;
        this.time = time;
    }
}

let userList: User[] = [];

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'creativeminds'
});

// router/express() listens on port 8080
router.listen(8080, "localhost", function () {
    console.log(`
    -------------------------------------------------------------
    Server wurde erfolgreich gestartet und läuft unter:
    Single Pager:
    http://localhost:8080/crud_user.html
    
    phpMyAdmin:
    http://localhost/phpmyadmin/
    -------------------------------------------------------------
  `);
    console.log("test2");
});

// routing different folders and main path. Folders contain stylesheets & scripts.
router.use("/",   express.static(__dirname + "/../"));
router.use("/jquery",  express.static(__dirname + "/../node_modules/jquery/dist/"));
router.use("/popper.js",    express.static(__dirname + "/../node_modules/popper.js/dist/"));
router.use("/bootstrap",    express.static(__dirname + "/../node_modules/bootstrap/dist/"));
router.use("/font-awesome", express.static(__dirname + "/../node_modules/@fortawesome/fontawesome-free/"));


// create
router.post("/user", function (req: Request, res: Response) {
    let vorname: string = (req.body.vorname ? req.body.vorname : "").trim();
    let nachname: string = (req.body.nachname ? req.body.nachname : "").trim();
    let message: string = "";

    if ((vorname == "") || (nachname == "")) return res.status(400).json({message: "vorname or nachname not provided", userList : userList});

    let insertData : [string, string, string, string, string] =
        [ new Date().toLocaleString(), vorname, cryptoJS.MD5("secret1").toString(), vorname, nachname ];
    let query : string = 'INSERT INTO userList (time, username, password, vorname, nachname ) VALUES (?,?,?,?,?);';
    connection.query(query, insertData, (err: MysqlError | null) => {
        if (!err) {
            userList.push(new User(userList.length, insertData[3], insertData[4], insertData[0]));
            message = vorname + " " + nachname + " successfully added";
            res.status(201).json({message: message, userList: userList});
        } else {
            res.status(200).json({message : "Select all error: " + err.code});
        }
    });
});


// read
router.get("/user", function (req: Request, res: Response) {
    console.log("get");
    let id: number = req.params.id;
    //--- process parameters ----------------------------------------------------

    if(isNaN(id)) return res.status(200).json({message : "ID ist keine Zahl...", userList : userList});

    userList = [];
    let query : string = 'SELECT id,time,username,vorname,nachname FROM userlist WHERE id = '+id+';';
    connection.query(query, (err: MysqlError|null, rows: any) => {
        if (!err) {
            if(!rows[0]) return res.status(200).json({message : "Nutzer mit der ID nicht gefunden...", userList : userList});
            userList.push(new User(rows[0].id, rows[0].vorname, rows[0].nachname, rows[0].time));
            res.status(200).json({message : "There are " + userList.length + " users in list", userList : userList})
        } else {
            res.status(200).json({message : "Select all error: " + err.code});
        }
    });
});


// update
router.put("/user/:id", function (req: Request, res: Response) {
    let vorname: string = (req.body.vorname ? req.body.vorname : "").trim();
    let nachname: string = (req.body.nachname ? req.body.nachname : "").trim();
    let id: number = req.params.id;
    let sqlBuilder : string = "UPDATE userlist SET ";

    if(isNaN(id)) return res.status(200).json({message : "ID ist keine Zahl...", userList : userList});
    if (vorname !== "") {
        sqlBuilder += "vorname = '" + vorname + "',";
    }
    if (nachname !== "") {
        sqlBuilder += "nachname = '" + nachname + "',";
    }
    if (vorname == "" && nachname == "") {
        return res.status(200).json({message : "Es wurde keine Änderung vorgenommen..?", userList : userList});
    }

    userList = [];
    sqlBuilder = sqlBuilder.slice(0, -1) + " WHERE id = "+id+";";
    connection.query(sqlBuilder, (err: MysqlError|null, rows: any) => {
        if (!err) {
            if(!rows) return res.status(200).json({message : "Nutzer mit der ID nicht gefunden...", userList : userList});
            if(rows.affectedRows === 1) {
                userList.push(new User(id, vorname, nachname, null));
                res.status(200).json({message: "Entry changed...", userList: userList})
            } else {
                res.status(200).json({message: "No entry has been changed...", userList: userList})
            }
        } else {
            res.status(200).json({message : "Select all error: " + err.code});
        }
    });
});


// delete
router.delete("/user/:id", function (req: Request, res: Response) {
    let id: number = req.params.id;
    //--- process parameters ----------------------------------------------------
    if(isNaN(id)) return res.status(200).json({message : "ID ist keine Zahl...", userList : userList});

    userList = [];
    let query : string = 'DELETE FROM userlist WHERE id = '+id+';';
    connection.query(query, (err: MysqlError|null, rows: any) => {
        if (!err) {
            if(rows.affectedRows === 0) return res.status(200).json({message : "Nutzer mit der ID nicht gefunden...", userList : userList});
            let query : string = 'SELECT id,time,username,vorname,nachname FROM userlist;';
            connection.query(query, (err: MysqlError|null, rows: any) => {
                if (!err) {
                    for (let i=0; i < rows.length; i++) {
                        if(rows[i]) {
                            userList.push(new User(rows[i].id, rows[i].vorname, rows[i].nachname, rows[i].time))
                        }
                    }
                    res.status(200).json({message : "There are " + userList.length + " users in list", userList : userList})
                } else {
                    res.status(200).json({message : "Select all error: " + err.code});
                }
            });
        } else {
            res.status(200).json({message : "Select all error: " + err.code});
        }
    });
});