// import of different modules
//import express    = require ("express");
import * as express from "express";
import bodyparser = require ("body-parser");
import {Request, Response} from "express";
import * as mysql from "mysql2";         // handles database connections
import {Connection, MysqlError} from "mysql2";
import {UserList} from "../server_Andreas/classes/UserList";
import {MySQL_Handler} from "../server_Andreas/classes/MySQL_Handler";

let router = express();
let productCrudRoute = require ("./routes/productCRUD_route");
let faqRoute = require ("./routes/faq_routes");

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'creativeminds'
});


let mySQL = new MySQL_Handler();

// router/express() listens on port 8080
router.listen(8080, "localhost", function () {
    console.log(`
    -------------------------------------------------------------
    Server wurde erfolgreich gestartet und läuft unter:
    Single Pager:
    http://localhost:8080/test.html
    
    phpMyAdmin:
    http://localhost/phpmyadmin/
    -------------------------------------------------------------
  `);
});

// routing different folders and main path. Folders contain stylesheets & scripts.
router.use("/", express.static(__dirname + "/../"));
router.use("/apiDoc", express.static(__dirname + "/apiDoc/"));
router.use("/jquery", express.static(__dirname + "/../../node_modules/jquery/dist/"));
router.use("/popper.js", express.static(__dirname + "/../../node_modules/popper.js/dist/"));
router.use("/bootstrap", express.static(__dirname + "/../../node_modules/bootstrap/dist/"));
router.use("/font-awesome", express.static(__dirname + "/../../node_modules/@fortawesome/fontawesome-free/"));
router.use(express.json());  // parsing json
router.use(bodyparser.urlencoded({extended: true}));


router.route("/productList").get(productCrudRoute.productList);
router.route("/findProductById/:id").get(productCrudRoute.findProductById);
router.route("/deleteProductById/:id").get(productCrudRoute.deleteProduct);
router.route("/createProduct").post(productCrudRoute.createProduct);
router.route("/updateProductById/:id").put(productCrudRoute.updateProduct);
router.route("/getCategoryList").get(faqRoute.getCategoryList);

/**
 * @api {post} /user Create User
 * @apiName CreateUser
 * @apiGroup User
 *
 * @apiDescription Empfaengt Daten für einen neuen Nutzer und legt ihn an, sofern die übergebene Email-Adresse nicht
 * bereits in Benutzung ist. Sollte die Email-Adresse bereits vergeben sein, wird eine entsprechende Fehlermeldung ausgegeben.
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Select all error: <code>error.code</code>"
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Internal Server Error
 *     {
 *       "message": "Email address already exists."
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "No user found."
 *     }
 *
 * @apiSuccess {Any[]} userList Bestaetigung ueber erfolgreichen Eintrag des neuen Nutzers und Liste aller Nutzer.
 * Je nach Registrierungsstatus werden nur bestimmte Attribute angezeigt.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        message: "User successfully added."
 *       "userList" : "userList"
 *     }
 *
 * @apiVersion 1.0.0
 */
router.post("/user", (req, res) => {
        let vorname: string = (req.body.vorname ? req.body.vorname : "").trim();
        let nachname: string = (req.body.nachname ? req.body.nachname : "").trim();
        let nickname: string = (req.body.nickname ? req.body.nickname : "").trim();
        let email: string = (req.body.email ? req.body.email : "").trim();

        let userList: UserList[] = [];

        if ((vorname == "") || (nachname == "")) return res.status(400).json({
            message: "vorname or nachname not provided",
            userList: userList
        });

        let insertData: [string, string, string, string] =
            [vorname, nachname, nickname, email];

        let query: string = 'INSERT INTO user(forename, surname, nickname, email ) VALUES (?,?,?,?);';

       connection.query('SELECT * FROM user WHERE email = ' + "'" + email + "'", function (err: MysqlError, result) {
            if (result[0]) {
                if(result[0].email === email) {
                    console.log("===");
                    return res.status(400).json({message: "Email address already exists"});
                }
            } else {
                connection.query(query, insertData, function (err: MysqlError, result) {
                    if (!err) {
                        connection.query('SELECT ID, forename, surname, nickname, email FROM user;', function (err: MysqlError, result) {
                            if(!err) {
                                if (!result[0]) {
                                    return res.status(500).json({message: "No user found."});
                                }
                                for (let i: number = 0; i < result.length; i++) {
                                    if (result[i]) {
                                        userList.push(new UserList(result[i].ID, result[i].forename, result[i].surname, result[i].nickname, result[i].email));
                                        console.log("nickname: "+result[i].nickname);
                                    }
                                }
                                res.status(200).json({
                                    message: vorname + " " + nachname + " successfully added",
                                    userList: userList
                                });
                            }
                        });
                    } else return res.status(500).json({message: "Select all error: " + err.code});
                });
            }
        });
});

/**
 * @api {get} /userList Userlist
 * @apiName GetUserList
 * @apiGroup User
 *
 * @apiDescription Sucht alle Nutzer aus der Datenbank. Angezeigt werden die Attribute Vorname, Nachname, Nickname und EMail-Adresse.
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Select all error: <code>error.code</code>"
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "No user data found."
 *     }
 *
 * @apiSuccess {Any[]} userList Liste aller Nutzer.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "userList: userList"
 *     }
 *
 * @apiVersion 1.0.0
 */
router.get("/userList", function (req: Request, res: Response) {
    let userList: UserList[] = [];
    let query: string = 'SELECT ID, forename, surname, nickname, email FROM user;';

    mySQL.query(query).then(rows => {
        let result: any = rows;
        if (!result[0]) return res.status(500).json({message: "No user data found."});
        for (let i: number = 0; i < result.length; i++) {
            if (result[i]) {
                userList.push(new UserList(result[i].ID, result[i].forename, result[i].surname, result[i].nickname, result[i].email));
            }
        }
        return res.status(200).json({userList: userList});
    }).catch(err => {
        res.status(500).json({message: "Select all error: " + err.code});
    });
    // mySQL.close();
});


/**
 * @api {get} /user/:id User nach ID suchen
 * @apiName FindUserByID
 * @apiGroup User
 *
 * @apiDescription Sucht einen Nutzer anhand seiner ID aus der Datenbank. Angezeigt werden die Attribute ID, Vorname,
 * Nachname, Nickname und EMail-Adresse.
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Error: <code>error.code</code>"
 *     }
 *
 * @apiParam {number} 1 ID eines in der Datenbank vorhandenen Nutzers.
 *
 * @apiSuccess {Any[]} ID, Vorname, Nachname, Nickname und Email-Adresse des gesuchten Nutzers.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "userList: userList"
 *     }
 *
 * @apiVersion 1.0.0
 */
router.get("/user/:id", (req, res) => {
    let id: number = (req.params.id ? req.params.id : "").trim();
    let userList: UserList[] = [];
    if (isNaN(id)) {
        return res.status(200).json({message: id + " is not a number"});
    }
    let query = 'SELECT ID, forename, surname, nickname, email FROM user WHERE ID =' + id + ';';

    mySQL.query(query).then(rows => {
        let result: any = rows;
        if (result.affectedRows != 0) {
            userList.push(new UserList(result[0].ID, result[0].forename, result[0].surname, result[0].nickname, result[0].email));
            return res.status(200).json({message: "", userList: userList});
        }
    }).catch(err => {
        res.status(500).json({message: "Select all error: " + err.code});
    });

    //  mySQL.close();
});

/**
 * @api {put} /user/:id Nutzer editieren
 * @apiName UpdateUser
 * @apiGroup User
 *
 * @apiDescription Editiert einen Nutzer anhand seinee Datenbank-ID.
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Select all error: <code>error.code</code>"
 *     }
 *
 * @apiParam {number} 1 ID eines in der Datenbank vorhandenen Nutzers.
 *
 * @apiSuccess {Any[]} userList Bestaeting ueber erfolgreiche Aenderung des Nutzers und Liste aller Nutzer.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message" : "User updated successfully",
 *       "userList" : "userList"
 *     }
 *
 * @apiVersion 1.0.0
 */
router.put("/user/:id", function (req: Request, res: Response) {
    let id: number = req.params.id;
    let vorname: string = (req.body.vorname ? req.body.vorname : "").trim();
    let nachname: string = (req.body.nachname ? req.body.nachname : "").trim();
    let nickname: string = (req.body.nickname ? req.body.nickname : "").trim();
    let email: string = (req.body.email ? req.body.email : "").trim();

    let query: string = "";
    let userList: UserList[] = [];

    if (isNaN(id)) {
        return res.status(200).json({message: id + " is not a number"});
    } else {
        // check if email already exists
        connection.query('SELECT * FROM user WHERE email = ' + "'" + email + "'", function (err: MysqlError, result) {
            if (result[0]) {
                if (result[0].email === email) {
                    console.log("===");
                    return res.status(400).json({message: "Email address already exists"});
                }
            }
        });

        query = 'UPDATE user SET ';
        if (vorname == "" && nachname == "" && nickname == "" && email == "") {
            res.status(200).json({message: "No input data"});
        } else {
            if (vorname !== "") {
                query += 'forename = ' + '"' + vorname + '"' + ",";
            }
            if (nachname !== "") {
                query += 'surname = ' + '"' + nachname + '"' + ",";
            }
            if (nickname !== "") {
                query += 'nickname = ' + '"' + nickname + '"' + ",";
            }
            if (email !== "") {
                query += 'email = ' + '"' + email + '"' + ";";
            }
        }

        query = query.substring(0, query.length - 1) + ' WHERE ID = ' + "'" + id + "'";
        console.log("query: " + query);
        mySQL.query(query).then(rows => {
            let result: any = rows;
            if (result.affectedRows != 0) {
                userList.push(new UserList(id, result.forename, result.surname, result.nickname, result.email));
            }
            return res.status(200).json({message: "User updated successfully", userList: userList});
        }).catch(err => {
            res.status(400).json({message: "Select all error: " + err.code});
        });

    }
});

/**
 * @api {delete} /delete Nutzer loeschen
 * @apiName DeleteUser
 * @apiGroup User
 *
 * @apiDescription Loescht einen Nutzer anhand seiner ID.
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Select all error: <code>error.code</code>"
 *     }
 *
 * @apiParam {number} 1 ID eines zu loeschenden Nutzers.
 *
 * @apiSuccess {Any[]} Bestaetigung ueber erfolgreiches Loeschen eine Nutzers.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       message: "User with ID deleted"
 *     }
 *
 * @apiVersion 1.0.0
 */
router.delete("/user/:id", function (req: Request, res: Response) {
    let id: number = req.params.id;
    let userList: UserList[] = [];

    // check if id is a number
    if (isNaN(id)) {
        return res.status(200).json({message: id + " is not a number"});
    }
    let query = 'DELETE FROM user WHERE ID =' + id + ';';

    mySQL.query(query).then(rows => {
        let result: any = rows;
        if (result.affectedRows != 0) {
            res.status(200).json({message: "User with ID " + id + " deleted", userList: userList});
        }
    }).catch(err => {
        res.status(400).json({message: "Error " + err.code});
    });

});

/* email in table address

 mySQL.query(query).then(rows => {
     let result: any = rows;
     if (result.affectedRows != 0) {
         console.log("user deleted ok");
         query = 'DELETE FROM address WHERE ID_user = ' + id + ';';
         mySQL.query(query).then(rows => {
             result = rows;
             if (result.affectedRows != 0) {
                 console.log("affected "+ result.affectedRows);
                 res.status(200).json({message: "User with ID " + id + " deleted", userList: userList});
             }
         }).catch(err => {
             res.status(400).json({message: "Error " + err.code});
         });

     } else {
         res.status(400).json({message: "Failed to delete user with ID " + id, userList: userList});
     }
 }).catch(err => {
     res.status(400).json({message: "Error " + err.code});
 });*/
//  mySQL.close();


/**
 * @api {get} /product/:id Produkt nach ID suchen
 * @apiName GetProduct
 * @apiGroup Product
 *
 * @apiDescription Sucht ein Produkt anhand seine ID aus der Datenbank.
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Select all error: <code>error.code</code>"
 *     }
 *
 * @apiParam {number} 1 ID des Produkts.
 *
 *
 * @apiSuccess {Any[]} productList Gesuchtes Produkt.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "productList": "productList
 *     }
 *
 * @apiVersion 1.0.0
 */
/*router.get("/product/:id", (req, res) => {
    let id: number = req.params.id;

    let productList: ProductList[] = [];

    if (isNaN(id)) {
        return res.status(200).json({message: id + " is not a number"});
    }

    let query = 'SELECT name, ' +
        'price, ' +
        'length,' +
        'width, ' +
        'height,' +
        'display_size,' +
        'resolution,' +
        'advanced_controls,' +
        'productivity_supplies,' +
        'processor,' +
        'graphics_card,' +
        'memory,' +
        'product_type,' +
        'micro_sd_slot,' +
        'ram,' +
        'extensible_memory,' +
        'picture' +
        ' FROM product WHERE ID = ' + '"' + id + '";';

    mySQL.query(query).then(rows => {
        let result: any = rows;
        if (result.affectedRows != 0) {
            if (result[0].extensible_memory) {
                result[0].extensible_memory = false;
            } else (result[0].extensible_memory = true);
            productList.push(new ProductList(
                id,
                result[0].name,
                result[0].price,
                result[0].length,
                result[0].width,
                result[0].height,
                result[0].display_size,
                result[0].resolution,
                result[0].advanced_controls,
                result[0].productivity_supplies,
                result[0].processor,
                result[0].graphics_card,
                result[0].memory,
                result[0].product_type,
                result[0].micro_sd_slot,
                result[0].ram,
                result[0].extensible_memory,
                result[0].picture));
            console.log("prod name: " + productList[0].resolution);
            res.status(200).json({message: "", productList: productList});
        }
    }).catch(err => {
        res.status(400).json({message: "Select all error " + err.code});
    });
});*/


/* used once upon a time (for about 15 minutes), when email was still in table address
function queryBuilder(id: number, vorname: string, nachname: string, nickname: string, email: string) : string {
    let query: string = "";
    let cols: string = "";
    let tables: string = "";
    let condition: string = "";


    if (email == "") {
        tables = ' user ';
        condition = ' user.ID = ' + '"' + id + '"';
    } else {
        if (vorname == "" && nachname == "" && nickname == "") {
            condition = ' address.ID_user = ' + '"' + id + '"';
            tables = ' address ';
        } else {
            tables = ' user, address ';
            condition = ' user.ID = ' + '"' + id + '"' + ' AND address.ID_user = user.ID ';
        }
    }

    if (vorname !== "") {
        cols = 'user.forename = ' + '"' + vorname + '"' + ", ";
    }
    if (nachname !== "") {
        cols += 'user.surname = ' + '"' + nachname + '"' + ", ";
    }
    if (nickname !== "") {
        cols += 'user.nickname = ' + '"' + nickname + '"' + ", ";
    }
    if (email !== "") {
        cols += 'address.email = ' + '"' + email + '"' + "; ";
    }
    console.log("cols 1: " +cols);
    cols = cols.substring(0, cols.length-2);
    console.log("cols 2: "+cols);
    query = 'UPDATE ' + tables + ' SET ' + cols + ' WHERE ' + condition + ';';
    console.log("query: " + query);

    return query;
}*/