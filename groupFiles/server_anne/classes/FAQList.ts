import {MySQL_Handler} from "../../../server/classes/MySQL_Handler";
import {FAQ} from "./FAQ";

let mySQL = new MySQL_Handler();

export class FAQList {

    public getFAQList(category: string): Promise<any> {
        let query: string = 'SELECT questi, answer FROM faq_questions_answers WHERE ID_faq_categories = ';

        switch (category) {
            case 'rent' :
                query += "'" + 1 + "';";
                break;
            case 'delivery' :
                query += "'" + 2 + "';";
                break;
            case 'repair' :
                query += "'" + 3 + "';";
                break;
        }

        return mySQL.query(query).then(rows => {
            let result: any = rows;
            let faq: FAQ[] = [];
            if (result != 0) {
                for (let i = 0; i < result.length; i++) {
                    faq.push(new FAQ(result[i].questi, result[i].answer));
                    console.log("q: " + result[i].questi + ", a: " + result[i].answer);
                }
            }
            console.log("list faq.length "+faq.length);
            return {faq: faq};
        });
    }
}