import express =require("express");
import { Request, Response } from "express";

let router = express();


router.listen(8080, "localhost", function () {
    console.log(`
    -------------------------------------------------------------
    Server wurde erfolgreich gestartet und läuft unter:
    Single Pager:
    http://localhost:8080/
    
    phpMyAdmin:
    http://localhost/phpmyadmin/
    -------------------------------------------------------------
  `);
});

router.use("/",   express.static(__dirname));
router.use("/jquery",  express.static(__dirname + "/../node_modules/jquery/dist/"));
router.use("/popper.js",    express.static(__dirname + "/../node_modules/popper.js/dist/"));
router.use("/bootstrap",    express.static(__dirname + "/../node_modules/bootstrap/dist/"));
router.use("/font-awesome", express.static(__dirname + "/../node_modules/@fortawesome/fontawesome-free/"));
router.use("/crypto-js", express.static(__dirname + "/../node_modules/crypto-js/"));
router.use(express.json());

router.get("/contact", function(req:Request, res: Response){
   console.log(req.body);
});

router.post("/contact", function(req:Request, res:Response){
    console.log(req.body);
   let vname: string = req.body.cName;
   let nname: string = req.body.cLastName;
   let tel: string = req.body.cT;
   let mail: string = req.body.cE;
   console.log("Hallo " + vname);
   res.status(200).json({message:"received contact formular from " + vname});

});