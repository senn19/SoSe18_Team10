/*class User {
    id: number;
    vorname: string;
    nachname: string;
    time: string;
}*/

$(function () {
    $('#createBtn').on("click", () => {
        let vorname: string = ($('#forenameInput').val() as string).trim();
        let nachname: string = ($('#surnameInput').val() as string).trim();
        let nickname: string = ($('#nicknameInput').val() as string).trim();
        let email: string = ($('#emailInput').val() as string).trim();

        let data: Object = {vorname: vorname, nachname: nachname, nickname: nickname, email: email};
        $.ajax({                // set up ajax request
            url: '/user',
            type: 'POST',    // POST-request for CREATE
            data: JSON.stringify(data),
            contentType: 'application/json',  // using json in request
            dataType: 'json',              // expecting json in response
            success: (data) => {
                render(data.message, data.userList);
                // readList();
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            }
        });
    });
});

$(function () {
    $('#readBtn').on("click", () => {
        let id: string = $('#userIDInput').val() as string;
        console.log("readBtn");
        $.ajax({                // set up ajax request
            url: 'user/' + id,
            type: 'GET',    // GET-request for READ
            dataType: 'json',   // expecting json
            success: (data) => {
                console.log(data.userList);
                render(data.message, data.userList)
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            }
        });
    });

});

$(function () {
    $('#readListBtn').on("click", () => {
        $.ajax({                // set up ajax request
            url: '/userList',
            type: 'GET',    // GET-request for READ
            dataType: 'json',   // expecting json
            success: (data) => {
                console.log(data.userList);
                render(data.message, data.userList);
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            }
        });
    });

});

$(function () {
    $('#readCatListBtn').on("click", () => {
        $.ajax({                // set up ajax request
            url: '/getCategoryList',
            type: 'GET',    // GET-request for READ
            dataType: 'json',   // expecting json
            success: (data) => {
                console.log(data.catList);
                render(data.message, data.userList);
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            }
        });
    });

});

$(function () {
    $('#deleteBtn').on("click", () => {
        console.log("deleteBtn");
        let id: string = $('#userIDInput').val() as string;
        $.ajax({                // set up ajax request
            url: '/user/' + id,
            type: 'DELETE',    // GET-request for READ
            dataType: 'json',   // expecting json
            success: (data) => {
                render(data.message, data.userList)
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            },
        });
    });

});


$(function () {
    $('#updateBtn').on("click", () => {
        console.log("click");
        let id: string = $('#userIDInput').val() as string;
        let vorname: string = ($('#forenameInput').val() as string).trim();
        let nachname: string = ($('#surnameInput').val() as string).trim();
        let nickname: string = ($('#nicknameInput').val() as string).trim();
        let email: string = ($('#emailInput').val() as string).trim();
        let data: Object = {vorname: vorname, nachname: nachname, nickname: nickname, email: email};

        $.ajax({
            url: '/user/' + id,
            type: 'PUT',    // PUT-request for update
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            success: (data) => {
                render(data.message, data.userList)
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            },
        });
    });

});
/*
$(function () {
    $('#readProductBtn').on("click", () => {
        let id: string = $('#productIdInput').val() as string;

        $.ajax({
            url: '/product/' + id,
            type: 'GET',
            dataType: 'json',
            success: (data) => {
                renderProductList(data.productList)
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            },
        });
    });
});
*/

$(function () {
    $('#readProductBtn').on("click", () => {
        let id: string = $('#productIdInput').val() as string;
        console.log("client");
        $.ajax({
            url: '/findProductById/' + id,
            type: 'GET',
            dataType: 'json',
            success: (data) => {
                renderProductList(data.message, data.productList)
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            },
        });
    });
});

$(function () {
    $('#deleteProductBtn').on("click", () => {
        let id: number = $('#productIdInput').val() as number;
        console.log("client");
        $.ajax({
            url: '/deleteProductById/' + id,
            type: 'GET',
            dataType: 'json',
            success: (data) => {
                renderProductList(data.message, data.productList)
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            },
        });
    });
});

$(function () {
    $('#updateProductBtn').on("click", () => {
        console.log("update");
        let id: number = $('#productIdInput').val() as number;
        let pIDUser: number =  $('#productIDUserInput').val() as number;
        let pIDCategory: number =  $('#productIDCategoryInputInput').val() as number;
        let pPrice: number = ($('#productPriceInput').val() as number);
        let pName: string = ($('#productNameInput').val() as string).trim();
        let pLength: string = ($('#productLengthInput').val() as string).trim();
        let pWidth: string = ($('#productWithInput').val() as string).trim();
        let pHeight: string = ($('#productHeightInput').val() as string).trim();
        let pDisplaySize: string = ($('#productDisplayInput').val() as string);
        let pResolution: string = ($('#productResolutionInput').val() as string).trim();
        let pAdvancedCtrls: string = ($('#productAdvancedCtrlsInput').val() as string).trim();
        let pProdSupp: string = ($('#productProdSuplInput').val() as string);
        let pProcessor: string = ($('#productProcessorInput').val() as string);
        let pGraphicsCard: string = ($('#productGraphicsCardInput').val() as string).trim();
        let pMemory: string = ($('#productMemoryInput').val() as string).trim();
        let pType: string = ($('#productTypeInput').val() as string).trim();
        let pMicroSDSlot: string = ($('#productMicroSDInput').val() as string).trim();
        let pRam: string = ($('#productRAMInput').val() as string);
        let pExtMem: number = ($('#productExtMemInput').val() as number);
        let pPic: string = ($('#productPicInput').val() as string).trim();

        let data: Object = {
            pIDUSer: pIDUser,
            pIDCategory: pIDCategory,
            pPrice: pPrice,
            pName: pName,
            pLength: pLength,
            pWidth: pWidth,
            pHeight: pHeight,
            pDisplaySize: pDisplaySize,
            pResolution: pResolution,
            pAdvancedCtrls: pAdvancedCtrls,
            pProdSupp: pProdSupp,
            pProcessor: pProcessor,
            pGraphicsCard: pGraphicsCard,
            pMemory: pMemory,
            pType: pType,
            pMicroSDSlot: pMicroSDSlot,
            pRam: pRam,
            pExtMem: pExtMem,
            pPic: pPic
        };

        $.ajax({
            url: '/updateProductById/' + id,
            type: 'PUT',    // PUT-request for update
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            success: (data) => {
                renderProductList(data.message, data.productList);
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            },
        });
    });

});

$(function () {
    $('#createProductBtn').on("click", () => {
        console.log("create product");
        let pIDUser: number =  $('#productIDUserInput').val() as number;
        let pIDCategory: number =  $('#productIDCategoryInputInput').val() as number;
        let pPrice: number = ($('#productPriceInput').val() as number);
        let pName: string = ($('#productNameInput').val() as string).trim();
        let pLength: string = ($('#productLengthInput').val() as string).trim();
        let pWidth: string = ($('#productWithInput').val() as string).trim();
        let pHeight: string = ($('#productHeightInput').val() as string).trim();
        let pDisplaySize: string = ($('#productDisplayInput').val() as string);
        let pResolution: string = ($('#productResolutionInput').val() as string).trim();
        let pAdvancedCtrls: string = ($('#productAdvancedCtrlsInput').val() as string).trim();
        let pProdSupp: string = ($('#productProdSuplInput').val() as string);
        let pProcessor: string = ($('#productProcessorInput').val() as string);
        let pGraphicsCard: string = ($('#productGraphicsCardInput').val() as string).trim();
        let pMemory: string = ($('#productMemoryInput').val() as string).trim();
        let pType: string = ($('#productTypeInput').val() as string).trim();
        let pMicroSDSlot: string = ($('#productMicroSDInput').val() as string).trim();
        let pRam: string = ($('#productRAMInput').val() as string);
        let pExtMem: number = ($('#productExtMemInput').val() as number);
        let pPic: string = ($('#productPicInput').val() as string).trim();

        let data: Object = {
            pIDUSer: pIDUser,
            pIDCategory: pIDCategory,
            pPrice: pPrice,
            pName: pName,
            pLength: pLength,
            pWidth: pWidth,
            pHeight: pHeight,
            pDisplaySize: pDisplaySize,
            pResolution: pResolution,
            pAdvancedCtrls: pAdvancedCtrls,
            pProdSupp: pProdSupp,
            pProcessor: pProcessor,
            pGraphicsCard: pGraphicsCard,
            pMemory: pMemory,
            pType: pType,
            pMicroSDSlot: pMicroSDSlot,
            pRam: pRam,
            pExtMem: pExtMem,
            pPic: pPic
        };

        $.ajax({
            url: '/createProduct',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            success: (data) => {
                renderProductList(data.message, data.productList)
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            }
        });
    });
});
/*   id: number;
    name: string;
    price: string;
    length: string;
    width: string;
    height: string;
    display_size: string;
    resolution: string;
    advanced_controls: string;
    productivity_supplies: number;
    processor: string;
    graphics_card: string;
    memory: string;
    product_type: string;
    micro_sd_slot: number;
    ram: string;
    extensible_memory: boolean;
    picture: string; //url
*/

$(function () {
    $('#readProductListBtn').on("click", () => {
        console.log("readprod");
        $.ajax({
            url: '/productList',
            type: 'GET',
            dataType: 'json',
            success: (data) => {
                renderProductList(data.message, data.productList)
            },
            error: (jqXHR) => {
                render(jqXHR.responseJSON.message, [])
            },
        });
    });
});

function renderProductList(message, productList): void {
    let buffer: string = "";
    console.log("prodlist.length: " + productList.length);
    console.log("ID: "+ productList[0].ID);
    buffer += "<div id='serverMessage'> " + message + "</div>\n";
    if (productList.length > 0) {
        buffer += "<div id='prodList'>\n";
        buffer += "  <table>\n";
        buffer += "    <tr>\n";
        buffer += "      <th> ID </th>\n";
        buffer += "      <th> Name </th>\n";
        buffer += "      <th> Price  </th>\n";
        buffer += "      <th> Length </th>\n";
        buffer += "      <th> Width </th>\n";
        buffer += "      <th> Height  </th>\n";
        buffer += "      <th> Display size </th>\n";
        buffer += "      <th> Resolution </th>\n";
        buffer += "      <th> Advanced Controls  </th>\n";
        buffer += "      <th> Productivity Supplies </th>\n";
        buffer += "      <th> Processor </th>\n";
        buffer += "      <th> Graphics Card  </th>\n";
        buffer += "      <th> Memory </th>\n";
        buffer += "      <th> Product Type </th>\n";
        buffer += "      <th> Micro SD Slot  </th>\n";
        buffer += "      <th> RAM </th>\n";
        buffer += "      <th> Extensible Memory </th>\n";
        buffer += "      <th style='width:50px;'> Picture </th>\n";
        buffer += "    </tr>\n";
        for (let product in productList) {
            if (productList[product] != null) {
                buffer += "<tr id='productID'" + productList[product].ID + "'>\n";
                buffer += "<td> " + productList[product].ID+ "</td>\n";
                buffer += "<td> " + productList[product].name + "</td>\n";
                buffer += "<td> " + productList[product].price + "</td>\n";
                buffer += "<td> " + productList[product].length + "</td>\n";
                buffer += "<td> " + productList[product].width + "</td>\n";
                buffer += "<td> " + productList[product].height + "</td>\n";
                buffer += "<td> " + productList[product].display_size + "</td>\n";
                buffer += "<td> " + productList[product].resolution + "</td>\n";
                buffer += "<td> " + productList[product].advanced_controls + "</td>\n";
                buffer += "<td> " + productList[product].productivity_supplies + "</td>\n";
                buffer += "<td> " + productList[product].processor + "</td>\n";
                buffer += "<td> " + productList[product].graphics_card + "</td>\n";
                buffer += "<td> " + productList[product].memory + "</td>\n";
                buffer += "<td> " + productList[product].product_type + "</td>\n";
                buffer += "<td> " + productList[product].micro_sd_slot + "</td>\n";
                buffer += "<td> " + productList[product].ram + "</td>\n";
                buffer += "<td> " + productList[product].extensible_memory + "</td>\n";
                buffer += "<td> " + productList[product].picture + "</td>\n";

                buffer += "</tr>\n";
            }
        }
    }
    buffer += "</table>";
    buffer += "</div>";
    $('#data').html(buffer);
}

function renderProduct(message, product): void {
    let buffer: string = "";

    buffer += "<div id='serverMessage'> " + message + "</div>\n";

    buffer += "<div id='prodList'>\n";
    buffer += "  <table>\n";
    buffer += "    <tr>\n";
    buffer += "      <th> ID </th>\n";
    buffer += "      <th> Name </th>\n";
    buffer += "      <th> Price  </th>\n";
    buffer += "      <th> Length </th>\n";
    buffer += "      <th> Width </th>\n";
    buffer += "      <th> Height  </th>\n";
    buffer += "      <th> Display size </th>\n";
    buffer += "      <th> Resolution </th>\n";
    buffer += "      <th> Advanced Controls  </th>\n";
    buffer += "      <th> Productivity Supplies </th>\n";
    buffer += "      <th> Processor </th>\n";
    buffer += "      <th> Graphics Card  </th>\n";
    buffer += "      <th> Memory </th>\n";
    buffer += "      <th> Product Type </th>\n";
    buffer += "      <th> Micro SD Slot  </th>\n";
    buffer += "      <th> RAM </th>\n";
    buffer += "      <th> Extensible Memory </th>\n";
    buffer += "      <th style='width:50px;'> Picture </th>\n";
    buffer += "    </tr>\n";
    buffer += "<tr id='productID'" + product.id + "'>\n";
    buffer += "<td> " + product.id + "</td>\n";
    buffer += "<td> " + product.name + "</td>\n";
    buffer += "<td> " + product.price + "</td>\n";
    buffer += "<td> " + product.length + "</td>\n";
    buffer += "<td> " + product.width + "</td>\n";
    buffer += "<td> " + product.height + "</td>\n";
    buffer += "<td> " + product.display_size + "</td>\n";
    buffer += "<td> " + product.resolution + "</td>\n";
    buffer += "<td> " + product.advanced_controls + "</td>\n";
    buffer += "<td> " + product.productivity_supplies + "</td>\n";
    buffer += "<td> " + product.processor + "</td>\n";
    buffer += "<td> " + product.graphics_card + "</td>\n";
    buffer += "<td> " + product.memory + "</td>\n";
    buffer += "<td> " + product.product_type + "</td>\n";
    buffer += "<td> " + product.micro_sd_slot + "</td>\n";
    buffer += "<td> " + product.ram + "</td>\n";
    buffer += "<td> " + product.extensible_memory + "</td>\n";
    buffer += "<td> " + product.picture + "</td>\n";
    buffer += "</tr>\n";
    buffer += "</table>";
    buffer += "</div>";
    $('#data').html(buffer);
}

function render(message: string, userList): void {
    //console.log("userList: "+userList[0]);
    let buffer: string = "";
    //--- render message --------------------------------------------------------
    buffer += "<div id='serverMessage'> " + message + "</div>\n";
    //--- render table (only if userData exists) --------------------------------
    if (userList.length > 0) {
        buffer += "<div id='userList'>\n";
        buffer += "  <table>\n";
        buffer += "    <tr>\n";
        buffer += "      <th style='width:20px '> id       </th>\n";
        buffer += "      <th style='width:100px'> vorname  </th>\n";
        buffer += "      <th style='width:100px'> nachname </th>\n";
        buffer += "      <th style='width:100px'> nickname </th>\n";
        buffer += "      <th style='width:100px'> email </th>\n";
        buffer += "    </tr>\n";
        for (let user in userList) { // iterate through array "userData"
            if (userList[user] != null) {  // ignore array-elements that have been deleted
                buffer += "    <tr id='user'" + userList[user].id + "'>\n";
                buffer += "      <td> " + userList[user].id + " </td>\n";
                buffer += "      <td> " + userList[user].forename + " </td>\n";
                buffer += "      <td> " + userList[user].surname + " </td>\n";
                buffer += "      <td> " + userList[user].nickname + " </td>\n";
                buffer += "      <td> " + userList[user].email + " </td>\n";
                buffer += "    </tr>\n";
            }
        }
    }
    //--- close table (and div) ------------------------------------------------
    buffer += "  </table>\n";
    buffer += "</div>";
    //--- put buffer-string into message and userlist --------------------------
    $('#data').html(buffer);


    //
    // Examples of loops: All loops provide the same results
    //
    // console.log("\n---------------------\n for-of Schleife)");
    for (let entry of userList) { // entry get value of all elements in list
        if (entry != null) {  // ignore array-elements that have been deleted
            console.log(entry.vorname + " " + entry.nachname);
        }
    }
    //console.log("\n for-in Schleife");
    for (let j in userList) { // j get index of all elements in list
        if (userList[j] != null) {
            console.log(userList[j].vorname + " " + userList[j].nachname);
        }
    }
    // console.log("\n for-Schleife)");
    for (let i = 0; i < userList.length; i++) { // index starts with 0 and end with length of list-1
        if (userList[i] != null) {
            console.log(userList[i].vorname + " " + userList[i].nachname);
        }
    }
    // console.log("\n while-Schleife)");
    let i: number = 0;           // i start with 0
    while (i < userList.length) { // loop runs while i < length of list
        if (userList[i] != null) {
            console.log(userList[i].vorname + " " + userList[i].nachname);
        }
        i++; // increment i - i.e. i=i+1
    }
}

/*function readList() {
    console.log("readlist");
    $.ajax({                // set up ajax request
        url: '/userList',
        type: 'GET',    // GET-request for READ
        dataType: 'json',   // expecting json
        success: (data) => {
            console.log(data.userList);
            render(data.message, data.userList)
        },
        error: (jqXHR) => {
            render(jqXHR.responseJSON.message, [])
        }
    });

}*/

