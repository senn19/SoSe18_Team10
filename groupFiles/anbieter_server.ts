import express =require("express");
import { Request, Response } from "express";

import * as mysql from "mysql2";
import {Connection, MysqlError} from "mysql2";
import {error, isNumber} from "util";

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'creativeminds'
});

let router = express();


router.listen(8080, "localhost", function () {
    console.log(`
    -------------------------------------------------------------
    Server wurde erfolgreich gestartet und läuft unter:
    Single Pager:
    http://localhost:8080/
    
    phpMyAdmin:
    http://localhost/phpmyadmin/
    -------------------------------------------------------------
  `);
});


router.use("/",   express.static(__dirname));
router.use("/jquery",  express.static(__dirname + "/../node_modules/jquery/dist/"));
router.use("/popper.js",    express.static(__dirname + "/../node_modules/popper.js/dist/"));
router.use("/bootstrap",    express.static(__dirname + "/../node_modules/bootstrap/dist/"));
router.use("/font-awesome", express.static(__dirname + "/../node_modules/@fortawesome/fontawesome-free/"));
router.use("/crypto-js", express.static(__dirname + "/../node_modules/crypto-js/"));
router.use(express.json());

router.post("/anbieter", function(req:Request, res: Response){ //
    let Name : string = req.body.Name;
    let name : string = req.body.name;
    let address : string = req.body.address;
    let phone : string = req.body.phone;
    let email : string = req.body.email;
    let details: string = req.body.details;
    let description: string = req.body.description;
    let price: number = req.body.price;

    // prüfe, ob price eine Zahl ist
    if(isNaN(price)){
       res.status(500).json ({message: "please insert a price"})
    }
    else{
        res.status(200).json ({message: price + "successfully added"})
    }

    // datenbank: 'INSERT INTO <tabelle> (name) VALUES (?)';
    let insertData : [string, string, string, string, string, string, string ] = [Name ,name, address, phone, email, details, description];
    let query: string = 'INSERT INTO anbieter (name) VALUES (?)';
    connection.query(query, insertData, (err: MysqlError | null) => {
            if(!err){
                res.status(200).json ({message:name + address + phone + email + details + description + price + Name + "successfully added"})

            }
                else{
                    res.status(500).json({message: err.code})
                }
    });
    console.log(req.body);
});
router.get("/anbieter_1", function(req:Request, res: Response){
    console.log(req.body);

    let Name: string = req.body.Name;
    let name: string = req.body.name;
    let address: string = req.body.address;
    let phone: string = req.body.phone;
    let email: string = req.body.email;
    let details: string = req.body.details;
    let description: string = req.body.description;
    let price: number = req.body.price;

    let query:string ='SELECT * From anbieter';
    connection.query(query,(err: MysqlError| null, rows)=>{
        if (!err){
            res.status(200).json ({message:rows[0].name + rows[0].address + rows[0].phone + rows[0].email + rows[0].details + rows[0].description + rows[0].price + rows[0].Name})
        }
        else{
            res.status(500).json ({message:err.code})
        }
    })

});