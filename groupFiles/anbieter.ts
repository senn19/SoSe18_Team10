$(function(){
    console.log("function success");

    // Show the Modal on load
    $("#upload_content").on("click",function () {
        console.log("upload_content.click success");
        $("#exampleModal").modal("show");
    });

    // Hide the Modal
    $("#exampleModal").on('click',function(){
        $("#exampleModal").modal("hide");
    });

    //Save Button
    $("#save").on("click",function () {
        console.log("save.click success");
        let Name: string = $("#product-name").val() as string;
        let name: string = $("#name").val() as string;
        let address: string = $("#address").val() as string;
        let tel:string = $("#tel").val() as string;
        let email:string = $("#e-mail").val() as string;
        let details:string = $("#details").val()as string;
        let description:string = $("#description").val() as string;
        let price: number = $("#price").val() as number;
        console.log("7");

        $.ajax({
            url: ("/anbieter"),
            method:  'POST',
            data:JSON.stringify({Name:Name, name:name, address:address, tel:tel, email:email, details:details, productDescription:description, price:price}),
            dataType:'json',
            contentType: 'application/json',
            error:(jqXHR)=> {
                console.log("false")
            },
            success:(data)=>{
                console.log("200");
            }
        })
    });

    //download Button
    $("#download_content").on("click",function () {
        console.log("Download.click success");

        let Name: string = $("#product-name").val() as string;
        let name: string = $("#name").val() as string;
        let address: string = $("#address").val() as string;
        let phone:string = $("#phone").val() as string;
        let email:string = $("#e-mail").val() as string;
        let details:string = $("#details").val()as string;
        let description:string = $("#description").val() as string;
        let price: number = $("#price").val() as number;
        console.log("9");

        $.ajax({
            url: ("/anbieter"),
            method:'GET',
            data:JSON.stringify({Name:Name, name:name, address:address, phone:phone, email:email, details:details, productDescription:description, price:price}),
            dataType:'json',
            contentType:'application/json',
            error:(jqXHR)=> {
                console.log("false")
            },
            success:(data)=> {
                console.log("201");
            }

        })
    });

});


