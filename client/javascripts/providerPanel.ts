$(function () {

    // render rows until new page will be rendered
    const RENDER_ROWS_IN_LIST = 6;
    const PRODUCT_CRUD_NAMING = "product";

    class ModalRendering {
        // deleteModal: delete of any listItems.
        delete_Title: JQuery = $("#modalDelete .modal-title");
        delete_Body: JQuery = $("#modalDelete .modal-body");
        delete_Button: JQuery = $("#modalDelete .modal-action");

        edit_Title: JQuery = $("#modalEdit .modal-title");
        edit_Body: JQuery = $("#modalEdit .modal-body");
        edit_Button: JQuery = $("#modalEdit .modal-action");

        create_Title : JQuery = $("#modalCreate .modal-title");
        create_Button: JQuery = $("#modalCreate .modal-action");
    }

    class ProductFunctions {
        // alert-message-box | dismissable
        alert: JQuery = $("#productList-Alert");
        message: JQuery = $("#productList-Message");
        provider_Product: JQuery = $("#provider_Product");

        // AJAX-POST: returns UserList[] created, on error {message: errorCode}
        createProduct(product : any) {
            return $.ajax({
                url: "/createProduct",
                method: "POST",
                dataType: "json",
                contentType: "application/json",
                data : JSON.stringify(product),
                error: jqXHR => {
                    this.alert.removeClass().addClass("alert alert-danger");
                    this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message);
                    $("#modalCreate").modal("hide");
                },
                success: response => {
                    this.alert.removeClass().addClass("alert alert-success");
                    this.message.html(response.message);
                    $("#modalCreate").modal("hide");
                    this.getProductList();
                }
            });
        }

        // AJAX-GET: returns ProductData[], on error {message: errorCode}
        getProductByID(id: number) {
            return $.ajax({
                url: "/findProductById/" + id,
                method: "GET",
                dataType: "json",
                contentType: "application/json",
                error: jqXHR => {
                    this.alert.removeClass().addClass("alert alert-danger");
                    this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message);
                },
                success: response => {
                    return response;
                }
            });
        }


        // AJAX-GET: returns productList, on error {message: errorCode}
        getProductList() {
            $.ajax({
                url: "/productList",
                method: "GET",
                dataType: "json",
                contentType: "application/json",
                error: jqXHR => {
                    console.error(jqXHR);
                },
                success: response => {
                    let render = new PanelRendering(response.productList);
                    render.productList();
                },
            })
        }

        // AJAX-DELETE: returns message, on error {message: error}
        deleteProduct(id: number) {
            $.ajax({
                url: "/deleteProductById/" + id,
                method: "DELETE",
                dataType: "json",
                contentType: "application/json",
                error: jqXHR => {
                    $("#modalDelete").modal("hide");
                    this.alert.removeClass().addClass("alert alert-danger");
                    this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message);
                },
                success: response => {
                    this.alert.removeClass().addClass("alert alert-success");
                    this.message.html(response.message);
                    $("#modalDelete").modal("hide");
                    this.getProductList();
                },
            })
        }

        // AJAX-UPDATE: returns productObject, on error {message: error}
        updateProduct(productObject: any, id: number) {
            $.ajax({
                url: "/updateProductById/" + id,
                method: "PUT",
                data: JSON.stringify(productObject),
                dataType: "json",
                contentType: "application/json",
                error: jqXHR => {
                    $("#modalEdit").modal("hide");
                    this.alert.removeClass().addClass("alert alert-danger");
                    this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message + (jqXHR.responseJSON.previousMessage ? "<br />"+jqXHR.responseJSON.previousMessage : ""));
                },
                success: response => {
                    this.alert.removeClass().addClass("alert alert-success");
                    this.message.html(response.message);
                    $("#modalEdit").modal("hide");
                    this.getProductList();
                },
            })
        }

    }

    class PanelRendering {

        provider_ProductList: JQuery = $("#provider_productList");

        list: any;
        paginator: string = "";

        row: number = 0;
        page: number = 0;
        html: string = "";

        constructor(list: any) {
            this.list = list;
        }

        // render userList after getting data from Ajax GET-request.
        public productList() {
            for (let key in this.list) {
                if (!this.list.hasOwnProperty(key)) continue;
                this.singleRow([this.list[key].ID, this.list[key].name,
                    this.list[key].price, this.list[key].product_type]);
            }
            this.pagination();
            this.provider_ProductList.nextAll().remove();
            this.provider_ProductList.after(this.html);
            $("#listPagination").html(this.paginator);
        }

        private singleRow(item) {
            if (this.page == 0 && this.row == 0) {
                this.singlePage();
            }
            if (this.row == RENDER_ROWS_IN_LIST) {
                this.row = 0;
                this.singlePage();
            }
            this.row++;
            this.html +=
                "  <tr>" +
                "    <th scope=\"row\" id='product-" + item[0] + "'>" + item[0] + "</th>\n" +
                "    <td>" + item[1] + "</td>\n" +
                "    <td>" + item[2] + "</td>\n" +
                "    <td>" + item[3] + "</td>\n" +
                "    <td>\n" +
                "        <div class=\"row\">\n" +
                "            <div class=\"col-3\"></div>\n" +
                "                <div class=\"col-3\">\n" +
                "                    <i id=\"updateBtn\" class='fa fa-edit'></i>\n" +
                "                </div>\n" +
                "                <div class=\"col-3\">\n" +
                "                    <i id=\"deleteBtn\" class='fa fa-trash'></i>\n" +
                "                </div>\n" +
                "            <div class=\"col-3\"></div>\n" +
                "        </div>\n" +
                "    </td>" +
                "  </tr>";
        }

        private singlePage() {
            if (this.page == 0) {
                this.page++;
                this.html += "<tbody id='productListPage-" + this.page + "'>";
            } else {
                this.html += "</tbody>";
                this.page++;
                this.html += "<tbody id='productListPage-" + this.page + "' class='d-none'>";
            }
        }

        private pagination(): void {
            this.paginator += " <li class=\"page-item\" id=\"listBack\"><a class=\"page-link\"> << </a></li>\n";
            for (let i: number = 1; i <= this.page; i++) {
                if (i == 1) {
                    this.paginator += "<li class=\"page-item active\" id=\"listNav-" + i + "\"><a class=\"page-link\">" + i + "</a></li>\n";
                    continue;
                }
                this.paginator += "<li class=\"page-item\" id=\"listNav-" + i + "\"><a class=\"page-link\">" + i + "</a></li>\n";
            }
            this.paginator += "<li class=\"page-item\" id=\"listForward\"><a class=\"page-link\"> >> </a></li>";
        }
    }

    // create new instances
    let productFunctions = new ProductFunctions(),
        modal = new ModalRendering();
    productFunctions.getProductList();

    // dismiss alert on click
    productFunctions.alert.on("click", ".close", function () {
        $(this).parent(".alert").addClass("d-none");
    });

    // pagination - onClick pageMenu
    $("#listPagination").on("click", "li", function () {
        let navPageID: string = $(this).attr("id");
        let pageID: string = "#productListPage-" + navPageID.split("-")[1];
        if (!$(this).hasClass("active") && navPageID != "listBack" && navPageID != "listForward") {
            $(pageID).toggleClass("d-none").siblings().not("thead").addClass("d-none");
            $(this).toggleClass("active").siblings().removeClass('active');
        }
        if (navPageID == "listBack" || navPageID == "listForward") {
            let activePage: string = $("#productListTable").children("tbody").not(".d-none").attr("id");
            let lastPage: number = Number($("#productListTable thead").siblings().last().attr("id").split("-")[1]);
            let activePageID: number = Number(activePage.split("-")[1]);
            switch (navPageID) {
                case("listBack"):
                    activePageID--;
                    if (activePageID == 0) return;
                    break;
                case("listForward"):
                    activePageID++;
                    if (activePageID > lastPage) return;
                    break;
            }
            $("#productListPage-" + activePageID).toggleClass("d-none").siblings().not("thead").addClass("d-none");
            $("#listNav-" + activePageID).toggleClass("active").siblings().removeClass("active");
        }
    });


    // deleteModal : get ID of item which to delete
    productFunctions.provider_Product.on("click", "#deleteBtn", function () {
        let deleteItem: number = Number($(this).parents("tr").children("th")
            .attr("id").split("-")[1]);
        modal.delete_Title.html("Produkt mit der ID " + deleteItem + " löschen?");
        modal.delete_Body.html("<p>Sind Sie sich sicher, dass Sie das Produkt löschen möchten?");
        modal.delete_Button.off().on("click", function () {
            productFunctions.deleteProduct(deleteItem);
        });
        $("#modalDelete").modal("show");
    });


    /* * * * * * * * * UPDATE PRODUCT * * * * * * * * */

    // modal: UPDATE  - updates User/Product on Button click
    productFunctions.provider_Product.on("click", "#updateBtn", function () {
        let updateItem: string[] = $(this).parents("tr").children("th")
            .attr("id").split("-"); //get ID from <th id=element-ID> and split it [element, ID]
        $("#modalEdit").modal("show");

        switch (updateItem[0]) { // [element]
            case PRODUCT_CRUD_NAMING: // "product"

                // fill all input elements in form with getItemData
                productFunctions.getProductByID(Number(updateItem[1])).then(message => {
                    let productData: any = message.productList; // productList{key:value}
                    modal.edit_Title.html("Produkt mit der ID " + updateItem[1] + " bearbeiten.");
                    // iterate through product.object and fill all inputs with the same key-Input w/ values
                    $("#pName-uInput").val(productData[0].name);
                    $("#pDescription-uInput").val(productData[0].description);
                    $("#pPrice-uInput").val(productData[0].price);
                    $("#pLength-uInput").val(productData[0].length);
                    $("#pWidth-uInput").val(productData[0].width);
                    $("#pHeight-uInput").val(productData[0].height);
                    $("#pDisplaySize-uInput").val(productData[0].display_size);
                    $("#pResolution-uInput").val(productData[0].resolution);
                    $("#pAdvancedCtrls-uInput").val(productData[0].advanced_controls);
                    $("#pProdSuppl-uInput").val(productData[0].productivity_supplies);
                    $("#pProcessor-uInput").val(productData[0].processor);
                    $("#pGraphicsCard-uInput").val(productData[0].graphics_card);
                    $("#pMemory-uInput").val(productData[0].memory);
                    $("#pRam-uInput").val(productData[0].ram);
                    $("#pPic-uInput").val(productData[0].picture);

                    // register on edit-action click
                    modal.edit_Button.off().on("click", () => {
                        let productObject: any = {};

                        $("#modalEdit input").each(function() {
                            if($(this).val()) {
                                productObject[$(this).attr("id").split("-")[0]] = $(this).val();
                            }
                        });
                        productFunctions.updateProduct(productObject, Number(updateItem[1]));
                    });
                });
        }
    });


    /* * * * * * * * * CREATE Product * * * * * * * * */

    // modal: CREATE - creates User/Product on Button click
    productFunctions.provider_Product.on("click", "#createBtn", function () {
        modal.create_Title.html("Ein neues Produkt anlegen.");
        $("#modalCreate").modal("show");

        // avoid multiple ajax requests 'off/on'
        modal.create_Button.off("click").on("click", () => {
            let productObject : any = {},
                validData : boolean = true;

            // iterate through input elements of form
            $("#modalCreate input").each(function() {
                if(!$(this).val()) {
                    validData = false;
                } else {
                    productObject[$(this).attr("id").split("-")[0]] = $(this).val();
                }
            });

            // if inputs aren't empty call function
            if(validData) {
                productFunctions.createProduct(productObject);
            }
        });
    })
});