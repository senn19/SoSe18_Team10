$(function () {

    // render rows until new page will be rendered
    const RENDER_ROWS_IN_LIST = 6;
    const USER_CRUD_NAMING = "user";

    class ModalRendering {
        // deleteModal: delete of any listItems.
        delete_Title: JQuery = $("#modalDelete .modal-title");
        delete_Body: JQuery = $("#modalDelete .modal-body");
        delete_Button: JQuery = $("#modalDelete .modal-action");

        edit_Title: JQuery = $("#modalEdit .modal-title");
        edit_Body: JQuery = $("#modalEdit .modal-body");
        edit_Button: JQuery = $("#modalEdit .modal-action");

        create_Title : JQuery = $("#modalCreate .modal-title");
        create_Button: JQuery = $("#modalCreate .modal-action");
    }

    class UserFunctions {
        // alert-message-box | dismissable
        alert: JQuery = $("#userList-Alert");
        message: JQuery = $("#userList-Message");
        admin_User: JQuery = $("#admin_User");

        // AJAX-POST: returns User[] created, on error {message: errorCode}
        createUser(user : any) {
            return $.ajax({
                url: "/createUser",
                method: "POST",
                dataType: "json",
                contentType: "application/json",
                data : JSON.stringify(user),
                error: jqXHR => {
                    this.alert.removeClass().addClass("alert alert-danger");
                    this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message);
                    $("#modalCreate").modal("hide");
                },
                success: response => {
                    this.alert.removeClass().addClass("alert alert-success");
                    this.message.html(response.message);
                    $("#modalCreate").modal("hide");
                    this.getUserList();
                }
            });
        }

        // AJAX-GET: returns UserData[], on error {message: errorCode}
        getUserByID(id: number) {
            return $.ajax({
                url: "/user/" + id,
                method: "GET",
                dataType: "json",
                contentType: "application/json",
                error: jqXHR => {
                    this.alert.removeClass().addClass("alert alert-danger");
                    this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message);
                },
                success: response => {
                    return response;
                }
            });
        }


        // AJAX-GET: returns userList, on error {message: errorCode}
        getUserList() {
            $.ajax({
                url: "/userList",
                method: "GET",
                dataType: "json",
                contentType: "application/json",
                error: jqXHR => {
                    console.error(jqXHR);
                },
                success: response => {
                    let render = new PanelRendering(response.userList);
                    render.userList();
                },
            })
        }

        // AJAX-DELETE: returns message, on error {message: error}
        deleteUser(id: number) {
            $.ajax({
                url: "/user/" + id,
                method: "DELETE",
                dataType: "json",
                contentType: "application/json",
                error: jqXHR => {
                    $("#modalDelete").modal("hide");
                    this.alert.removeClass().addClass("alert alert-danger");
                    this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message);
                },
                success: response => {
                    this.alert.removeClass().addClass("alert alert-success");
                    this.message.html(response.message);
                    $("#modalDelete").modal("hide");
                    this.getUserList();
                },
            })
        }

        // AJAX-UPDATE: returns userObject, on error {message: error}
        updateUser(userObject: any, id: number) {
            $.ajax({
                url: "/user/" + id,
                method: "PUT",
                data: JSON.stringify(userObject),
                dataType: "json",
                contentType: "application/json",
                error: jqXHR => {
                    $("#modalEdit").modal("hide");
                    this.alert.removeClass().addClass("alert alert-danger");
                    this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message + (jqXHR.responseJSON.previousMessage ? "<br />"+jqXHR.responseJSON.previousMessage : ""));
                },
                success: response => {
                    this.alert.removeClass().addClass("alert alert-success");
                    this.message.html(response.message);
                    $("#modalEdit").modal("hide");
                    this.getUserList();
                },
            })
        }

    }

    class PanelRendering {

        admin_UserList: JQuery = $("#admin_UserList");

        list: any;
        paginator: string = "";

        row: number = 0;
        page: number = 0;
        html: string = "";

        constructor(list: any) {
            this.list = list;
        }

        // render userList after getting data from Ajax GET-request.
        public userList() {
            for (let key in this.list) {
                if (!this.list.hasOwnProperty(key)) continue;
                this.singleRow([this.list[key].ID, this.list[key].forename,
                    this.list[key].surname, this.list[key].email]);
            }
            this.pagination();
            console.log(this.admin_UserList.children("thead").nextAll());
            this.admin_UserList.nextAll().remove();
            this.admin_UserList.after(this.html);
            $("#listPagination").html(this.paginator);
        }

        private singleRow(item) {
            if (this.page == 0 && this.row == 0) {
                this.singlePage();
            }
            if (this.row == RENDER_ROWS_IN_LIST) {
                this.row = 0;
                this.singlePage();
            }
            this.row++;
            this.html +=
                "  <tr>" +
                "    <th scope=\"row\" id='user-" + item[0] + "'>" + item[0] + "</th>\n" +
                "    <td>" + item[1] + "</td>\n" +
                "    <td>" + item[2] + "</td>\n" +
                "    <td>" + item[3] + "</td>\n" +
                "    <td>\n" +
                "        <div class=\"row\">\n" +
                "            <div class=\"col-3\"></div>\n" +
                "                <div class=\"col-3\">\n" +
                "                    <i id=\"updateBtn\" class='fa fa-edit'></i>\n" +
                "                </div>\n" +
                "                <div class=\"col-3\">\n" +
                "                    <i id=\"deleteBtn\" class='fa fa-trash'></i>\n" +
                "                </div>\n" +
                "            <div class=\"col-3\"></div>\n" +
                "        </div>\n" +
                "    </td>" +
                "  </tr>";
        }

        private singlePage() {
            if (this.page == 0) {
                this.page++;
                this.html += "<tbody id='userListPage-" + this.page + "'>";
            } else {
                this.html += "</tbody>";
                this.page++;
                this.html += "<tbody id='userListPage-" + this.page + "' class='d-none'>";
            }
        }

        private pagination(): void {
            this.paginator += " <li class=\"page-item\" id=\"listBack\"><a class=\"page-link\"> << </a></li>\n";
            for (let i: number = 1; i <= this.page; i++) {
                if (i == 1) {
                    this.paginator += "<li class=\"page-item active\" id=\"listNav-" + i + "\"><a class=\"page-link\">" + i + "</a></li>\n";
                    continue;
                }
                this.paginator += "<li class=\"page-item\" id=\"listNav-" + i + "\"><a class=\"page-link\">" + i + "</a></li>\n";
            }
            this.paginator += "<li class=\"page-item\" id=\"listForward\"><a class=\"page-link\"> >> </a></li>";
        }
    }

    // create new instances
    let userFunctions = new UserFunctions(),
        modal = new ModalRendering();
    userFunctions.getUserList();

    // dismiss alert on click
    userFunctions.alert.on("click", ".close", function () {
        console.log($(this).parent(".alert"));
        $(this).parent(".alert").addClass("d-none");
    });

    // pagination - onClick pageMenu
    $("#listPagination").on("click", "li", function () {
        let navPageID: string = $(this).attr("id");
        let pageID: string = "#userListPage-" + navPageID.split("-")[1];
        if (!$(this).hasClass("active") && navPageID != "listBack" && navPageID != "listForward") {
            $(pageID).toggleClass("d-none").siblings().not("thead").addClass("d-none");
            $(this).toggleClass("active").siblings().removeClass('active');
        }
        if (navPageID == "listBack" || navPageID == "listForward") {
            let activePage: string = $("#userListTable").children("tbody").not(".d-none").attr("id");
            let lastPage: number = Number($("#userListTable thead").siblings().last().attr("id").split("-")[1]);
            let activePageID: number = Number(activePage.split("-")[1]);
            switch (navPageID) {
                case("listBack"):
                    activePageID--;
                    if (activePageID == 0) return;
                    break;
                case("listForward"):
                    activePageID++;
                    if (activePageID > lastPage) return;
                    break;
            }
            $("#userListPage-" + activePageID).toggleClass("d-none").siblings().not("thead").addClass("d-none");
            $("#listNav-" + activePageID).toggleClass("active").siblings().removeClass("active");
        }
    });


    // deleteModal : get ID of item which to delete
    userFunctions.admin_User.on("click", "#deleteBtn", function () {
        let deleteItem: number = Number($(this).parents("tr").children("th")
            .attr("id").split("-")[1]);
        modal.delete_Title.html("Benutzer mit der ID " + deleteItem + " löschen?");
        modal.delete_Body.html("<p>Sind Sie sich sicher, dass Sie den Benutzer löschen möchten?");
        modal.delete_Button.off().on("click", function () {
            userFunctions.deleteUser(deleteItem);
        });
        $("#modalDelete").modal("show");
    });


    /* * * * * * * * * UPDATE USER * * * * * * * * */

    // modal: UPDATE  - updates User/Product on Button click
    userFunctions.admin_User.on("click", "#updateBtn", function () {
        let updateItem: string[] = $(this).parents("tr").children("th")
            .attr("id").split("-"); //get ID from <th id=element-ID> and split it [element, ID]
        $("#modalEdit").modal("show");

        switch (updateItem[0]) { // [element]
            case USER_CRUD_NAMING: // "user"

                // fill all input elements in form with getUserData
                userFunctions.getUserByID(Number(updateItem[1])).then(message => {
                    let userData: any = message.user; // user{key:value}
                    modal.edit_Title.html("Benutzer mit der ID " + updateItem[1] + " bearbeiten.");
                    console.log(userData);
                    // iterate through user.object and fill all inputs with the same key-Input w/ values
                    for (let key in userData) {
                        modal.edit_Body.find("#" + key + "-uInput").val(userData[key]);
                    }

                    // register on edit-action click
                    modal.edit_Button.off().on("click", () => {
                        let userObject: any = {};

                        $("#modalEdit input").each(function() {
                            if($(this).val()) {
                                userObject[$(this).attr("id").split("-")[0]] = $(this).val();
                            }
                        });
                        userFunctions.updateUser(userObject, Number(updateItem[1]));
                    });
                });
        }
    });


    /* * * * * * * * * CREATE USER * * * * * * * * */

    // modal: CREATE - creates User/Product on Button click
    userFunctions.admin_User.on("click", "#createBtn", function () {
        modal.create_Title.html("Einen neuen Benutzer anlegen.");
        $("#modalCreate").modal("show");

        // avoid multiple ajax requests 'off/on'
        modal.create_Button.off("click").on("click", () => {
            let userObject : any = {},
                validData : boolean = true;

            // iterate through input elements of form
            $("#modalCreate input").each(function() {
                if(!$(this).val()) {
                    validData = false;
                } else {
                    userObject[$(this).attr("id").split("-")[0]] = $(this).val();
                }
            });

            // if inputs aren't empty call function
            if(validData) {
                userFunctions.createUser(userObject);
            }
        });
    })
});