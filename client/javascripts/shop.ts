$(function () {

    class ShopData {

        shop: JQuery = $("#products");
        pagination: JQuery = $("#shopPagination");
        search: JQuery = $("#navSearchInput");

        storedProducts: any[] = []; // liveSearch Array with stored products


        /* AJAX GET-Request:    returns on success ({message: xyz, productList: [Product{ID,name,picture]})
                                returns on error ({message: error}) */
        public getShopList(args: string = ""): void {
            let shopList = new ShopList();
            $.ajax({
                url: '/shop' + args,
                type: 'GET',
                dataType: 'json',
                error: (jqXHR) => {
                    this.shop.html(jqXHR.responseJSON.message);
                },
                success: (data) => {
                    this.storedProducts = data.productList;
                    shopList.renderShop(data.productList);
                },
            });
        }
    }

    class ShopList {

        html: string = "";
        paginator: string = "";

        page: number = 0;
        row: number = 0;
        col: number = 0;

        /* 4 cols generate one new row, 3 rows generate one new page
        *  productList[Product{ID: xyz, name: xyz, picture: xyz}] */
        public renderShop(productList: string[]): void {
            for (let product in productList) {
                if (this.page == 0) { // render new page if no page is existent
                    this.renderPage();
                }
                if (!(this.row <= 3)) { // render new row if there are already 3 rows
                    this.renderPage();
                }
                this.renderRow(); // render row, adds one new row to counter
                this.renderCol(productList[product]); // render col, adds one new col to counter
            }
            this.renderPagination(); // render Pagination - pages == PaginationMenus
            shopData.pagination.html(this.paginator);
            shopData.shop.html(this.html);
        }

        private renderRow(): void {
            if (this.col == 4) {
                this.html += '</div>';
                this.col = 0;
            }
            if (this.col == 0 && this.row < 3) {
                this.html += '<div class="row">';
                this.row++;
            } else if (this.col == 0 && this.row == 3) {
                this.row++;
            }
        }

        private renderCol(productList): void {
            if (this.col == 0 && this.row == 4) return;
            this.html += '<div class="col-sm-12 col-md-3 text-center">' +
                '       <div id="' + productList.id + '" class="article">\n' +
                '           <div class="article-image">\n' +
                '               <img src="images/shopImages/' + productList.picture + '" class="img-thumbnail" />\n' +
                '           </div>\n' +
                '           <div class="article-text">\n' +
                '               <div class="article-title">\n' +
                '                   <h6>' + productList.name + '</h6>\n' +
                '               </div>\n' +
                '           </div>\n' +
                '       </div>\n' +
                '   </div>';
            this.col++;
        }

        private renderPage(): void {
            if (this.page == 0) {
                this.page++;
                this.html += '<div id="shopPage-' + this.page + '">';
            } else {
                this.row = 0;
                this.html += '</div>';
                this.page++;
                this.html += '<div id="shopPage-' + this.page + '" class="d-none">';
            }
        }

        private renderPagination(): void {
            this.paginator += " <li class=\"page-item\" id=\"shopBack\"><a class=\"page-link\"> << </a></li>\n";
            for (let i: number = 1; i <= this.page; i++) {
                if (i == 1) {
                    this.paginator += "<li class=\"page-item active\" id=\"shopNav-" + i + "\"><a class=\"page-link\">" + i + "</a></li>\n";
                    continue;
                }
                this.paginator += "<li class=\"page-item\" id=\"shopNav-" + i + "\"><a class=\"page-link\">" + i + "</a></li>\n";
            }
            this.paginator += "<li class=\"page-item\" id=\"shopForward\"><a class=\"page-link\"> >> </a></li>";
        }
    }

    class Filter {

        categories: JQuery = $(".catList");
        producer: JQuery = $(".prodList");

        prefixCategory: string = "&category=";
        prefixProducer: string = "&producer=";

        // query-param - GET on: /url?category=XYZ&producer=XYZ
        filterList: string[] = ["?"];

        /* this function will iterate through the filterList and remove all duplicates of filters which have
        been pushed before on execute. If no duplicate values exist, filter as well as prefix will be pushed into the filterList and
        transmitted to 'createFilter()'. */
        public filterShopBy(prefix: string, filter: string): string {
            for (let key in this.filterList) {
                if (this.filterList[key] == filter) {
                    let index: number = this.filterList.indexOf(this.filterList[key]) - 1;
                    this.filterList.splice(index, 2);
                    return this.createFilter();
                }
            }
            this.filterList.push(prefix, filter);
            return this.createFilter();
        }

        private createFilter(): string {
            // console.log(this.filterList.join("")); // debug
            return this.filterList.join("")
        }
    }

    // create instances
    let shopData = new ShopData();
    let filter = new Filter();
    shopData.getShopList();

    // filter categories: ?category=ID
    filter.categories.children("li").on("click", function () {
        shopData.getShopList(filter.filterShopBy(filter.prefixCategory, $(this).attr("id")));
        if ($(this).children().children().hasClass("fa fa-check-square")) {
            $(this).children().children().removeClass().addClass("far fa-square");
        } else {
            $(this).children().children().removeClass().addClass("fa fa-check-square");
        }
        $(this).toggleClass("active");
    });

    // filter producer: ?producer=ID
    filter.producer.children("li").on("click", function () {
        shopData.getShopList(filter.filterShopBy(filter.prefixProducer, $(this).attr("id")));
        if ($(this).children().children().hasClass("fa fa-check-square")) {
            $(this).children().children().removeClass().addClass("far fa-square");
        } else {
            $(this).children().children().removeClass().addClass("fa fa-check-square");
        }
        $(this).toggleClass("active");
    });

    // pagination - onClick pageMenu
    shopData.pagination.on("click", "li", function () {
        let navPageID: string = $(this).attr("id");
        let pageID: string = "#shopPage-" + navPageID.split("-")[1];
        if (!$(this).hasClass("active") && navPageID != "shopBack" && navPageID != "shopForward") {
            $(pageID).toggleClass("d-none").siblings().addClass("d-none");
            $(this).toggleClass("active").siblings().removeClass('active');
        }
        if (navPageID == "shopBack" || navPageID == "shopForward") {
            let activePage: string = $("#products").children().not(".d-none").attr("id");
            let lastPage: number = Number($("#products :last-child").attr("id").split("-")[1]);
            let activePageID: number = Number(activePage.split("-")[1]);
            switch (navPageID) {
                case("shopBack"):
                    activePageID--;
                    if (activePageID == 0) return;
                    break;
                case("shopForward"):
                    activePageID++;
                    if (activePageID > lastPage) return;
                    break;
            }
            $("#shopPage-" + activePageID).toggleClass("d-none").siblings().addClass("d-none");
            $("#shopNav-" + activePageID).toggleClass("active").siblings().removeClass("active");
        }
    });


    // liveSearch - instead of stupid database flooding, filter productList we've already got in here.
    shopData.search.on("focus", function () {
        $("html, body").animate({scrollTop: $("#shop").offset().top - 78}, "slow"); // navbar 78px
    });

    shopData.search.on("keydown", function () {
        let shopList = new ShopList();
        let productListFiltered: string[] = shopData.storedProducts.filter((product) => {
            return (product.name).toLowerCase().indexOf(($(this).val() as string).toLowerCase()) > -1;
        });
        shopList.renderShop(productListFiltered);
    });

    // navMenu : will filter products as well as the categoryMenu
    $("#navMenu li a").on("click", function (e) {
        e.preventDefault();
        let clickedID: string = $(this).attr("id").split("_")[1];
        shopData.getShopList(filter.filterShopBy(filter.prefixCategory, clickedID));
        let navButton: JQuery = $("#" + clickedID);
        navButton.toggleClass("active");
        if (navButton.children().children().hasClass("fa fa-check-square")) {
            navButton.children().children().removeClass().addClass("far fa-square");
        } else {
            navButton.children().children().removeClass().addClass("fa fa-check-square");
        }
        $("html, body").animate({scrollTop: $("#shop").offset().top - 78}, "slow"); // navbar 78px
    });

    // how it works >> shop button
    $("#hiw_shop").on("click", function () {
        $("html, body").animate({scrollTop: $("#shop").offset().top - 78}, "slow"); // navbar 78px
    });

    $("#cartNavButton").on("click", function () {
        $("html, body").animate({scrollTop: $("#cart").offset().top - 78}, "slow"); // navbar 78px
    });
});

