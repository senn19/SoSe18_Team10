$(function () {

    class RegisterForm {
        registerSubmit: JQuery = $("#register-Submit");

        invalidMail: JQuery = $("#register-Invalid-Email");
        invalidRail: JQuery = $("#register-Invalid-Repeat-Email");
        invalidPassword: JQuery = $("#register-Invalid-Password");
        invalidRassword: JQuery = $("#register-Invalid-Repeat-Password");

        inputForename : JQuery = $("#register-Forename");
        inputSurname : JQuery = $("#register-Surname");
        inputMail: JQuery = $("#register-Email");
        inputRail: JQuery = $("#register-Repeat-Email");
        inputPassword: JQuery = $("#register-Password");
        inputRassword: JQuery = $("#register-Repeat-Password");

        alert : JQuery = $("#register-Alert");
        message : JQuery = $("#register-Message");
    }

    class ValidateData {

        message: string;

        // used to validate email clientside - helpful for login and registration
        public checkEmail(email: string): boolean {
            let posAt: number = email.indexOf("@");
            let posDot: number = email.lastIndexOf(".");

            if (posAt < 1 || posDot < posAt + 2 || posDot + 2 > email.length) {
                this.message = "Die eingegebene E-Mail Adresse ist ungültig";
                this.renderMessage(registerForm.invalidMail);
                return false;
            }
            this.message = "<span class='text-success'>Die eingegebene E-Mail Adresse ist gültig.</span>";
            this.renderMessage(registerForm.invalidMail);
            return true;
        }

        // used to validate password clientside - helpful for registration
        public checkPassword(password: string): boolean {
            if (!password.match(/[0-9]/g)) {
                this.message = "Das Passwort muss mindestens eine Zahl enthalten.";
                this.renderMessage(registerForm.invalidPassword);
                return false;
            }
            if (!password.match(/[A-Z]/g) || !password.match(/[a-z]/g)) {
                this.message = "Das Passwort muss Groß/Kleinbuchstaben enthalten.";
                this.renderMessage(registerForm.invalidPassword);
                return false;
            }
            if (password.length < 7 || password.length > 16) {
                this.message = "Das Passwort muss zwischen 7 und 16 Zeiechen liegen.";
                this.renderMessage(registerForm.invalidPassword);
                return false;
            }
            this.message = "<span class='text-success'>Das Passwort erfüllt alle Voraussetzungen.</span>";
            this.renderMessage(registerForm.invalidPassword);
            return true;
        }

        // used to validate repeated email clientside - helpful for registration
        public checkRail(rail: string, email: string): boolean {
            if (!email || email != rail) {
                this.message = "Die E-Mail Adressen sind nicht identisch.";
                this.renderMessage(registerForm.invalidRail);
                return false;
            }
            this.message = "<span class='text-success'>Die E-Mail Adressen sind identisch.</span>";
            this.renderMessage(registerForm.invalidRail);
            return true;
        }

        // used to validate repeated password clientside - helpful for registration
        public checkRassword(rassword: string, password: string): boolean {
            if (rassword != password) {
                this.message = "Die Passwörter müssen identisch sein.";
                this.renderMessage(registerForm.invalidRassword);
                return false;
            }
            this.message = "<span class='text-success'>Beide Passwörter sind identisch.</span>";
            this.renderMessage(registerForm.invalidRassword);
            return true;
        }

        // used to validate forename/surname
        public checkNames(object : any): boolean {
            if(object.forename == "" || object.surname == "") {
                registerForm.alert.removeClass().addClass("alert alert-danger");
                registerForm.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />" + "Vor- und Nachname" +
                    " dürfen nicht leer bleiben! Korrigieren Sie Ihre Angaben.");
                return false;
            }
            return true;
        }

        // renders any kind of errorMessages in selected field
        private renderMessage(field: JQuery): void {
            field.removeClass("d-none");
            field.html(this.message);
        }
    }

    class Login {

        message: JQuery = $("#login-Message");
        alert: JQuery = $("#login-Alert");

        email: string;
        password: string;

        constructor(email: string, password: string) {
            this.email = email;
            this.password = password;
        }

        public loginAccByEmail() {
            $.ajax({
                url: "/login",
                method: "POST",
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({email: this.email, password: this.encryptPassword()}),
                error: (jqXHR) => {
                    this.alert.removeClass().addClass("alert alert-danger");
                    this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message);
                },
                success: (data) => {
                    if (data.login) {
                        this.alert.removeClass().addClass("alert alert-success");
                        this.message.html(data.message);
                        setTimeout(() => {
                            location.reload();
                        }, 1500);
                    } else {
                        this.alert.removeClass().addClass("alert alert-danger");
                        this.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />" + data.message);
                    }
                },
            })
        }

        // encrypts entered password before sending to the server, afterwards sending encrypted pass without key.
        private encryptPassword(): string {
            return CryptoJS.AES.encrypt(this.password, "crossword").toString();
        }
    }

    class Register {

        email: string;
        password: string;
        forename : string;
        surname : string;

        message: string = "";

        constructor(email: string, password: string, forename : string, surname : string) {
            this.email = email;
            this.password = password;
            this.forename = forename;
            this.surname = surname;
        }

        // sends request to the server, get response from the server
        public regAccByEmail(): void {
            $.ajax({
                url: "/register",
                method: "POST",
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({email: this.email, password: this.encryptPassword(), forename : this.forename, surname : this.surname}),
                error: (jqXHR) => {
                    registerForm.alert.removeClass().addClass("alert alert-danger");
                    registerForm.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />"
                        + jqXHR.responseJSON.message);
                },
                success: (data) => {
                    if(data.user) {
                        registerForm.alert.removeClass().addClass("alert alert-success");
                        registerForm.message.html(data.message);
                    } else {
                        registerForm.alert.removeClass().addClass("alert alert-danger");
                        registerForm.message.html("<strong>Es ist ein Fehler aufgetreten:</strong><br />" + data.message);
                    }
                },
            });
        }

        // encrypts entered password before sending to the server, afterwards sending encrypted pass without key.
        private encryptPassword(): string {
            return CryptoJS.AES.encrypt(this.password, "crossword").toString();
        }
    }

    // open userForm on click
    $("#MainMenu_Login").on("click", () => {
        $("#userForm").modal("show");
    });

    $("#login-Form").on("keypress", (e) => {
        if(e.which == 13) {
            $("#login-Submit").trigger("click");
        }
    });

    // loginForm - read Data from Inputs, call loginFunction()
    $("#login-Submit").on("click", function () {
        let formID: string = this.id.split("-")[0],
            inputData: any = {
                email: ($("#" + formID + "-Email").val() as string),
                password: ($("#" + formID + "-Password").val() as string)
            };
        let login = new Login(inputData.email, inputData.password);
        login.loginAccByEmail();
    });

    // userForm switch login/register
    $("#userForm .modal-header .col-6").on("click", function() {
        if(!$(this).children(".modal-title").hasClass("active")) {
            $(this).parent().children().children().toggleClass("active");
            let clickedID : string = $(this).children(".active").attr("id").split("-")[0];
            $("#"+clickedID+"-Form").toggleClass("d-none").siblings().toggleClass("d-none");
        }
    });

    let registerForm = new RegisterForm();
    let validateData = new ValidateData();

    registerForm.inputPassword.on("keyup", () => {
        validateData.checkPassword(registerForm.inputPassword.val() as string);
    });

    registerForm.inputRassword.on("keyup", () => {
        validateData.checkRassword(registerForm.inputRassword.val() as string, registerForm.inputPassword.val() as string);
    });

    registerForm.inputRail.on("keyup", () => {
        validateData.checkRail(registerForm.inputRail.val() as string, registerForm.inputMail.val() as string);
    });

    registerForm.inputMail.on("keyup", () => {
        validateData.checkEmail(registerForm.inputMail.val() as string);
    });

    $("#register-Form").on("keypress", (e)=> {
         if(e.which == 13) {
             registerForm.registerSubmit.trigger("click");
         }
    });

    // registration, check on valid input -> start registration process
    registerForm.registerSubmit.on("click", () => {
        let email: string = encodeURI(registerForm.inputMail.val() as string),
            rail: string = encodeURI(registerForm.inputRail.val() as string),
            password: string = encodeURI(registerForm.inputPassword.val() as string),
            rassword: string = encodeURI(registerForm.inputRassword.val() as string),
            forename : string = encodeURI(registerForm.inputForename.val() as string),
            surname : string = encodeURI(registerForm.inputSurname.val() as string);

        if (!(validateData.checkEmail(email)) || !(validateData.checkRail(rail, email))
            || !(validateData.checkPassword(password)) || !(validateData.checkRassword(rassword, password))
            || !(validateData.checkNames({forename : forename, surname : surname}))) return;
        let registration = new Register(email, password, forename, surname);
        registration.regAccByEmail();
    });

});

