$(function () {

    // open aboutText/Image on firm-img click
    $("#firm").on("click", "img", function() {
        let imgClicked : JQuery = $("."+$(this).attr("class").split(" ")[0].split("-")[0]+"-About");
        $("#input, #aboutHead, #aboutText, #aboutImage").children().not(".d-none").addClass("d-none");
        imgClicked.removeClass("d-none");
    });

    // slide left on click
    $(".fa-angle-left").on("click", function() {
        if($(this).parent().nextAll().last().prev().hasClass("d-none")) {
            $(this).parent().nextAll().not(".d-none").first().addClass("d-none");
            $(this).parent().nextAll().last().prev().removeClass("d-none");
        }
    });

    // slide right on click
    $(".fa-angle-right").on("click", function() {
        if($(this).parent().prevAll().last().next().hasClass("d-none")) {
            $(this).parent().prevAll().not(".d-none").first().addClass("d-none");
            $(this).parent().prevAll().last().next().removeClass("d-none");
        }
    });


    // just some footer things yo
    $("#aboutUs-Footer, #partner-Footer").on("click", function() {
        $("html, body").animate({scrollTop: $("#aboutus").offset().top - 78}, "slow"); // navbar 78px
    });
});