$(function () {

    const WEEK_DAYS = 7,
        MONTH_DAYS = 30;

    class ProductAjax {

        // AJAX-GET request to get Product from Database
        public getProductByID(id: number): void {
            $.ajax({
                url: "/findProductById/" + id,
                method: "GET",
                contentType: "application/json",
                dataType: "json",
                error: jqXHR => {
                    console.log(jqXHR.responseJSON); // ToDo err
                },
                success: data => {
                    render.itemData = data.productList;
                    render.renderProduct();
                }
            });
        }

        // AJAX-GET request to get Pricing from Server
        public getProductPrice(time : number, price : number) { // only on 'view' - not on 'rent'process..
            return $.ajax({
                url: "/calc?time="+time+"&price="+price,
                method: "GET",
                contentType: "application/json",
                dataType: "json",
                error: jqXHR => {
                    console.log(jqXHR.responseJSON); // ToDo err
                },
                success: data => {
                    return data.calc;
                }
            });
        }

    }

    // render item in shop
    class RenderItem {

        // store item details in array
        itemData : any[];

        prevArticle: string;
        nextArticle: string;

        productPage: JQuery = $("#productDetails");

        productImage: JQuery = $("#article-Image");
        productDescription: JQuery = $("#article-Description");
        productTable : JQuery = $("#productTechInfo");

        productNext: JQuery = $("#articleNext");
        productPrevious: JQuery = $("#articlePrevious");

        productPrice : JQuery = $("#productPrice");
        rentWLength : JQuery = $(".rent-WLength");
        rentMLength : JQuery = $(".rent-MLength");

        productRentTime : JQuery = $("#productRentTime");

        // process to render product details
        public renderProduct() {
            this.productDescription.children("h4").html(this.itemData[0].name);
            this.productDescription.children("p").html(this.itemData[0].description);
            this.productImage.children("img").attr("src", "images/shopImages/" + this.itemData[0].picture);

            this.productPrevious.removeClass().addClass("btn btn-greenery "+this.prevArticle);
            this.productNext.removeClass().addClass("btn btn-greenery "+this.nextArticle);

            this.productTable.html(`
                <tr>
                    <td>`+this.itemData[0].length+`</td>
                    <td>`+this.itemData[0].width+`</td>
                    <td>`+this.itemData[0].display_size+`</td>
                    <td>`+this.itemData[0].processor+`</td>
                    <td>`+this.itemData[0].graphics_card+`</td>
                    <td>`+this.itemData[0].memory+`</td>
                    <td>`+this.itemData[0].ram+`</td>
                    <td>`+this.itemData[0].advanced_controls+`</td>
                </tr>
            `);
            product.getProductPrice(7, this.itemData[0].price).then(price => {
                this.productPrice.children("h5").html(price.calc + "€ /wtl.");
            });
        }

        // process to render pricing
        public renderWeekPrice(productTime : number) {
            product.getProductPrice(productTime, this.itemData[0].price).then(price => {
                this.productPrice.children("h5").html(price.calc + "€ /wtl.");
            });
        }

        public renderMonthPrice(productTime : number) {
            product.getProductPrice(productTime, this.itemData[0].price).then(price => {
                this.productPrice.children("h5").html(price.calc + "€ /mtl.");
            });
        }

        // get next or previous article and render it
        public getPrevNextArticle(id : number) {
            let articleID: number = id,
                prevID: number = articleID,
                nextID: number = articleID,
                actNum: number;
            $("#products").find(".article").each(function () {
                actNum = Number($(this).attr("id"));
                if (actNum > articleID) {
                    nextID = actNum;
                    return false;
                } else if (actNum < articleID) {
                    prevID = actNum;
                }
            });
            this.prevArticle = prevID.toString();
            this.nextArticle = nextID.toString();
        }
    }

    let render = new RenderItem();
    let product = new ProductAjax();

    // on click article in shop
    $("#products").on("click", ".article", function () {
        if (render.productPage.hasClass("d-none")) render.productPage.toggleClass("d-none");
        $("#article-Details").css("max-height",
            Number($("#productDetails .wrapper").height())-Number($("#article-Head-Row").height())-100);
        render.getPrevNextArticle(Number($(this).attr("id")));
        $("html, body").animate({scrollTop: render.productPage.offset().top - 78}, "slow"); // navbar 78px
        product.getProductByID(Number($(this).attr("id")));
    });

    // on click button next or previous on articleDetails page
    render.productNext.on("click", function() {
        render.rentMLength.children().removeClass("active");
        render.rentWLength.children().removeClass("active").first().addClass("active");
        render.getPrevNextArticle(Number($(this).attr("class").split(" ")[2]));
        product.getProductByID(Number($(this).attr("class").split(" ")[2]));
    });

    render.productPrevious.on("click", function() {

        render.rentMLength.children().removeClass("active");
        render.rentWLength.children().removeClass("active").first().addClass("active");
        render.getPrevNextArticle(Number($(this).attr("class").split(" ")[2]));
        product.getProductByID(Number($(this).attr("class").split(" ")[2]));
    });

    // on click render pricing
    render.rentWLength.on("click", "p", function() {
        if(!$(this).hasClass("active")) {
            $(this).toggleClass("active").siblings(".active").toggleClass("active");
        }
        render.renderWeekPrice(Number($(this).text()) * WEEK_DAYS);
    });

    render.rentMLength.on("click", "p", function() {
        if(!$(this).hasClass("active")) {
            $(this).toggleClass("active").siblings(".active").toggleClass("active");
        }
        render.renderMonthPrice(Number($(this).text()) * MONTH_DAYS);
    });

    render.productRentTime.on("click", "p", function() {
        if(!$(this).hasClass("active")) {
            render.rentMLength.children().removeClass("active");
            render.rentWLength.children().removeClass("active");
            $(this).toggleClass("active").siblings().toggleClass("active");
            render.productRentTime.children().not(".rent-Time, .underline, h5").toggleClass("d-none");
        }
    });


});