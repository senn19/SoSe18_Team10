$(function(){

    $("#MainMenu_Logout").on("click", ()=> {
        $.ajax({
            url : "/logout",
            method : "POST",
            dataType : "json",
            error : (error) => {
                console.log(error);
            },
            success : (data) => {
                location.reload();
                console.log(data.message);
            }
        });
    });

    $("#MainMenu_UserPanel").on('mouseenter', function() {
        $(this).find(".dropdown-menu").first().stop(true, true).slideDown();
    });

    $(".userPanel, #navIcons").on('mouseleave', function() {
            $(".userPanel").first().delay(3000).stop(true,true).slideUp();
    });
});

